--테이블에서 컬럼에 해당하는 데이터를 검색하라
select column1, column2, column3
from table명;

select job_id,job_title,min_salary,max_salary
from jobs;

select * from jobs;

--컬럼명, 테이블에 별칭을 지정할 수 있다.
--별칭(alias)으로 한글이나 영문공백을 사용할 때는 " " (따음표)를 지정한다.

select salary, salary*10 as bonus
from employees;

select salary, salary*10 as "b o n u s" -- alias가 공백이 있을경우 쌍따움표가 있어야 한다.
from employees;

select salary, salary*10 as 보너스 -- 이전 버전에서는 한글일 경우 ""로 묵어줘야했다.
from employees;

select last_name || '님의 월급은 ' || salary || '입니다.' as name -- 영어공부좀 하자!!!
from employees;

select first_name
from employees;

-- 컬럼에서 중복된 값 제거후 출력 distinct 여러개의 컬럼중 처음에만 써야한다.
select distinct first_name
from employees;
-- 주의할점 두개의 컬럼에 중복제거를 할때는 첫번째를 중복제거후 뒤에는 그냥나온다.
select distinct first_name, last_name
from employees;

--select 입력순서
/*select 컬럼명, 컬럼명
from 테이블명
where 컬럼명='값'
group by 컬럼명
having 컬럼명 = '값'
order by 컬럼명 desc; 해석할때도 제일 나중에 해야함
*/
select first_name from employees;

--employees테이블에서 salary이 3000미만일 때의
--first_name, salary을 출력하라
select first_name, salary
from employees
where salary<=3000;

--employees테이블에서 first_name 컬럼의 값이 'David' 아닐때의
-- first_name, salary을 출력하시오
-- 오라클에서 비교 연산자 같지 않다 !=,<> 이고 같다는 = 이다
select first_name, salary
from employees
where first_name !='David';

select first_name, salary
from employees
where first_name <>'David';

-- &&(and) ||(or)
-- employees테이블에서 salary이 3000, 9000, 17000일때
-- first_name, hire_date, salary을 출력하라

select first_name, hire_date, salary
from employees
where salary=3000 OR salary=9000 OR salary=17000; 

select first_name, hire_date, salary
from employees
where salary in(3000,9000,17000);-- 같은 컬럼에 있는걸 비교할때 or일경우 in을 이용하여 사용할수있다.

--employees테이블에서 salary이 3000부터(이상) 5000까지(이하)의 일때의
--first_name, hire_date, salary 을 출력하라
select first_name, hire_date, salary
from employees
where salary>=3000 and salary<=5000;

select first_name, hire_date, salary
from employees
where salary between 3000 and 5000; -- 작거나 같다 아니면 크거나 같다 이면 between을 이용 할수있다.

--employees테이블에서 job_id가 'it_prog'이 아닐때
--first_name, email, job_id을 출력하라.

select first_name, email, job_id
from employees
where job_id <> 'IT_PROG';

select first_name, email, job_id
from employees
where NOT(job_id = 'IT_PROG'); -- not(조건문)하면 조건문의 반대로 나옴 트루면 펄스 펄스면 트루가 나옴

--employees테이블에서 slary이 3000부터 5000까지 아닐때의
--first_name, email, job_id을 출력하라.

select first_name, email, job_id
from employees
where not(salary between 3000 AND 5000);

select first_name, email, job_id
from employees
where salary not between 3000 AND 5000;

select first_name, hire_date, salary
from employees
where not(salary>=3000 and salary<=5000);

--employees테이블에서 commission_pct이 null일때
--first_name, salary, commission_pct을 출력하라
select first_name, salary, commission_pct
from employees
where commission_pct is null; -- 널값을 비교할때는 is을 사용한다.
--null이 아닐때
select first_name, salary, commission_pct
from employees
where not(commission_pct is null);

select first_name, salary, commission_pct
from employees
where commission_pct is not null;

--employees테이블에서 first_name에 'der' 이 포함이된
--first_name, salary, email을 출력하라
select first_name, salary, email
from employees
where first_name like '%der'; -- %der끝나는 문자를 포함한것을 출력한다. %der%중간에 있는경우는 중간에 문자가 있는걸 가져온다.

--특정문자 하나를 가르킬때
--employees테이블에서 first_name의 값중 A로 시작하고
-- 두번째 문자는 임의문자이며 exander로 끝나는 (_ 임의문자)
--first_name, salary, email을 출력하라
select first_name, salary, email
from employees
where first_name like 'A_exander';

/*
where절에서 사용된 연산자 3가지 종류
1 비교연산자 : = > >= < <= != <> ^=(다르다)
2 SQL연산자 : between a and b, in, like, is null
3 논리연산자 : ANd, OR, NOt

우선순위
1 괄호()
2. not연산자
3. 비교연산자, SQL연산자
4 AND
5 OR
*/

--employees테이블에서 job_id을 오름차순으로
--first_name, email, job_id을 출력하시오.    기본은 오름차순
select first_name, email, job_id
from employees
order by job_id ASC;--(오름차순)

select first_name, email, job_id
from employees
order by job_id; --  별도로 지정을 안해주면 오름차순

--employees테이블에서 department_id을 오름차순으로
--first_name을 내림차순으로
-- department_id first_name, salary을 출력하시오.
select department_id,first_name, salary
from employees
order by department_id asc, first_name desc; 
-- department가 먼저 오름차순으로 정리를 하고 오름차순한 것에서 같은 값일때 내림차순으로 정리한다.


--employees테이블에서 가장 최근 입사 순으로
--사원의 first_name, salary, hire_date을 출력하시오.
select first_name, salary, hire_date
from employees
order by hire_date DESC;

--employees테이블에서 업무(job_id)이 'FI_ACCOUNT'인 사원들의
--급여(salary)가 높은순으로 first_name, job_id, salary을 출력하시오
select first_name, job_id, salary
from employees
where job_id='FI_ACCOUNT'
order by salary DESC;

