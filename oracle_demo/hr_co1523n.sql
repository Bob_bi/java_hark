select * from departments where lower(department_name) like lower('%der%');--lower 소문자변환

CREATE TABLE mem (
    num number,
    name varchar(50),
    age number,
    loc varchar2(100)
);

CREATE SEQUENCE mem_num_seq
START WITH 1
INCREMENT BY 1
Nocache
NOCYCLE;


INSERT INTO mem VALUES(mem_num_seq.nextval, '홍길동', 30, '서울');
commit;