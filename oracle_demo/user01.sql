sql> conn user01/tiger

create table tabata(
  id number
);

insert into tabata values(10);
select * from tabata;
update tabata set id=20;
select * from tabata;

/*
1. system.sysfh 접속
2.사용자 계정 생성 => user01/tiger
3. greant to create session to user01 -- 접근 권한 보호
4. greant  create table => 테이블 생성 권한 부여
*/

create view tview 
as
select * from tabata;

select * from tview;

show user;

/*
select문 실행을 하지만 에러발생한다. 이유는  emp객체에 접근할 수 있는
  권한이 없다. emp객체는 hr계정의 소유이다
  자기 소유가 아닌 다른 계정의 소유객체에 접근하기 위해서는 반드시
  접근 권한을 받아야 한다.*/
select * from emp;

select * from hr.emp; 
-- select 실행시 emp객체의 소유자인 hr을 명시 했기 때문에 정상 실행된다.
-- 사용자에게 권한을 줬다 하더라고 사용하는 계정에서 만든 객체가 아니기때문에 만들어진 사용자를 적은후 쩜을 붙여 사용해야 한다. (hr.emp)

-- user01 계정에 부여된 권한 정보 조회
select * from user_tab_privs_recd;


--ORA-01031: insufficient privileges 권한이 충분하지 않다
-- 에러발생 즉 user01계정에 부여된 emp객체를 조회하는 select 명령문이 철회 되었기 때문에....
select * from hr.emp;

create table addd(
ads number,
bse varchar2(30),
cc varchar2(30)
);

insert into addd(ads,bse,cc) values(1,'df','null');
select * from addd;
