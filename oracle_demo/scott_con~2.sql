select * from user_constraints;SELECT
    data1,
    data2

desc test;
alter table test add job varchar2(20);
select * from test;

alter table test rename column data2 to salary;
select * from test;

alter table test modify job varchar2(30);
DESC test; 
-- 구조확인 명령어

alter table test ADD grads number(1);
desc test;
select * from test;
update test set grads=1 where data1 =1000;
-- 업데이트 테스트 데이블에 date1에 값이 1000이면 grads에 1로 업데이트 하라
select *from test;
alter table test modify grads number(2);
update test set grads=10 where data1=1000;
desc test;
update test set grads=10 where data1=1000;
insert into test values(2000,30,'saleman',2);
select * from test;
alter table test modify grads number(1);
desc test;
-- 테이블에 이미 데이터가 저장이 되어있는 상태에서 그레이드스에 이미 두자리수의 데이터가 들어가 있어서 한자리로 줄일려면 오류가 난다.
-- ORA-01440: column to be modified must be empty to decrease precision or scale

insert into test(data1,salary,job)
values (3000,40,'clark');
select * from test;
--컬럼에 default값을 변경
-- alter 로 default값을 지정할수 있다. 지정을 하면 새롭게 나오는 값 부터 적용된다.
alter table test modify grads number(2) default 0;
insert into test(data1,salary,job)
values (4000,50,'research');
select *from test;
--제약조건 새롭게 추가
alter table test drop column job;
desc test;
delete from test where data1=2000;
alter table test add constraints tast_data1_pk primary key(data1);
select * from test;
desc test;
select constraint_name, table_name from user_constraints
where table_name ='TEST';
--제약조건 네임 수정
test_data1_pk_new
alter table test rename constraints tast_data1_pk to tast_data1_pk_new;
select constraint_name, table_name from user_constraints
where table_name='TEST';
--제약 조건 삭제
alter table test drop constraints tast_data1_pk_new;
select constraint_name, table_name from user_constraints
where table_name='TEST';

--컬럼을 삭제하지 않지만 논리적으로 컴럼의 사용을 제한하는 옵션!!
select * from test;

alter table test set unused(grads);
select * from test;
desc test; -- 구조확인
select *from user_tables;
select * from user_unused_col_tabs;

--테이블 명 변경
alter table test rename to test_new;
select * from test;
select * from test_new;
select table_name from user_tables;
--delete는 삭제후 데이터는 지워지지만 구조는 남아있어 커밋을 안하면 되살릴수 있다. drop은 자동커밋이되기 때문에 되살릴수 없다.

--*테이블 생성 DDL
create table customer(
id varchar2(20) primary key,
pwd varchar2(20) not null,
name varchar2(20) not null,
phone number not null
);


--*insert, select 구문
-- phone 타입이 number 이므로 정수 숫자만 저장할수 있기때문에 앞자리가 0으로 시작하면 저장이 안된다.
insert into customer values ('kd_hong', 'pass01', '홍길동', '01055557777');
select * from customer;
--*수정
-- 컬럼 유형을 변경할경우 컬럼내부에 있는 데이터를 지우고 나서 변경을 해야 한다.
delete from customer where phone='01055557777';
alter table customer modify phone varchar2(30);


--//////////////////////////////////
--* 회원 테이블 작성 구문
create table member(
id varchar2(20) primary key,
pwd varchar2(20) not null,
phone varchar2(11) not null
);
--*구매목록 테이블
create table buy(
no number primary key,
item varchar2(20) not null,
mem varchar2(20) not null,
data DATE default sysdate, -- 디폴트값을 설정을 했기 때문에 값을 넣어줄 필요가 없다.
foreign key (mem) references member(id) on delete set null
);
insert into member values('hi','남자','data');
insert into buy(no,item,mem) values('2','dfsf','hi');
select * from member;
select * from buy;
delete from member where id='hi';

--/////////////////////////////////////////////
create table mem_tbl(
 
no number,
id varchar2(20),
pw varchar2(20) not null,
phone varchar2(11) not null,
primary key(no,id) -- 복합키 설정
);
SELECT * FROM user_constraints;

--==========================================
create table mem_tbl(
no number primary key,
id varchar2(20) unique,
pw varchar2(20),
phone varchar2(11),
not null(,id,pw,phone) -- not null은 컬럼 레벨에서만 만들수있다.
-- 문법상으로는 unique와  not null을 동시에 적용할수 있다. 그렇게 하면 기본키와 동일한 자격을 갖춘다.
);



