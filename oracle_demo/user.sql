select * from 


show user;

-- 시스템 계정을 생성하기 위해 시스템 관리자로 접속
sql> conn system/a1234

--현재 시스템관리자 상태이기 때문에 다른 사용자계정 생성
--가능하므로 정상실행
create user user01 identified by tiger;

--현재 사용자 계정 검색(sys,system일때만 사용자 계정
-- 생성할 수 있음)

show user;

사용자 권한 : 시스템권한과 객체권한으로 구분된다

 데이터베이스 관리자가 가지는 시스템권한
 1) create user : 새롭게 사용자를 생성하는 권한
 2) drop user : 사용자를 삭제하는 권한
 3) drop any table : 임의의 테이블을 삭제할 수 있는 권한
 4) query rewrite : 질의재작성을 할 수 있는 권한
 5) backup any table : 임의의 테이블을 백업할 수 있는 권한

 일반 사용자가 데이터베이스를 관리하는 권한
 1) create session : 데이터베이스에 접속할수 있는 권한
 2) create table : 사용자 스키마에게 테이블을 생성할 수 있는 권한
 3) create view : 사용자 스키마에게 뷰를 생성할 수 있는 권한
 4) create sequence : 사용자 스키마에게 시퀀스를 생성할 수 있는 권한
 5) create procedure : 사용자 스키마에게 프로시저(함수)를 생성할수 있는 권한
 
 
 --사용자에게 권한 부여하기 위한 grant 명령어
 -- grant sustem_privilege to iwer_name;
 
 --user01 사용자에게 데이터베이스 접근권한 부여
 grant create session to user01;
 
 grant create user to user01;
 grant drop user to user01;
 grant drop any table to user01;
 grant query rewrite to user01;
 grant backup any table to user01;
 
 --사용자 계정  변경
 sqL> conn user01/tiger
 
 select * from hr.employees; -- 커멘드 창에서는 유저01사용자로 권한을 받았는데 유저01은 데이터 접근권한이 없이때문에 볼수 없다.
 commit;
 grant create table to user01;
 alter user user01 default tablespace users quota unlimited on users; 
 
 drop user user02;
 create user user02 identified by tiger;
 
 /* user02 계정에 데이터베이스 접근권한 부여시 with admin option
 옵션을 지정하게 되면 현재 사용자 계정으로 다른 사용자에게 권한을 부여할수 있다.*/
grant create session to user02 with admin option; 이권한을 user02가 다른사용자들에게 부여할수 있다,
grant create view to user02 with admin option;

grant create session to user01 with admin option; 이권한을 user02가 다른사용자들에게 부여할수 있다,
grant create view to user01 with admin option;



/* user02 계정에 데이터 베이스 접근권한 부여시 with admin option
 옵션을 지정하게 되면 현재 사용자 계정으로 다른 사용자에게 권한을 부여할 숭 있다.*/
 
 grant create session to user02 with admin option;
 
 
 
 
 
 create user user03 identified by tiger;
 --user 03에 connect, resource롤 권한 부여
 
 grant connect, resource to user03;
 
 --데이터디셔너리인 dict체이블에서 role을 검색
select * from dict-- 데이터사전에 있는 테이블 종류
where table_name like '%ROLE%'; -- 테이블의 종류 보기

--mrole롤 생성
create role mrole;

-- 롤생성후 관리자 쪽에서 권한을 준다.

-- mrole롤을 user03계정에 권한 부여
grant mrole to user03;

select * from hr.emp;

-- 부여했던 권한을 다시 뺐을수 있다.
revoke mrole from user03;

-- 롤을 제거할수 있다.
drop role mrole;

--------------------------------------------------------------
--user04 사용자 생성
create user user04 identified by tiger;

-- 데이터베이스에 접근할수 있는 권한
grant create session to user04 with admin option;

-- user04에 테이블 생성할수 있는 권한 주기
grant create table to user04;
alter user user04 default tablespace users quota unlimited on users; 

/* SQL> create user user04 idetified bt tiger
2 default tablespace users ramporary tavlespace temp
3.quota 1m on users;
  => user04  사용자 생성*/
  
--테이블스페이스 공간??


select * from departments order by department_id;
