/*=================================================
조인(join) : 여러개의 테이블에서 원하는 데이터를
            추출해주는 쿼리문이다.
join은 oracle제품에서 사용되는 oracle용 join이 있고
모든 제품에서 공통적으로 사용되는 표준(ansi) join이 있다.
===================================================*/
/* cartesian product(카티션 곱) 조인 :
   테이블 행의 개숫만큼 출력해주는 조인이다.
   1) oracle용 cartesian product*/
   select e.department_id, e.first_name,
           e.job_id, j.job_title
    from employees e, jobs j; -- 테이블.컬럼명 을 해줘야 한다. --  임플로이의 갯수와 잡스의 갯수를 곱한 결과만큼 나온다.
    
    select count(*) from employees; -- 107
    select count(*) from jobs; -- 19
    
--2) ansi용 cartesian product(cross join)
  select e.department_id, e.first_name,
         e.job_id, j.job_title
    from employees e cross join jobs j;
    
--2 equi join(== iner join)
/* 가장 많이 사용되는 조인방법으로 조인 대상이 되는 두 테이블에서
  공통적으로 존재하는 컬럼의 값이 일치되는 행을 연결하여
  결과를 생성하는 방법이다.*/
--(1) oracle equi join
select e.department_id, e.first_name,
       e.job_id,    j.job_id, j.job_title
from employees e, jobs j
where e.job_id=j.job_id; -- equi join은 웨어 절에서  관계연산을 하는것이다
-- e.job_id j.job_id 외래키와 기본키로 연결이 되어있어서 불러올수가 있다?

select job_id
from employees
where job_id is null;--0개

--(2) ansi용 equi join(inner join)
select e.department_id, e.first_name,
       e.job_id,    j.job_id, j.job_title
from employees e inner join jobs j
on e.job_id =j.job_id;-- ansi에서는 웨어가 아니라 on으로 웨어를 대신한다.

--employees 와 departments테이블에서 사원번호(employees_id),
-- 부서번호(department_id), 부서명(department_name)을 검색하시오

select e.employee_id,e.department_id,d.department_name 
from employees e, departments d
where e.department_id = d.department_id;  -- employee 테이블과 department테이블을 조인을 시켜서 공통된 자료를 가져온다 107 임플로이 테이블에
                                         -- null값이 있어서 널값은 가져올수 없다.

select department_id
from employees
where department_id is null;

--employees 와 jobs테이블에서 사원번호(employees_id),
-- 업무번호(job_id), 업무명(JOB_TITLE)을 검색하시오

select e.employee_id , e.job_id, j.job_title
from employees e , jobs j
where e.job_id=j.job_id;

--job_id가 'FT_MGR'인 사원이 속한
--급여(salary)의 최소값(min_salary).
--최대값(max_salary)을 출력하시오
select e.job_id, e.salary, j.min_salary,j.max_salary--e급여가 min max에 해당하는지 확인
from employees e, jobs j
where e.job_id=j.job_id 
and e.job_id='FI_MGR';

--부서가 'Seattle'(냐쇼)에 있는 부서에서 근무하는
--직원들이 first_name, hire_date, department_name, city(losation테이블)
-- 출력하는 select문장을 작성하여라

select e.first_name, e.hire_date, d.department_name, l.city
from employees e, departments d, locations l
where e.department_id=d.department_id
and d.location_id=l.location_id
and l.city='Seattle';


--annsi join으로 변경
select e.first_name, e.hire_date, d.department_name, l.city
from employees e inner join departments d
on e.department_id=d.department_id
   inner join locations l
   
on d.location_id=l.location_id
where l.city='Seattle';

--20번 부서의 이름과 그부서에 근무하는 사원의 이름(first_name)을출력하시오
select d.department_id ,d.department_name, e.first_name
from employees e,departments d
where d.department_id=e.department_id and e.department_id=20;

--1400,1500 번 위치의 도시 이름과 그곳에 있는 부서의 이름을 출력하시오(도시의 번호)

select l.city,d.department_name , l.location_id
from locations l ,departments d
where l.location_id=d.location_id and d.location_id in (1400,1500);

--3. natural join
/* natural join은 두테이블 간의 동일한 이름을 갖는 모든 칼럼들에
  대해 equi(=) join을 수행한다.
  그리고,sql server에서는 지원하지 않는 기능이다. 잘 사용하지 않는 기술이다.*/
  select first_name, salary, department_id, department_name
  from employees natural join departments;

--4.non_equi join
/*(=)연사자를 제외한 >=,<=,>,<등의 연산자를 이용해서
   조건을 지정하는 조인방법이다.*/

select e.first_name, e.salary,j.min_salary,
       j.max_salary, j.job_title
from employees e, jobs j
where e.job_id=j.job_id
and e.salary>=j.min_salary
and e.salary<=j.max_salary;

--(2) ansi용 non_equi join
select e.first_name, e.salary,j.min_salary,
       j.max_salary, j.job_title
from employees e join jobs j
on e.job_id=j.job_id
and e.salary>=j.min_salary
and e.salary<=j.max_salary;

--(1) oracle용 outer join
--한쪽 테이블에는 데이터가 있고 다른 반대쪽에는 데이터가 없는 경우에
--데이터가 있는 테이블의 내용을 모두 가져오는 조인이다.

select e.first_name, e.employee_id,
       d.department_id,e.department_id
from employees e, departments d
where e.department_id = d.department_id(+) -- (+)가 된곳의 일치하지 않은 데이터를 다 가져온다.
order by e.employee_id;

--(2) ansi용 outer join

select e.first_name, e.employee_id,
       d.department_id,e.department_id
from employees e left outer join departments d -- 먼저 일치한것부터 가져오고  그다음 일치하지 않은것을 가져온다.
on e.department_id =d.department_id -- (+)가 된곳의 일치하지 않은 데이터를 다 가져온다.
order by e.employee_id;

--6 self join
--하나의 테이블을 두개의 테이블로 설정해서 사용하는 조인방법이다.
--(1) oracle용 self join
select e.employee_id as "사원번호", 
       e.first_name as "사원이름", 
       e.manager_id as "관리자번호",
       m.first_name as "관리자이름"
from employees e, employees m
where e.manager_id=m.employee_id;

--(2) ansi용 self join
select e.employee_id as "사원번호", 
       e.first_name as "사원이름", 
       e.manager_id as "관리자번호",
       m.first_name as "관리자이름"
from employees e join employees m
on e.manager_id=m.employee_id;


 ----------------------------------------------
 문제
 ----------------------------------------------
 
1)EMPLOYEES 테이블에서 입사한 달(hire_date) 별로 인원수를 조회하시오 . 
  <출력: 월        직원수   >
select to_char(hire_date, 'mm')as 월,count(*)as 인원수
from employees
group by to_char(hire_date, 'mm')
order by to_char(hire_date, 'mm');

  
2)각 부서에서 근무하는 직원수를 조회하는 SQL 명령어를 작성하시오. 
단, 직원수가 5명 이하인 부서 정보만 출력되어야 하며 부서정보가 없는 직원이 있다면 부서명에 “<미배치인원>” 이라는 문자가 출력되도록 하시오. 
그리고 출력결과는 직원수가 많은 부서먼저 출력되어야 합니다.
select nvl(to_char(department_id),'미배치인원'),count(*)
from employees
group by department_id
having count(*)<5
order by count(*);


3)각 부서 이름 별로 2005년 이전에 입사한 직원들의 인원수를 조회하시오.
 <출력 :    부서명		입사년도	인원수  >
select d.department_name as 부서명, to_char(hire_date, 'yyyy')as 입사년도,count(*)as 인원수
from departments d ,employees s
where d.department_id=s.department_id
and to_char(hire_date, 'yyyy')<2005
group by to_char(hire_date, 'yyyy'),department_name
order by department_name;


4)직책(job_title)에서 'Manager'가 포함이된 사원의 이름(first_name), 직책(job_title), 부서명(department_name)을 조회하시오.
select first_name, job_title, department_name
from employees e, jobs j, departments d
where d.department_id = e.department_id
and e.job_id=j.job_id
and j.job_title like ('%Manager%');


5)'Executive' 부서에 속에 있는 직원들의 관리자 이름을 조회하시오. 
단, 관리자가 없는 직원이 있다면 그 직원 정보도 출력결과에 포함시켜야 합니다.
 <출력 : 부서번호 직원명  관리자명  >

select d.department_id as, e.first_name as "직원명", e2.first_name as "관리자명"
from departments d, employees e , employees e2
where d.department_id = e.department_id
and e.manager_id = e2.employee_id(+)
and d.department_name = 'Executive';



