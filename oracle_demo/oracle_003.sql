------------------------------------------------
/*오라클에서 제공되는 명령어
1.DQL(Data Query Language 데이터 질의어) :
  데이터를 검색해서 추출할때 사용된다.(select)
2.DML (Data Maniqulation Languge  데이터 조작어) :
 데이터는 추가 수정 삭제 병합해주는 명령어들이다.
 (insert, uqdate, delete, merge)
3.DDL(Data Definition Language 데이터 정의어) :
테이블의 구조를 정의 , 변경해주는 명령어들이다.
(create, drop, alter, truncate)
4.DCL(Data COntrol Langyage 데이터 제어어) :
사용자의 권한을 부여 제거해주는 명령어들이다.(grant, revoke)
5.TCL(Transaction Control Language 트렌잭션 처리어) {
트랭잭션 설정, 취소를 처리해주는 명령어들이다,
(commit , rollback, savepoint)-*/
--------------------------------------------------------------
--create 절 where절 from w절에서 서브쿼리 사용가능

drop table emp01;
--테이블 구조를 복사하는 방법
create table emp01
as 
select first_name, salary
from employees;  --employees테이블에서 first_name, salary를 복사 가져와 서 emp01테이블 이름으로 생성한다.

select * from emp01;

create table emp01
as 
select first_name, salary
from employees
where 1=0; -- 테이블의 구조만 복사생성하기위해선 where절에서 만족하지않는 조건문을 생성 1=0은 될수가 없으므로 구조만 가져올수 있다.

--테이블에 레코드를 삽입
insert into 테이블(컬럼1,컬럼2)
values('값1',값2);
insert into emp01(first_name, salary)
values('candy',5000);

select* from emp01;

--테이블 모든 컬럼에 데이터를 삽입할때는 컬럼명을 생략할 수 있다.
insert into emp01
values('Steven',4000);

desc emp01; -- 컬럼의 유형을 확인할수 있다 테이블의 구족확인 할수 있는 명령어
select *from emp01;

--묵시적 null 삽입 방법
insert into emp01(first_name)-- 특정한 값만 데이터를 넣어 줄때는 테이블옆에 데이터를 넣어주고싶은 컬럼명을 적고 그컬럼에만 데이터를 넣어주면 자동으로 남아있는 컬럼에는 null값이 들어간다.
values('LEx');
select * from emp01;

--명시적으로 null 삽입방법
insert into emp01
values('Paran', null);

select * from emp01;

---------------------------------
테이블의 내용을 수정하는 명령어이다
UPDATE 테이블명
SET 컬럼1=값1, 컬럼2=값2
WHERE 컬럼=값;
----------------------------------
--emp01테이블 first_name컬럼의 값이 'candy'일때
--salary컬럼의 값을  8000으로 수정하라
UPDATE emp01
SET salary=8000 -- 여러개의 컬럼의 값을 변경해주고 싶으면 ,을 찍고 여러개를 찍으면 된다.
where first_name ='candy';
select * from emp01;

-------------------------------
/*DELETE
DELETE  FROM 테이블명 WHERE 컬럼=값*/
--특정 컬럼을 삭제하는게 아니라 레코드 단위로 삭제를 할수 있다. 행단위가 지워진다.
------------------------------------------------------------
delete from emp01
where first_name = 'candy';

select * from emp01;

--======================================
/*트랜잭션(Transaction) 시작해서 끝나는거 까지의 단계를 트랜잭션이라 한다.
1 트랜잭션 정의
  -한번에 수행되어야 할 데이터베이스의 일련의 Read와
   Write 연산을 수행하는 단위
  -하나의 논리적 기능을 수행하기 위한 작업의 단위로서
  데이터베이스의 일관된 상태를 또 다른 일관된 상태로
  변환시킴
  
  트랜잭션시작-> 수정->삭제->삽입 -> 트랜잭션종료
  하나의 작업이 시작해서 정상적으로 종료될때까지의 과정을 말한다.(트랜잭션)
  (논리적인 작업단위)
  
  은행(ATM)->기계 카드삽입-> 기계가 카드를 읽음->
  인출금액 입력-> 비밀번호 입력-> 기계에서 출금액 처리
  -> 출금금액 제공-> 카드제공
  
  <트랜잭션 시작>
  -데이터베이스에 처음 접속했을 때
  -하나 또는 여러 개의 DML문이 실행된후 commit 또는 rollback
   문이 실행된 직후
  
   <트랜잭션 종료>
  - commit 또는 rollback문이 실행될때
  - DDL 또는 DCL문이 실행될 때 (자동 커밋이 된다)
  - 정상적으로 데이터베이스를 종료할때
  - 비정상적으로 데이터베이스를 종료할때
  
2 트랜잭션 안전성 확보를 위한 ACID
  1) ACID정의 
     -데이터베이스에서 논리적인 작업단위인 트랜잭션이
       안전하게 수행되는 것을 보장하는 특성집합
  2) ACID의 필요성
     -다중 사용자 환경 대응 :동일데이터-다중작업환경에서의
          데이터 처리 정확성 보장
     -안전한 트랜잭션 수행을 통한 데이터베이스 무결성 유지
  3) 트랜잭성 구성요소   (A,C,I,D)
    Atomicity(원자성):
     -트랜잭션은 한 개 이상의 동작을 논리적으로 한 개의
      작업단위(single unit of work)로서 분해가
      불가능한 최소의 단위
     -연산 전체가 성공적으로 처리되거나 또는 한 가지라도
      실패할 경우 전체가 취소되어 무결성을 보장 (무결성) == 정상적인 값만을 저장할수있게 보장하는것
      (All or Nothing)
    Consistency(일관성):
    -트랜잭션이 실행을 성공적으로 완료하면 언제나 모순 없이
     일관성 있는 데이터베이스 상태를 보존함
    - 테이블의 데이타는 갱신되고 그에 따른 인덱스는 갱신되지 않는 등의 부정합이 있어선 않된다.
    Isolation(고립성,독립성):
    -트랜잭션이 실행 중에 생성하는 연산의 중간 결과를
     다른 트랜잭션이 접근할 수 없음
   -커밋된 트랜잭션은 장애가 발생해도 데이타가 복구되여야 한다는 특성.
  Durability(영속성,지속성,내구성):
    -성공이 완료된 트랜잭션의 결과를 영구적으로 데이터베이스에
     저장됨*/
----------------------------------------------------------------------------------------

commit;-- 디벨로퍼를 이용하여 작성하면 컴퓨터 메로리에 저장이 되는데 실제 데이터베이스에는 저장이 안된다 그러므로 commit을 이용하여 실체화를 해 
--데이터 베이스로 저장할수있게 된다.

insert into emp01
values('Beta',7000);

select * from emp01;
commit;

insert into emp01 -- 커밋후 DML명령어를 취소를 할수 있다.
values('John',2000);

savepoint sp;

delete from emp01
where first_name = 'Beta';

rollback to sp; -- 롤백 세이브포인트가 생성된 때로 돌아간다 행해졌던 건 다 취소가 된다.

delete from emp01
where first_name ='John';

select * from emp01;

create table emp02( -- DDL create table을 하면 이전에 앞서 했던 명령어들은 자동커밋 aoto commit이 행해지고 나서 테이블이생성한다.
emp_id number primary key, -- 컴퓨터 내부에있는 Dram에 저장이 되어 있는데  dbms으로 데이터을 만들고 실행을 하면 디램에 저장이 되고 commit을 해야 데이터베이스내에 접근을해서 저장이 된다.
-- 만약 commit을 하지 않고 컴퓨터를 끄면 디램에 있는 데이터들은 휘발성이므로 그대로 삭제가 된다. 중요한 데이타면 꼭 commit을 해줘야 한다.
first_name varchar2(50),
salary number);


A 세션
insert into emp02 values(1,'park',9000);
select * from emp02;

B 세션
insert into emp02 values(1,'park',9000);
/* A세션에서 입력중인 데이터는 입력할 수 없다.
  waiting상태가 된다.(LOCK상태 발생) */
  
A세션
   commit;--a상태에 있다 insert를 커밋을 시켜주면 b상태에 있는 insert는 락상태가 해제가 되면서 오류가 뜬다 데이타 베이스카 a상태에 있는 자료가 저장이 되어서
   
B세션
--A세션이 commit이 발생되면 B세션은 해당 행에 대한 LOCK상태가 해제가
-- 되면서 오류발생 ORA-00001: unique constraint (HR.SYS_C007056) violated

B세션
SQL =insert into emp02 values(1,'park',9000);
--ORA-00001: unique constraint (HR.SYS_C007056) --violated  LOCK상태가 걸리는게 아니라 바로 오류가 뜬다.

B세션
SQL> insert into emp02 values(2,'dong',8000);
SQL> select * from emp02

A세션
SQL> insert into emp02 values(2,'dong',8000);
/*B세션에서 입력중인 데이터는 입력할 수 없다.
   waiting 상태가 된다. (LOCK상태가 된다).*/

B세션
SQL> rollback; 

