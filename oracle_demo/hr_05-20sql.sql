
select * from emp;
show user;

grant select,insert,update,delete on emp to user01;
--hr  계정에서 user01계정에 emp객체를 셀렉트 인설트 업데이트 딜리트 할수있는 권한을 부여한다.

--사용자에게 부여된 권한 조회
/*
1) user_tab_privs_made 데이터 디셔너리 : 현재 사용자가 다른 사용자에게 부여한 권한 정보를 알려줌

2) user_tab_privs_recd 데이터 디셔너리 : 자신에게 부여된 권한 정보를 알려줌
*/

-- hr 계정이 다른 소유자에게 부여한 권한 정보 조회
select * from user_tab_privs_made;

/*
사용자에게 권한을 뺏기 위한 RECOKE명령어
: 사용자에게 부여한 권한을 테이터베이스 관리자나 객체 소유자로 부터 철회하기 위해서 사용
revorke object_privilege on object from user_name;*/


-- user01 계정에 부여한 ㄷmp객체를 조회할수 잇는 select 명령문 철회
revoke select on emp from user01;

/*
데이터베이스 롤 권한제어
  롤 : 사용자에게 보다 효율적으로 권한을 부여할 수 있도록 여러
      개의 권한을 묶어 놓은 것
  롤의 종류 : 데이터베이스를 설치하면 기본적으로 제공되는 사전
        정의된 롤과 사용자가 정의한 롤로 구분

  사전 정의된 롤의 종류(db설치시 제공)
   1) CONNECT 롤 : 사용자가 데이터베이스에 접속 가능하도록
         하기 위해서 가장 기본적인 시스템권한 8가지를 정의
      -ALTER SESSION, CREATE CLUSTER, CREATE DATABASE LINK,
       CREATE SEQUENCE, CREATE SESSION, CREATE SYNONYM,
       CREATE TABLE, CREATE VIEW

   2)RESOURCE 롤 : 사용자가 객체(테이블, 뷰, 인덱스)를 생성할
      수 있도록 하기 위해서 시스템 권한을 묶어 정의
      - CREATE CLUSTER, CREATE PROCEDURE, CREATE SEQUENCE,
        CREATE TABLE, CREATE TRIGER
   3)DBA 롤 : 사용자들이 소유한 데이터베이스 객체를 관리하고
       사용자들을 작성하고 변경하고 제거할 수 있도록하는
        모든 권한
*/

--mrole롤에 emp객체 조회할 수 있는 select문 권한 설정
--롤은 시스템에서 만들어야 한다.

grant select on emp to mrole;

select * from emp;