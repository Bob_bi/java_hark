
create table mini_member(
id varchar2(30) primary key,
pwd varchar2(30) not null
);
drop table  mini_member;

select * from mini_member;

create table music_playlist(
num number primary key,
name varchar2(30)
);

insert into music_playlist values(1,'Master_jogam_sentence_001');
insert into music_playlist values(2,'Master_jogam_sentence_002');
insert into music_playlist values(3,'Master_jogam_sentence_003');
insert into music_playlist values(4,'Master_jogam_sentence_004');
insert into music_playlist values(5,'Master_jogam_sentence_005');
insert into music_playlist values(6,'Master_jogam_sentence_006');
insert into music_playlist values(7,'Master_jogam_sentence_007');
insert into music_playlist values(8,'Master_jogam_sentence_008');
insert into music_playlist values(9,'Master_jogam_sentence_009');
insert into music_playlist values(10,'Master_jogam_sentence_010');
insert into music_playlist values(11,'Master_jogam_sentence_011');
insert into music_playlist values(12,'Master_jogam_sentence_012');
insert into music_playlist values(13,'Master_jogam_sentence_013');
insert into music_playlist values(14,'Master_jogam_sentence_014');
insert into music_playlist values(15,'Master_jogam_sentence_015');
insert into music_playlist values(16,'Master_jogam_sentence_016');
insert into music_playlist values(17,'Master_jogam_sentence_017');
insert into music_playlist values(18,'Master_jogam_sentence_018');
insert into music_playlist values(19,'Master_jogam_sentence_019');
insert into music_playlist values(20,'Master_jogam_sentence_020');
insert into music_playlist values(21,'Master_jogam_sentence_021');
insert into music_playlist values(22,'Master_jogam_sentence_022');
insert into music_playlist values(23,'Master_jogam_sentence_023');
insert into music_playlist values(24,'Master_jogam_sentence_024');
insert into music_playlist values(25,'Master_jogam_sentence_025');
insert into music_playlist values(26,'Master_jogam_sentence_026');
insert into music_playlist values(27,'Master_jogam_sentence_027');
select * from music_playlist;
