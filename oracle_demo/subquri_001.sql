-----------------------------------------------------------------------------------------
--서브쿼리(subquery)
-- 하나의 SQL문안에 포함되어 있는 또 다른 SQL문을 말한다.
-- 서브쿼리는 알려지지 않은 기준을 이용한 검색을 위해 사용한다.
-- 서브쿼리는 메인쿼리가 서브쿼리를 포함하는 종속적인 관계이다. (메인쿼리와 서브쿼리의 값이 동일해야 한다.)
-- 서브쿼리는 메인쿼리의 컬럼을 모두 사용할 수 있지만 메인쿼리는 서브쿼리의 컬럼을 사용할 수 없다. 
-- 질의 결과에 서브쿼리 컬럼을 표시해야 한다면 조인방식으로 변환하거나 함수, 스칼라 서브쿼리(scarar subquery)등을 사용해야 한다. 
-- 조인은 집합간의 곱(Product)의 관계이다. 
-- 조인 관계의 태이블은 대등한 관계이다.
 -------------------------------------------------------------------------------
 -- lex가 근무하는 부서명을 출력하시오.
select department_id
from employees
where first_name ='Lex';

select department_name
from departments
where department_id=90;

select d.department_name
from employees e, departments d
where e.department_id=d.department_id
and e.first_name='Lex';

select department_name
from departments
where department_id=
                   (select department_id
                   from employees
                   where first_name='Lex');
                   
--Lex와 같은 부서에 근무하느 사원 이름과 부서번호를 출력하시오

select departmetn_id
from employees
where first_name='Lex';

select first_name
from employees
where department_id=90;

select e.first_name, e.department_id
from employees e, employees m
where e.department_id = m.department_id
and m.first_name ='Lex';


select first_name , department_id
from employees
where department_id=
            (select department_id
            from employees
            where first_name='Lex');
            
--'Lex'와 동일한 업무(job_id)를 가진 사원의 이름(first_name),
-- 업무(job_title), 입사일(hire_date)을 출력하시오.

select e.first_name , j.job_title, e.hire_date
from employees e , jobs j -- 두테이블에 모두 있는 컬럼이 있을때에는 반드시 조인시킬때 명시를 해야한다.
where e.job_id=
           (select job_id -- 서브쿼리에 있는 결과값은 메인쿼리에서 사용할수 없다.
            from employees
           where first_name='Lex')
    and e.job_id = j.job_id;



--'IT'에 근무하는 사원의 이름, 부서번호를 출력하시오.

select first_name, department_id
from employees
where department_id=(
                        select department_id
                        from departments
                        where department_name='IT');
                        
-- 'Bruce'보다 급여를 많이 받은 사원이름(first_name), 부서명 , 급여를 출력하시오
select e.first_name, d.department_name, e.salary
from departments d , employees e
where d.department_id=e.department_id
and e.salary>( -- 비교연산자를 사용하려면 서브쿼리에 있는 결과 행의 결과가 하나여만 가능하다.
            select salary
            from employees
            where first_name='Bruce')
order by e.salary;


--Steven와 같은 부서에서 근무하는 사원의 이름 ,급여 ,입사일을 출력하시오
select first_name, salary, hire_date department_id-- 서브쿼리의 결과가 많은 경우 in연산자를 이용하여 구해야 한다.
from employees 
where department_id in(
                        select department_id
                        from employees
                        where first_name='Steven');
                        
--부서별로 가장 급여를 많이 받는 사원의 이름, 급여, 부서번호를 출력하시오
select first_name, salary, department_id
from employees
where (department_id,salary) in( -- 정확한 열의 갯수가 같고 갑이 일정해야한다.

                                select department_id, max(salary)
                                from employees
                                group by department_id)
order by department_id;

--30소속된 사원들중에서 급여를 가장 많이 받는 사원보다 더 많은 급여를
-- 받는 사원의 이름, 급여, 입사일을 출력하시오.(all)
--(서브쿼리에서 max()함수를 사용하지 않는다.)

select first_name, salary, hire_date
from employees
where salary >all( -- 서브쿼리에 모두만족하는 값들 보다 더 큰 값을 할때는 all을 사용
                select salary
                from employees
                where department_id=30);
                
--부서 번호가 30번인 사원들이 받는 최저급여보다 높은 급여를 받는 사원의 이름,
--급여, 입사일을 출력하시오. (min()함수를 사용하지 않는다.)(any)

select first_name, salary, hire_date -- any는 서브쿼리에 있는 값들중에 최저급여 보다만 크면 값을 뽑을수 있다.
from employees
where salary >any(
                  select salary
                  from employees
                  where department_id=30);
                  
--20번 부서에 속한 사원이 있으면 사원들의 사원명, 입사일, 급여,
--부서번호를 출력하시오(exists)

select first_name, hire_date,salary
from employees
where exists(select department_id -- 서브쿼리에 있는 결과가 true이면 무조건 메인쿼리에 있는값을 가져온다.
             from employees
             where department_id=20);
             
-----------------------------------------------------
-- top-n 서브쿼리
/*  상위의 값을 추출할때  사용된다.
  <, <=연산자를 사용할 수 있다. 단 비교되는 값이 1일때는 =도 가능하다.
  order by절을 사용할 수 있다.*/
-----------------------------------------------------
--급여가 가장높은 상위 3명을 검색하시오
select emp.first_name, emp.salary -- * 이걸로도 가능
from (select first_name, salary -- from절에 있는 select절에 있는 것만 메인쿼리에 있는 select절에 사용가능하다.
      from employees
      order by salary desc) emp
where rownum<=3; -- 결과 출력번호(행의번호)를 나타내주는 값을 의미함

--급여가 가장 높은 상위 4위부터 8위까지 검색하시오
select em.*
from (select first_name, salary
      from employees
      order by salary desc) em
where rownum <=8
minus
select em.*
from (select first_name, salary
      from employees
      order by salary desc) em
where rownum <=3;
--선생님꺼
select e.*
from ((select em.*,rownum as rm
     from (select first_name, salary
           from employees
    order by salary desc) em))e
where rm >=4 and rm<=8;-- 몇번부터 몇번까지 출력하기 위해선 서브쿼리를 이용해야 한다. 오라클에서 제공이 되는 출력번호!!!! 기억해야함!!!

-- 월 별 입사자 수를 조회하되 입사자수가 가장 많은 상위 3개의 달만 출력되도록 하시오
-- <출력 : 월    입사자수 >

select *
from (select to_char(hire_date, 'mm')as "월",count(*) as "입사자수"
        from employees
        group by to_char(hire_date, 'mm')
        order by "입사자수" desc)
where rownum <=3
order by "월";

-------------------------------------------------------------
/* 상관관계 서브쿼리
 : 서브쿼리에서 메인쿼리의 컬럼을 참조한다.(메인쿼리를 먼저수행한다.)
  서브쿼리는 메인쿼리 각각의 행에 대해서 순서적으로 한번씩 실행한다.
<아래 쿼리 처리순서>
1st : 바같쪽 쿼리의 첫째 row에 대하여
2an : 안쪽 쿼리에서 자신의 속해있는 부서의 MAX salary과
      비교하여 true이면 바깥의 컬럼값을 반환하고,
      false이면 값을 버린다.
3rd : 바깥쪽 쿼리의 두 번쩨 row에 대하여 마찬가지로 실행하며,
      이렇게 바깥쪽 쿼리의 마지막 row까지 실행한다.*/
--------------------------------------------------------------
--부서별 최고 급여를 받는 사원을 출력하시오.
/*6*/ select first_name, salary, department_id
/*1*/ from employees e
/*5*/ where salary=(
/*4*/      select max(salary)
/*2*/      from employees
/*3*/       where department_id=department_id     )
/*7*/order by department_id;

select employee_id, first_name,department_id
from employees;