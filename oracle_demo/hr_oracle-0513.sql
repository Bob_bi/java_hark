
--집합연산자
--- 둘 이상의 query결과를 하나의 결과로 조합한다.
--- select의 인자 갯수가 같아야 한다.
--   첫번째 select가 2개이면 두번째 select도 2개여야 한다.
--- 타입이 일치해야 한다.
--  예를 들어 character이면 character이여야한다.

--집합연산자 종류
--union(합집합) - 중복행이 제거된 두 query
--union all(합집합) - 중복행이 포함된 두 query
--intersect(교집합) 두 쿼리에 공통적인 행
--minus(차집합) - 첫번째 쿼리에 있는 행 중 두번째 쿼리에 없는 행표시

--집합연산자 사용이유
--서로 다른 테이블에서 유사한 형태의 결과를 반환하기 위해서
--서로 같은 테이블에서 서로 다른 질의를 수행하여 결과를 합치기 위해서

--========================================================
--nuino(합집합)
select department_id, first_name, last_name
from employees
where department_id=20 or department_id=40
 union
select department_id, first_name, last_name -- 안에 있는 데이터가 같아야 한다
from employees
where department_id=20 or department_id=60;


--union all(합집합)
select department_id, first_name, last_name
from employees
where department_id=20 or department_id=40
 union all
select department_id, first_name, last_name -- 안에 있는 데이터가 같아야 한다
from employees
where department_id=20 or department_id=60;

--intersect(합집합)
select department_id, first_name, last_name
from employees
where department_id=20 or department_id=40
 intersect
select department_id, first_name, last_name -- 안에 있는 데이터가 같아야 한다
from employees
where department_id=20 or department_id=60;

--minus(차집합)
select department_id, first_name, last_name
from employees
where department_id=20 or department_id=40
 minus
select department_id, first_name, last_name -- 안에 있는 데이터가 같아야 한다
from employees
where department_id=20 or department_id=60;

--=========================================================
--SQL(structured query language)함수
--1.단일함수 : 행 하나당 하나의 결과를 출력한다.
--        (문자함수, 숫자함수, 날짜함수, 변환함수, 일반함수)
--2.복수행함수 : 행 여러개당 하나의 결과를 출력한다
--        (합계, 평균, 최대, 최소, 갯수)
--==========================================================
-- SELECT문에서는 반드시 테이블명을 명시해야 한다.
-- 그래서 SELECT절에서 식이나 특정함수를 이용해서 결과값을 가져올때
-- 사용할 수 있는 DUAL이라는 테이블을 제공하고 있다.

select 3+1-- 특정한 식이나 값을 구할때 dual사용
from dual;

select sysdate --sysdate 오라클에서 제공되는 오늘의 날짜 테이블
from dual;

--단일행함수 중 문자함수
-- 단어의 첫글자만 대문자로 변경해주는 함수이다.
select initcap('korea hello')
from dual;

select upper('korea') -- 문자를 전부 대문자로 변경
from dual;

select first_name, upper(first_name),
         last_name, upper(last_name)
from employees;

--모든 문자를 소문자로 변경해주는 함수이다.
select lower('KOREA')
from dual;

select first_name, lower(first_name),
       last_name , lower(last_name)
from employees;

--employees테이블 first_name에서 대소문자 구분없이
-- 'Ja'가 포함이 된 first_name, salary을 출력하라.

select first_name, salary
from employees
where upper(first_name) like upper('%JA%'); -- like 포함된 대소문자 구분없이 하려면 미리 대문자나 소문자로 변경후 뽑는게 좋다.

--문자의 길이를 리턴해주는 함수이다.
select length('KOREA')
from dual;

select length('한국')
from dual;

--문자의 길이를 바이트로 구해서 리턴해주는 함수
select lengthb('korea')
from dual;

select lengthb('한국') --한글은 한글자당 3바이트이기때문에 6바이트가 나온다.
from dual;

create table user1(
     data varchar2(5)
);

insert into user1(data)
values('korea');

insert into user1(data) --ORA-12899: value too large for column 
--"HR"."USER1"."DATA" (actual: 11, maximum: 5) 오류 메세지 크기는 5바이트지만 실제로는 11바이트가 들어가서 오류뜸
values('south korea');

insert into user1(data)--ORA-12899: value too large for column "HR"."USER1"."DATA" (actual: 6, maximum: 5) 한글은 한글자당 3바이트이기때문에 계산을 잘해서 적용해야 한다.
values('한국');

--특정범위의 문자를 추출해 주는 함수이다.
-- substr('문자','시작위치','갯수')
select substr('oracle test', 1, 4)
from dual;
-- 뒤에서 부터 가져오기
select substr('oracle test', -3, 4)
from dual;

select substr('oracle test', -3, 1)
from dual;

select substr('오라클 테스트', 3, 4)
from dual;

select substrb('oracle test', 2, 4)
from dual;

select substrb('오라클 테스트', 1, 6) --공백 1바이트 특수문자 1바이트 한글만 3바이트
from dual;

--특정문자의 인덱스를 추출해주는 함수이다
select instr('korea' , 'or')
from dual;

select instr('한국자바' , '자')
from dual;

--특정문자의 바이트 인덱스를 추출해주는 함수이다.
select instrb('korea','or')
from dual;

select instrb('한국자바' , '자')
from dual;

--주어진 문자열에서 왼쪽으로 특정문자를 채우는 함수이다.
select lpad('korea',8,'*')
from dual;

--주어진 문자열에서 오른쪽으로 특정문자를 채우는 함수이다.
select rpad('korea',8,'*')
from dual;

--주어진 문자열에서 왼쪽의 특정문자를 삭제하는 함수이다.
select ltrim('***korea','*')
from dual;

--주어진 문자열에서 오른쪽의 특정문자를 삭제하는 함수이다.
select rtrim('korea****','*')
from dual;

--주어진 문자열에서 양쪽 특정문자를 삭제하는 함수이다.
select trim('   korea') -- 공백제거
from dual;
select trim('*' from '**korea***') -- 특정문자 제거
from dual;

-- 주어진 문자열에서 오른쪽의 공백제거 확인방법
select length(rtrim('korea    '))
from dual;

--주어진 문자열에서 양쪽의 공백제거
select length(trim(' ' from '   korea    '))
from dual;
select length(trim('   korea    '))
from dual;

--주어진 문자의 아스키 코드값을 구하는 함수이다.
select ascii('A')
from dual;

--주어진 아스키 코드값의 문자를 구하는 함수이다.
select chr(65)
from dual;

--주어진 문자를 연결하는 함수이다.
SELECT CONCAT('java','jsp')
from dual;

select 'java' || 'jsp'
from dual;

--숫자함수
--3.55을 소수점 1의 자리까지 구하시오(반올림) 
select round(3.55, 1)
from dual;

select round(42523.55, -1)
from dual;

select round(42523.55, 0)
from dual;

--256.78을 무조건 올림한다(올림)
select ceil(256.78)
from dual;

--289.78에서 소수 이하는 무조건 버림
select floor(289.78)
from dual;

--2의 3승(거듭제곱)
select power(2, 3)
from dual;

--25의 제곱근
select sqrt(25)
from dual;

--나머지
select mod(10, 3)
from dual;

--날짜함수
--현재 시스템에서 제공해주는 오늘의 날짜 구하는 함수
select sysdate
from dual;

select sysdate+1
from dual;

-- 첫번째 인자의 달에 두번째 인자값을 더한 날짜를 반환
select add_months(sysdate, 10)
from dual;

--===========================================
/* 변환형 함수
   숫자             문자             날짜
   to_number() <-> to_char() <-> to_date()   숫자와 날짜를 직접적으로 바꿀순 없다 숫자에서 문자 문자에서 날짜 날짜에서 문자 문자에서 숫자는 가능하다.
*/
--===========================================
--1 숫자 ->문자
--첫번째 인자값을 두번쨰 인자값의 형식으로 변환해주는 함수
select to_char(2532, '99999,999.9') -- 9는 숫자를 표현해주고 ,는 천자리.는 소수점자리이다.
from dual;

select to_char(2532, '009,999.00')-- 표현형태가 0으로 할경우 다 찍혀서 나온다.
from dual;

--각 나라의 통화를 표현해 줄 때 L기호를 사용한다.
select to_char(2532, 'L999,999.99')
from dual;

--2 날짜 -> 문자
select to_char(2532, 'L999,999.99')
from dual;

select to_char(sysdate, 'yyyy-mm-dd hh:mi:ss day') -- day요일이  다나옴 dy는 요일 앞글자만 나옴
from dual;

select to_char(sysdate, 'yyyy-mm-dd hh:mi:ss dy')
from dual;

select to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss dy') -- 24시간 시간을 표현
from dual;

--to_number()
--   문자 -> 숫자

select '253' || '12' --문자열 +으로 결과 나옴
from dual;

select concat('253','12')
from dual;

select '253' + '12' --숫자형으로 + 결과나옴
from dual;

select to_number('253')+to_number('12')
from dual;

--to_date()
--문자 -> 날짜

select to_date('2013-10-14')
from dual;

select first_name, hire_date
from employees;

--hire_date(입사일)을 이용해서
-- '홍길동님은 2003년 1월 3일에 입사했습니다'로
--출력하는 queryf를 작성하시오

select first_name|| '은 ' || ltrim(to_char(sysdate, 'yyyy'),'0')||'년 '||ltrim(to_char(sysdate, 'mm'),'0')||'월 '||ltrim(to_char(sysdate, 'dd'),'0')|| '일 ' ||'에 입사했습니다,' as "입사일" --as컬럼명 재설정
from employees;

select '홍길동님의' || to_char(2003, 9999) || '년' || to_char(1, 9) || '월' || to_char(3, 9) || '일에 입사했습니다.' as hire_date
from dual;
------------------------------------------
--일반함수
--nvl() 자주 사용함 꼭 기억해둘것
-----------------------------------
--nvl(컬럼, 대체값) : 첫번째 인자값이 null이면 0으로 대체해서 출력한다.
-- 대체할 값이 숫자이면 두번째 인자값에 숫자를 지정한다.
-- 대체할 값이 문자이면 두번째 인자값에 문자를 지정한다.
-- 대체할 값이 날짜이면 두번째 인자값에 날짜를 지정한다.

--commission_pct컬럼의 값이 null인 경우 0으로 대체해서 출력한다.
select commission_pct, nvl(commission_pct, 0)
from employees;

-- null값이 들어가있는 칼럼을 문자로 출력하고싶다. 숫자를->문자
select first_name, nvl(manager_id,'CEO')
from employees;

select first_name, manager_id,nvl(to_char(manager_id),'CEO')
from employees;

--nvl2(컬럼, 대체1, 대체2) : 컬럼의 값이 null아니면 대체 1로,
--null이면 대체2로 출력한다.
select commission_pct, nvl2(commission_pct,1,0)
from employees;

-----------------------
--일반함수
--decode()
------------------------
--decode(컬럼,조건1,값1, 조건2, 값2, 조건3, 값3, 그밖의 값)
--department_id값이 10이면 'AA', 20이면 'BB' 30이면 'CC',
-- 그 밖의 값은 'DD'
--java의 switch_case문과 비슷

select first_name, department_id,
            decode(department_id, '10','ACCOUTING','20','RESEARCH',
            '30','SALES','40','OPERATIONS','OTHERS') as "code"
from employees;

--직급이 'PR_REP' 인 사원은 5%, 'SA_MAN'인 사원은 10%
-- 'AC_MGR'인 사원은 15%, 'PU_CLERK' 인 사원은 20%를 인상
select job_id, salary,decode(job_id,'PR_REP',salary*1.05,'SA_MAN',salary*1.1,
                'AC_MGR',salary*1.15,'PU_CLERK',salary*1.2,salary) as 급여
from employees;

-------------------------------------
-- 일반함수
-- case()
-------------------------------------
/*case when 조건1 then 결과1
     when 조건2 then 결과2
     when 조건3 then 결과3
     else 결과n
end*/
-- 자바의 다중 if~else제어문과 비슷하다.
select first_name, department_id,
        case when department_id=10 then 'ACCOUNTING'
             when department_id=20 then 'RESEARCH'
             when department_id=30 then 'SALES'
             when department_id=40 then 'OPERATIONS'
             else 'OTHERS'
        end as code
from employees;

--입사일에서 월이 1-3이면 '1사분기', 4-6이면 '2사분기',
--   7-9이면 '3사분기', 10-12이면 '4사분기'
--로 처리를 하고 사원면(first_name),
--입사일(hire_date), 분기로 출력하시오.

select first_name, hire_date,
        case when to_char(hire_date, 'mm') <= 3 then '1사분기'
             when to_char(hire_date, 'mm') <= 6 then '2사분기'
             when to_char(hire_date, 'mm') <= 9 then '3사분기'
             when to_char(hire_date, 'mm') <= 12 then '4사분기'
        end as "분기"
from employees;

-----------------------------------
--그룹함수
------------------------------------
--max(컬럼) : 최대값
select max(salary)
from employees;

--min(컬럼) : 최소값
select min(salary)
from employees;

--count(컬럼) : 개수
select count(commission_pct)--컬럼에서 null 값을 제외한 데이터가 입력되어있는 횡의 갯수
from employees;

select count(*) -- 현재테이블에 저장되어있는 데이터가 있는 행의 갯수
from employees;

--sum(컬럼) : 합계
select sum(salary)
from employees;

--avg(컬럼) : 평균
select avg(salary)
from employees;

--집계함수와 단순컬럼은 함께 사용 할 수 없다.(출력되는 레코드수가 다르기 때문이다)
--ORA-00937: 단일 그룹의 그룹함수가 아닙니다.
select first_name, count(*) --단일 함수와 복수함수는 같이 사용할수가 없다.
from employees;
--그룹함수와 단순컬럼을 사용하기 위해서는
--단순컬럼을 그룹화 해야 한다.(group by)
select department_id, count(*)
from employees
group by department_id
order by department_id;--오름차순일때 널값은 제일 아래로 간다.

--50이하인 부서에 대해서 null이 아닌 부서별의 직원수를 출력하시오
select department_id, count(*)
from employees
where department_id is not null
group by department_id
having department_id<=50
order by department_id;

--부서별 최소급여, 최대급여를 출력하시오.(부서별 오름차순)
select department_id, max(salary), min(salary)
from employees
where department_id is not null
group by department_id 
ORDER BY department_id;

--업무별(job_id) 급여합계를 출력하시오
select job_id, sum(salary)
from employees
group by job_id
order by job_id;

