package java0528_gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java0527_jdbc.EmpDAO;
import java0527_jdbc.EmpDTO;

class DesignTest2 extends JFrame implements ActionListener{
	
	JTextField inputTxt;
	JButton searchBtn;
	JTable table;
	DefaultTableModel model;
	JComboBox<String> jcom;
	
	public DesignTest2() {
		
		String[] item = new String[] {"전체", "사원명", "급여"};
		jcom = new JComboBox<String>(item);
		
		inputTxt = new JTextField(20);
		searchBtn = new JButton("검색");
		
		JPanel jp = new JPanel();
		jp.add(jcom);
		jp.add(inputTxt);
		jp.add(searchBtn);
		
		//테이블 헤드
		Object[] obj = {"사원번호", "사원명", "급여", "입사일"};
		model = new DefaultTableModel(obj, 0);
		table = new JTable(model);
		
		//라인의 높이
		table.setRowHeight(20);
		
		//컬럼이동 불가능
		table.getTableHeader().setReorderingAllowed(false);
		
		add(BorderLayout.NORTH, jp);
		add(BorderLayout.CENTER, new JScrollPane(table));
		//버튼에 ActionListener연결
		searchBtn.addActionListener(this);
		
		setSize(500, 400);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		EmpDAO dao = EmpDAO.getInstance();
		int index = jcom.getSelectedIndex(); // 컴보박스 인덱스번호 나타내기
		String combox = (String)jcom.getSelectedItem(); // 컴보박스 내의 스트링갑 나타내는것
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(combox,(Object)inputTxt.getText());
		
		System.out.printf("%d:%s\n", index, combox);
		List<EmpDTO> aList = dao.comboxSearchMethod(map);
		
		if(aList.size()==0) {
			JOptionPane.showMessageDialog(this, "검색결과가 없습니다.");
			return;
		}
		
		//테이블 초기화
		model.setRowCount(0);
		
		for(EmpDTO dto:aList) {
			Object[] line = new Object[4];
			line[0] = dto.getEmployee_id();
			line[1] = dto.getFirst_name();
			line[2] = dto.getSalary();
			line[3] = dto.getHire_date();
			model.addRow(line);
		}
		
		//TextField 초기화
		//inputTxt.setText("");
		inputTxt.requestFocus();//
		inputTxt.select(0,inputTxt.getText().length());//커서 포인터 제자리로 돌리기
	}//end actionPerformed()
}//end DesignTest

public class Java217_gui {

	public static void main(String[] args) {
		
		new DesignTest2();
	}
}
