package java0528_gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class UserSize extends Frame{
	Button btn;
	public UserSize() {
		//Frame컨테이너의 Layout을 null로 초기화한다
		setLayout(null); //기본layout사용안할시 null
		
		btn = new Button("button");
		
		//가로위치, 세로위치, 가로크기, 세로크기
		//btn.setBounds(50, 50, 200, 150);
		
		btn.setSize(200, 150);
		btn.setLocation(50, 50);
		add(btn);
		
		setSize(300, 300);
		setVisible(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
	}//end UserSize()
}//end Usersize
public class Java211_gui {

	public static void main(String[] args) {
		
		new UserSize();
	}
}
