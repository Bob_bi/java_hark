package java0528_gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class FlowLayoutTest extends Frame{
	Button oneBtn, twoBtn, threeBtn;
	Panel pn;
	TextArea ta;
	
	public FlowLayoutTest() {
		
		oneBtn = new Button("one");
		twoBtn = new Button("two");
		threeBtn = new Button("three");
		
		//Panel의 기본 레이아웃은 FlowLayout이다.
		//FlowLayout에 컴포넌트를 연결할때는 add해준 순서대로
		//왼쪽에서 오른쪽으로 연결된다.
		pn = new Panel();
		pn.add(oneBtn);
		pn.add(twoBtn);
		pn.add(threeBtn);
		
		ta = new TextArea(10, 20); //센터로 배치하면 파라미터 의미x
		
		this.add(BorderLayout.NORTH, pn);
		this.add(BorderLayout.CENTER, ta);
		
		setSize(500, 400);
		setVisible(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
	}//end FlowLayoutTest()
}//end FlowLayoutTest////
public class Java210_gui {

	public static void main(String[] args) {
		new FlowLayoutTest();
	}//end main
}//end class
