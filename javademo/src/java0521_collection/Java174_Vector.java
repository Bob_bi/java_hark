package java0521_collection;

import java.util.Vector;

public class Java174_Vector {

	public static void main(String[] args) {
		//백터를 이용하여 만들면 필요없는 구역이 생길수 있다. 필요없는 메모리구역은 삭제할수 있다.
		Vector<String> v = new Vector<String>(); // 초기값으로 20개가 생성이 되고 그 이상이 되면 2가 추가된다.
		//백터를 지정안하면 기본적인 10이다.
		v.addElement(new String("java"));// append (뒤에 추가되는것)
		v.add(new String("jsp"));// append (뒤에 추가되는것)
		v.add(1,new String("spring"));// 첫번째 인자는 인댁스번호 두번째 인자값은 인댁스번호에 두번째 인자값을 insert해준다
		
		
		v.trimToSize();// 남는 용량 제거해줄때 사용함.(요소가 저장되어 있지 않은 메로리 제거)
		System.out.println("용량 크기:"+v.capacity());
		System.out.println("요소 갯수:"+v.size());
		
		for(int i=0; i<v.size(); i++)
			System.out.printf("v[%d]=%s\n",i,v.get(i));

	}//end main()

}//end class
