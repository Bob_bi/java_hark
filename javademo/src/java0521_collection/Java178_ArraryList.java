package java0521_collection;

import java.util.ArrayList;

public class Java178_ArraryList {

	public static void main(String[] args) {
		ArrayList<String> aList = new ArrayList<String>();
		aList.add(new String("oracle"));
		aList.add(new String("mysql"));
		aList.add(new String("mssql"));
		String[] arr = displary(aList);
		for(String data : arr)
			System.out.println(data);

	}//end main()
	
	public static String[] displary(ArrayList<String> aList) {
		/*String[] arr = new String[aList.size()];		
		for(int i=0; i<(aList.size()-1); i++) {
			arr[i] = aList.get(i);
		}
		return arr;*/
		
		return aList.toArray(new String[aList.size()]);// toArray()메소드 매개값에있는 유형대로 그값으로 리턴을 해준다.
	}

}//end class
