package java0521_collection;

import java.util.Vector;

public class Java170_vector {

	public static void main(String[] args) {
		Vector v = new Vector();
		//Integer -> Object : up-casting
		v.add(new Integer(10));
		v.add(new Integer(20));
		v.add(new Integer(30));
		
		//int -> Integer -> Object
		//otoboxing 이 일어나 업개스팅이 일어남
		v.add(40);
		
		//Object -> Integer :down_casting
		Integer it1=(Integer)v.get(0);//복사해서 가져옴
		Integer it2=(Integer)v.get(1);
		Integer it3=(Integer)v.get(2);
		Integer it4=(Integer)v.get(3);
		
		System.out.println(it1);
		System.out.println(it2);
		System.out.println(it3);
		
		
		
		
	}//end main()

}//end class
