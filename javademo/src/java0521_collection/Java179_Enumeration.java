package java0521_collection;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

//Enumeration은 Vector와 Hashtable에서만 제공이 되는 인터페이스이다.
public class Java179_Enumeration {

	public static void main(String[] args) {
		
		Vector<Integer> v = new Vector<Integer>();
		v.add(new Integer(10));
		v.add(new Integer(20));
		v.add(new Integer(30));
		
		Enumeration<Integer> enu =v.elements(); // 형재 백터에 저장되어있는 요소들은 나열해준다.
		while(enu.hasMoreElements())
			System.out.println(enu.nextElement());// 열거형 벡터와 헤쉬테이블에서만 제공되는 클래스
		
		
		//반복자
		Iterator<Integer> ite = v.iterator();
		while(ite.hasNext())
			System.out.println(ite.next());
		
		// 개선된 루프
		for(Integer i : v)
			System.out.println(i);

	}//end main()

}//end class
