package java0521_collection;

import java.util.Vector;

/*
 * 홍길동 30
 * 이영희 25
 * 
 */
public class Java173_Vector {

	public static void main(String[] args) {
		Person p1 = new Person("홍길동", 30);
		Person p2 = new Person("이영희", 25);
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		//==================================
		System.out.println("==========================");
		Person[] ps = new Person[2];
		ps[0] = new Person("홍길동",30);
		ps[1] = new Person("이영희",25);
		for(Person p: ps)
			System.out.println(p.toString());
		
		
		
		
		//컬렉션 (자료구조를 구현해 놓은것을 컬렉션이라한다.)
		System.out.println("==============================");
		Vector<Person> vs = new Vector<Person>();
		vs.add(new Person("홍길동",30));
		vs.add(new Person("이영희",25));
		vs.add(new Person("류뜡", 33));
		for(Person p : vs) {
			System.out.println(p.toString());
		}

	}//end main()

}//end class
