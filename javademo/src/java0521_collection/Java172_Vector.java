package java0521_collection;

import java.util.Vector;

/*
 * 배열 : length키워드
 * 문자열 : length() 메소드
 * 컬렉션 : size()메소드 
 */

/*
 * 컬렉션 클래스를 선언하고 생성할 때 제너릭을 선언한다.
 * 제너릭(generic) : 컬렉션 클래스에 저장된 객체를 가져올때
 *  다운캐스팅하는 작업을 생략할 수 있도록 제공해주는 기능이다.
 */

public class Java172_Vector {

	public static void main(String[] args) {
		Vector<String> v = new Vector<String>();// <유형>에서 유형을 쓰면 백터에서 꺼내올때 다운캐스팅을 안해도 자동으로 된다. (끝에 있는것 생략도 가능)
		v.add(new String("java"));
		v.add(new String("jsp"));
		v.add(new String("spring"));
		
		//일반 반복문
		System.out.println("/////////일반 반복문//////////");
		for(int i=0; i<v.size(); i++) {
			String data=v.get(i);
			System.out.println(data);
		}
		//향상된 포문
		System.out.println("///////////개선된 루프///////////");
		for(String st : v)
			System.out.println(st);
		
		//10 10.4 4.8f
		//Integer, double, folat 의 상위 객체 Number
		Vector<Number> vt =new Vector<Number>();
		//10(Integer) -> Number -> Object(up-casting)
		vt.add(new Integer(10));
		//10.4(Double) -> Number -> Object(up-casting)
		vt.add(new Double(10.4));		
		//4.8(Float) -> Number -> Object(up-casting)
		vt.add(new Float(4.8f));
		
		Integer it;
		Double de;
		Float ft;
		// instanceof
		//내꺼
		for(int i=0; i<vt.size(); i++) {
			if(vt.get(i) instanceof Integer) {
				System.out.println(it=(Integer)vt.get(i));		
			}else if(vt.get(i) instanceof Double) {
				System.out.println(de=(Double)vt.get(i));				
			}else if(vt.get(i) instanceof Float) {
				System.out.println(ft=(Float)vt.get(i));
				
			}
		}
		
		//선생님꺼
		for(Number ne : vt) {
			if(ne instanceof Integer) {// 비교하는 객체의 자신이거나 하위 클래스만 올수있다.
				System.out.println((Integer)ne);	
			}else if(ne instanceof Double) {
				System.out.println((Double)ne);					
			}else if(ne instanceof Float) {
				System.out.println((Float)ne);									
			}
		}
				

	}//end main()

}//end class
