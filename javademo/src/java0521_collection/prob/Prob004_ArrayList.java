package java0521_collection.prob;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * 2 
 * 10 
 * 4 
 * 6
 */
public class Prob004_ArrayList {

	public static void main(String[] args) {
		int[] arr = { 5, 9, 3, 2, 7 };
		int[] num = { 1, 10, 15, 4, 6 };
		ArrayList<Integer> v = merge(arr, num);
		for (Integer it : v)
			System.out.println(it);
	}// end main()

	public static ArrayList<Integer> merge(int[] arr, int[] num) {
		// arr,num배열을 병합한후 2의 배수만 리턴하는 프로그램을 구현하시오.
		ArrayList<Integer> ar = new ArrayList<Integer>();
		int som=0;
		for(int a : arr) {
				ar.add(a);				
		}
		for(int a : num) {
				ar.add(a);
		}
		
		Iterator<Integer> iter = ar.iterator();
		while(iter.hasNext()) {
			Integer s = iter.next();
			if(s%2==1) {
				iter.remove();				
			}
		}
		
		return ar;

	}// end merge();

}






