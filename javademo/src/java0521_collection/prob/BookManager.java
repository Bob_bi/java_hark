package java0521_collection.prob;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class BookManager {
	public static int getRentalPrice(ArrayList<BookDTO> bookList, String kind) {
		// 구현하세요.
		int sum=0;
		for(BookDTO b : bookList) {
			if(b.getKind().contains(kind)) {
				sum+=b.getRentalPrice();
			}
		}
	
		
		return sum;
	}//end getRentalPrice()
}//end class






