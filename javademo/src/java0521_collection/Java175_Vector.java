package java0521_collection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.util.Vector;
/* [프로그램 출력결과]
kim    56  78   12  146
hong   46  100  97  243
park   96  56   88  240
*/
public class Java175_Vector {

	public static void main(String[] args) {
		String sn = "./src/java0521_collection/score.txt";
		Vector<Sawon> vt = lines(sn);
		prnDisplay(vt);
	}// end main()

	private static Vector<Sawon> lines(String fileName) {
		// strName 매개변수의 값을 이용해서 Vector에 데이터를 저장한후
		// 리턴하는 프로그램을 구현하시오.
		
		Vector<Sawon> vt = new Vector<Sawon>(); // 벡터 객체를 가져와서 배열의 형태로  저장한다. 하나의 객체는  객체안에있는 멤버 메소드 다 사용가능.
		Scanner sc;
		try {
			sc = new Scanner(new File(fileName));
			while(sc.hasNextLine()) {
				String stn=sc.nextLine();
				String[] data=stn.split("[:/]");
				Sawon sn = new Sawon(data[0],Integer.parseInt(data[1]),
						Integer.parseInt(data[2]),
						Integer.parseInt(data[3]));
				
				vt.add(sn);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return vt;
		
//		Vector<Sawon> v =new Vector<Sawon>();		
//		RandomAccessFile ref = null;
//		String num=null;
//		try {
//			String str=null;
//			ref = new RandomAccessFile(fileName, "r");
//			while((str=ref.readLine())!=null) {
//				String[] strim = str.split("[:/]");
//				Sawon sa = new Sawon(strim[0],Integer.parseInt(strim[1]),
//						Integer.parseInt(strim[2]),Integer.parseInt(strim[3]));
//				v.add(sa);
//			}
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return v;
	
	}//end lines()
	
	private static void prnDisplay(Vector<Sawon> vt) {
		for(Sawon s : vt) {
			System.out.println(s.toString());
		}
		
	
	}//end prnDisplay()
	
}// end class
