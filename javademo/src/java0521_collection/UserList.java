package java0521_collection;

import javax.swing.text.AbstractDocument.LeafElement;

/*
 * 선형리스트(Linear List)
 * 1 순서리스트(Ordered List)
 * 2 자료들 간에 순서를 갖는 리스트
 * 3 선형 리스트에서 원소를 나열한 수서는 원소들의 순서가 된다.
 */

//연결리스트 하나의 노드(배열과 비슷함)를 만들고 연결하는 방식
public class UserList {
	
	Object sale[] = new Object[3];// 오브젝트 배열 생성
	int point=0;
	
	public void add(Object data) {
		if(sale.length==point) {
			Object[] arr = new Object[sale.length*2];
			System.arraycopy(sale, 0, arr, 0, sale.length);
			sale =arr;
		}
		sale[point++]=data;
	}//end add
	
	public Object get(int index) {
		return sale[index];
	}
	
	//요소의 크기(백터에 들어간 객채의 크기)
	public int size() {
		return point;
	}
	// 요소 제거
	public Object remove(int index) {
		Object obj = sale[index];
		for(int i=index; i<point; i++) {
			sale[i] = sale[i+1];
		}
		point--;
		return obj; 
	}

}//end class
