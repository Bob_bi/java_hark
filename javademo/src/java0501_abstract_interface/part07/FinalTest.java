package java0501_abstract_interface.part07;

public class FinalTest {
	final int CODE=1; //final이 붙은 필드는 초기값을 할당해줘야 한다.
	
	void display(){
		// 상수는 새로운 값을 할당 할 수 없다.
		//CODE=5;
	}
	
	void call() { // final이 안붙은 메소드
		System.out.println("call");
	}
	
	final void process() { // final이 붙은 메소드
		System.out.println("process");
	}

}//end class
