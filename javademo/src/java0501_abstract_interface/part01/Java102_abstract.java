package java0501_abstract_interface.part01;

/*
 * 클래스 종류 : 클래스, 추상 클래스, 인터페이스
 *  메소드 정의 : 메소드 선언부 + 메소드 구현부
 *  public void prn(){}
 * 
 * 추상메소드 : 선언부만 정의되어 있는 메소드.
 * abstract public void prn();
 * 
 * 추상 클래스 : 추상메소드를 가지고 있는 클래스이다.
 *  abstract public class Test{}
 *  
 *  추상클래스 제공 목적 : 추상메소드를 강제적으로 오버라이딩하고
 *      객체생성을 할 수 없도록 하기 위해서.
 */

/*
 * Car : 속도 , 샛상, 움직인다. 속도를 높인다. 멈춘다. work()
 * Senda extends Car:
 * Truck extends Car;
 * 
 */
public class Java102_abstract  {

	public static void main(String[] args) {
		//추상 클래스는 객체 생성을 할 수 없다.
		//CarAbs sc =new CarAbs();
		
		TruckAbs truck = new  TruckAbs();
		truck.work();
		
		SendaAbs senda = new SendaAbs();
		senda.work();
			
		

	}//end main()

}//end class
