package java0501_abstract_interface.part01;

abstract public class CarAbs { // 추상 클래스에도 abstract을 붙여줌   0개 이상 메소드를 가지고 있는 클래스 추상클래스가 됨.
	int speed;
	String color;
	
	abstract public void work();
	// 클래스에 abstract를 붙이는 순간 추상클래스가 되어서 메소드에 abstract를 안붙여도 상관없다.
	// 추상메소드(abstract method) abstract 추상메서드 키워드
	
	public void display() {
		System.out.printf("%d %s\n",speed, color);
	}

}
