package java0501_abstract_interface.part04;

/*
 * 인터페이스(interface)
 * 1.추상메소드와 상수를 가지고 있는 형태이다. (abstract을 안붙여도 된다.) 멤버변수는 상수만 제공이 된다. 인터페이스에 있는 메소드는 전부 추상메소드이다.
 * 2.클래스 작성에 도움을 쥐 위한 목적으로 제공이 되는 표준명세서이다.
 * 3.@@implements@@ : 클래스에 인터페이스를 상속할 때 사용되는 키워드이다.
 * 4.인터페이스는 접근제어자로@@ public@@만 제공한다.
 * 5.인터페이스의 추상메소드에 abstract키워드를 명시하지 않아도 된다.
 *   자바가상머신에서 자동으로 처리하기 때문에.
 * 6.모든 인터페이스의 메소드는 @@묵시적으로 @@public abstract@@이다.
 * 7.필드는@@묵시적으로 @@public static final@@이다.                    @@: 중요!!! 시험에 나옴!!
 * 8.@@객체 생성은 하지 못하지만,@@ 참조형 타입으로서는 사용 가능하다.               서로 성격이 다른 클래스 객체끼리 연결할때 인터페이스를 사용한다.
 */


/*
 *              class                interface
 * 멤버변수                     o                    상수
 * 메소드                   선언부+구현부                            선언부(추상메소드)
 * 생성자                         o                      x
 * 초기화블록(static) o                      x
 * 내부클래스                   o                      o
 * 
 * extends : 클래스에 클래스를 상속(단일상속), 인터페이스에 인터페이스 상속(다중상속)
 * implements : 클래스에 인터페이스 상속(다중상속)
 * 
 * 추상클래스와 인터페이 차이점
 * 1. 추상 클래스는 단일 상속만 가능하지만 인터페이스는 다중 상속이 가능하다.@@
 * 2. 추상 클래스는 구현시에는 extends 키워드를 사용하지만, 인터페이스 구현시에는 implements 키워드를 사용하지만, 인터페이스 구현시에는 implements 키워드를 사용한다.@@
 * 3. 추상 클래스는 abstract메소드가 0개 이상 존재하지만, 인터페이스는 모든 메소드가 abstract이다.@@
 * 4. 추상 클래스는 명시적으로 abstract를 사용해야 하지만, 인터페이스는 묵시적으로 abstract이다.@@
 * 5. 추상 클래스는 일반 필드를 가질 수 있지만, 인터페이스는 상수필드만 가질 수 있다.@@
 */

public class Java105_interface {

	public static void main(String[] args) {
		//CarRun cn = new CarRun(); 객체 생성 안됨
		System.out.println(CarRun.carCount);
		
		TruckRun tr = new TruckRun();

		
		CarRun.Sun ss = new CarRun.Sun(); // 인터페이스 내에 있는  내부클래스는 객체 생성이 가능하다.
		

	}//end main()

}//end class
