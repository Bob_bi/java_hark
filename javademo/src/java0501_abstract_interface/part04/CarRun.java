package java0501_abstract_interface.part04;

public interface CarRun {
	int carCount =5; // 인터페이스느 기본값이 제공이 되지 않는다. 오직 상수만 제공이 되기 때문에 기본값을 입력해줘야 한다. publi static final int CarRun=5
	
	//public CarRun() {}
	//static {System.out.println("test");    초기화 블록 static블록을 가질수 없다.
	
	void prn(); // 인터페이스 내에 있는 모든 접근제어자는 public만 제공한다.   public abstract
	//void display() {}    중가로 만 있어도 구현부가 있다고 컴퓨터가 인식하므로  안된다. @@멤버변수 필드 지역변수 베리어블@@
	class Sun{} // 인터페이스 내부에는  클래스를 가질수 있다.
	

}//end class
