package java0501_abstract_interface.part02;

public class Tri extends Shape {// 클래스가 접근이 가능해야 상속이 가능함

	public Tri() {
		
	}
	
	public Tri(int width, int heigth) {
		super(width,heigth);
	}
	
	@Override
	double getArea() {
		return (getWidth()*getHeight())/2;
	}
}//end class
