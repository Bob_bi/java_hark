package java0501_abstract_interface.part02;

public class Java103_abstract {
	
	public static void main(String[] args) {
		
		Rect rr = new Rect(10,20);
		System.out.println("사각형의 넓이:"+rr.getArea());
		
		Tri tr = new Tri(10,20);
		System.out.println("삼각형의 넓이:"+tr.getArea());
		
		
	}//end main()

}// end class
