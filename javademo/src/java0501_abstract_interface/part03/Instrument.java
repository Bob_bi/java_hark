package java0501_abstract_interface.part03;
/*
 * 피아노 연주하다. => 손가락
 * 플롯 연주하다. => 입
 */
abstract public class Instrument {// 모디파이 제어자 순서가 바뀌어도 상관없다.( abstract public)
	String name;
	
	public Instrument() {
		
	}
	
	public Instrument(String name) {
		this.name = name;
	}
	
	abstract public void play();

}
