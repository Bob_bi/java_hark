package java0501_abstract_interface.prob.part03;

public class Whale extends Fish implements Mammal {
	
	public Whale() {
		super("고래");
		
	}

	

	@Override
	public void bear() {
		System.out.println(name+"는 새끼를 낳습니다.");
		
	}

}
