package java0501_abstract_interface.part06;

public class UserTest extends User implements Score, Print {
	int score;
	public UserTest() {
	
	}
	
	public UserTest(String name, int score) {
		super(name);
		this.score = score;
	}

	@Override
	public int getScore() {
		
		return sol*score;
		
	}

	@Override
	public String toPaint() {
		
		return ("이름 :" +toString()+"\n점수 : "+ getScore());
	}

}
