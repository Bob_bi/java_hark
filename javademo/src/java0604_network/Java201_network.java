package java0604_network;

import java.net.MalformedURLException;
import java.net.URL;

public class Java201_network {

	public static void main(String[] args) {
		try {
			URL url= new URL("https://movie.daum.net:8080/moviedb/main?movieId=112957"); // :8080 다음 포트 번호
			System.out.printf("getHost():%s\n",url.getHost()); // get방식으로 하면 주소창에 정보를 노출한다.
			//포트번호 가져오기
			System.out.printf("getPort():%s\n",url.getPort());
			//프로토콜 자겨오기
			System.out.printf("getProtocal():%s\n",url.getProtocol());
			//경로가져오기 (패스)
			System.out.printf("getPath():%s\n",url.getPath());
			//쿼리가져오기
			System.out.printf("getQuery():%s\n",url.getQuery());
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}//end main()

}//end class
