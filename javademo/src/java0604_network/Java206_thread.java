package java0604_network;

class PriorityTest extends Thread{
	@Override
	public void run() {
		for(int i=0; i<=5; i++) {
			System.out.printf("%s priority=%d i=%d\n",
					getName(), getPriority(), i); // 쓰레드의 기본적인 이름과  기본적인 우선순위5
		}
	}//end run()////////////////
}//end calss////////////////////

public class Java206_thread {

	public static void main(String[] args) {
		PriorityTest t1 = new PriorityTest();
		t1.start();
		System.out.println("main end");
		
		PriorityTest t2 = new PriorityTest();
		/*
		 * 스레드의 우선순위는  1~10까지 지정할  수 있다.
		 * 스레드의 기본값은 5이다.
		 */
		t2.setPriority(Thread.MAX_PRIORITY);// 10
		t2.start();
		
		PriorityTest t3 = new PriorityTest();
		t3.setPriority(8);
		t3.start();
		
		PriorityTest t4 = new PriorityTest();
		t4.setName("user");
		t4.start();

	}//end main()

}//end class
