package java0604_network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Java202_ClientSocket {
		
	
	public static void main(String[] args) {
			try {
				//3 서버연결 요청(서버주소, 포드번호)
				Socket socket = new Socket("127.0.0.1",7777);
				
				//4번 입출력스트림 연결
				InputStream is = socket.getInputStream(); // 읽기 위한 스트림
				OutputStream os = socket.getOutputStream();// 쓰기위한 스트림
				OutputStreamWriter ow = new OutputStreamWriter(os); // 클라이언트에서 쓴걸 서버쪽으로 보내는거
				
				//5 서버에 정보를 보냄
				ow.write("클라이언의 정보를 보냄");
				ow.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}//end main()
	
}//end class
