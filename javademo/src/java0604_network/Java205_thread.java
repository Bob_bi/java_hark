package java0604_network;

/*
 * 스레드 생명주기(thread Life Cycle)
 * start()-실행준비 상태(RUNNAVLE)상태 - run() - RUN  (run(),RUN이 반복됨)- TERMINATED 종료상태
 *        sleep(), wait() -대기상태(WAITING)
 * 대기상태 빠져나오기 웨이트 슬립 wait()->no tify, no tifyall(수동), sleep()(자동)
 * 
 *  
 */

class LifeCycle extends Thread{
	public LifeCycle() {
	
	}
	// 상속받은 메소드의 예외처리를 할경우  throws 익셉션은 상위 클래스의 익셉션이나 더 구체적인 익셉션이 필요하다.
	@Override
	public void run() { // Thread클래스의 run()이란 메소드는  Throws가 선언되어있지 않아서  쓰로우익셉션을 할수 없다.
		for(int i=0; i<=5; i++) {
			System.out.printf("%s i=%d\n",getName(),i);
			//System.out.println("run:"+getState());// 형재 상태값
			try {
				
				Thread.sleep(1000);// 1초   (WAITING-일시정지상태)
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}//end run()///////////////////////
}//end LifeCycle/////////////////////////////////
public class Java205_thread {

	public static void main(String[] args) {
		LifeCycle cc = new LifeCycle();
		
		System.out.println(cc.getState());//현재 쓰레드의  상태값  new -객체생성
		cc.start();
		try {
			//지정된 시간동안 스레드가 실행도록 한다.
			//지정된 시간이 지나거나 종료가 되면 join() 을 호출한
			//스레드로 다시 돌아와 실해을 계속 수행한다.
			cc.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			
		}
		System.out.println(cc.getState());// TERMINATED 상태
		System.out.println("main end");

	}//end main()

}//end class
