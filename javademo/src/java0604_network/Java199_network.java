package java0604_network;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class Java199_network {

	public static void main(String[] args) {
		try {
			URL url = new URL("https://www.daum.net");
			URLConnection conn=url.openConnection();
			Scanner sc = new Scanner(conn.getInputStream()); //URL 네트워크 에서 자료 읽어 오기.
			while(sc.hasNext()) {
				System.out.println(sc.nextLine());
			}
			System.out.println(conn);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}//end main()

}//end class
