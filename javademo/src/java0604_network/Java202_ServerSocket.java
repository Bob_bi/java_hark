package java0604_network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.Buffer;

public class Java202_ServerSocket {

	public static void main(String[] args) {
		try {
			//1 포트번호를 지정해서 서버 생성
			ServerSocket server = new ServerSocket(7777);//포트번호
			System.out.println("접속");
			// 클라이언트 요청이 들어올때 까지 대기
			Socket socket = server.accept();
			//4번 입출력스트림 연결
			InputStream is = socket.getInputStream(); // 읽기 위한 스트림
			OutputStream os = socket.getOutputStream();// 쓰기위한 스트림
			InputStreamReader ir = new InputStreamReader(is); //  클라이언트가 보낸 정보를 읽어 내리는것
			BufferedReader br = new BufferedReader(ir); // 서버에 있는 정보를 자바에 읽어 내리는것
			
			//5 클라이언트의 정보를 읽음
			System.out.println(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}//end main()

}//end class
