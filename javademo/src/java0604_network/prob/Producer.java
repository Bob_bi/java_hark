package java0604_network.prob;

public class Producer extends Thread {
	private VendingMachine vd;
	public Producer() {
	}
	public Producer(VendingMachine vd) {
		this.vd = vd;
	}

	@Override
	public synchronized void run() {
		for(int i=1; i<=10; i++) {
			System.out.println(Thread.currentThread().getName()+" : 음료수 No. "+i+"넣음");
			vd.putDrink("음료수 No."+i);
		}		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	

}
