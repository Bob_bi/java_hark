package java0604_network;

class User2 implements Runnable{
	public User2() {
	
	}
	
	@Override
	public void run() {
		for(int i=0; i<=5; i++) {
			System.out.printf("%s i=%d\n",
					Thread.currentThread().getName(),i);
		}		
	}//end run()///
}//end User2/////////////

public class Java204_thread {

	public static void main(String[] args) {
		User2 us = new User2();
		Thread th = new Thread(us);
		th.start();
		System.out.println("main thread");

	}//end main()

}//end class
