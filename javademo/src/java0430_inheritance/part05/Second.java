package java0430_inheritance.part05;

public class Second extends First {
	int b = 20;
	
	//annotation(어노테이션) 오버라이딩이 제대로 됬는지 확인 하는 방법
	@Override
	void prn() {
		System.out.println("b="+b);
	}
	
	public void superThisMethod(){
		super.prn();
		this.prn();
	}
	
//	public static void display() {  static이 붙어 있는 메소드에서 this and super는 쓸수가 없다.
//		super.prn();
//		this.prn();
//	}
	

}
