package java0430_inheritance.prob.part02;

public class StudentExam extends Human {
	String harbun;
	public StudentExam(String name, int age, int height, int weight, String harbun) {
		super(name, age, height, weight);
		this.harbun=harbun;
	}
	
	public String toString() {
		String data = name + "\t" + age + "\t" + height + "\t" + weight+ "\t"+harbun;
		return data;
	}
	

}//end class
