package java0430_inheritance.prob.part05;

public class Nangjan extends Basic {
	int ondo;

	public Nangjan(String name, int price, int ag) {
		super(name,price, ag);

	}
	public void ondoUp() { // 성능 온도 업
		ondo++;
		System.out.println("냉장고의 온도가 올라갔습니다.");
	}
	public void ondoDown() { // 성능 온다 다운
		ondo--;
		System.out.println("냉장고의 온도가 내려갔습니다.");
	}

	public void powerOn() {
		if(power) {
			System.out.println("전원이 켜졌습니다.");
		}else {
			System.out.println("전원이 꺼졌습니다.");
		}
	}
	
	public void prn() {
		System.out.printf("%s의  가격은 : %d  이고  수량은 : %d개입니다.\n",getName(),getPrice(),getAg());
	}

}
