package java0430_inheritance.prob.part05;

public class Fan extends Basic {
	int po; // 풍속  
	boolean he; // 회전
	
	public Fan() {
		
	}
	
	public Fan(String name, int price, int ag ) {
		super(name,price,ag);
	}
	
	public void pong() {
		switch (po) {
		case 1:
			System.out.println("풍속이 약입니다.");
			break;
		case 2:
			System.out.println("풍속이 중입니다.");
			break;
		case 3:
			System.out.println("풍속이 강입니다.");
			break;

		default:
			break;
		}
	}
	
	
	public void heH() {
		if(he) {
			System.out.println("회전입니다.");
			
		}else {
			System.out.println("정지입니다.");
		}
	}
	
	public void prn() {
		System.out.printf("%s의  가격은 : %d  이고  수량은 : %d개입니다.\n",getName(),getPrice(),getAg());
	}

}
