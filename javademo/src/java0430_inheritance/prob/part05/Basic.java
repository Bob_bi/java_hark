package java0430_inheritance.prob.part05;

public class Basic {
	private String name; //물건이름
	 boolean power;// 전원스위치  true 이면 켜지고 false이면 꺼짐
	 private int price; // 가격
	 private int ag; // 갯수
	
	
	 public Basic() {
		
	}
	 
	 public Basic(String name, int price, int ag) {
		 this.name=name;
		 this.price=price;
		 this.ag=ag;
	 }


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public int getAg() {
		return ag;
	}


	public void setAg(int ag) {
		this.ag = ag;
	}
	
	
	public int avg() {
		return price*ag;
	}



	 
	 
	 
	

}
