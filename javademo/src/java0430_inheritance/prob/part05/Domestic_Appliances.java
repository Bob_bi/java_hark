package java0430_inheritance.prob.part05;

public class Domestic_Appliances {

	public static void main(String[] args) {
		Nangjan nj = new Nangjan("냉장고", 2500000, 2);
		Tv tv = new Tv("텔레비젼",1500000,1);
		Fan fan = new Fan("선풍기",25000,10);
		
		System.out.println("=====================");
		nj.prn();
		tv.prn();
		fan.prn();
		
		System.out.println("냉장고의 기능:==============");
		nj.power=true;
		nj.powerOn();
		nj.ondoDown();
		nj.power=false;
		nj.powerOn();
		
		 
		System.out.println("선풍기의 기능:==============");
		fan.power=true;
		fan.po = 2;
		fan.pong();
		fan.he = true;
		fan.heH();


		System.out.println("델레비젼의 기능:==============");
		tv.power=true;
		tv.powerOn();
		tv.chanelUp();
		tv.chanelUp();
		tv.chanelUp();
		System.out.printf("현재 채널은 %d\n",tv.chanel);
		tv.bolumUp();
		tv.bolumUp();
		tv.bolumUp();
		tv.bolumUp();
		System.out.printf("현재의 볼륨은 %d\n",tv.bolum);
		tv.power=false;
		tv.powerOn();
		
		
		
		
	}//end main()

}//end class
