package java0430_inheritance.prob.part05;

public class Tv extends Basic {
	int bolum; // 볼륨
	int chanel; // 채널
	
	public Tv() {
		
	}
	

	public Tv(String name, int price, int ag ) {
		super(name,price,ag);
	}
	
	
	public void chanelUp() {
		chanel++;
	}
	public void chanelDw() {
		chanel--;
	}
	public void bolumUp() {
		bolum++;
		System.out.println("볼륨을 높였습니다.");
	}
	public void bolumDw() {
		bolum--;
		System.out.println("볼륨을 내렸습니다.");
	}
	
	
	public void powerOn() {
		if(power) {
			System.out.println("전원이 켜졌습니다.");
		}else {
			System.out.println("전원이 꺼졌습니다.");
		}
	}
	
	public void prn() {
		System.out.printf("%s의  가격은 : %d  이고  수량은 : %d개입니다.\n",getName(),getPrice(),getAg());
	}


}//end class
