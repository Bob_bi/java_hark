package java0430_inheritance.part02;

public class MyGrand {
	public MyGrand() {//(6)
		super();//(7)
		System.out.println("MyGrand");//(8)
		/*
		 * 자식생성자에서는 반드시 부모생성자를 호출해야 한다.
		 * 부모생성자 호출이 생략되어 있으면 JVM에서 super()로 호출한다.
		 */
	}//(9)

}//end class



