package java0430_inheritance.part02;

public class MyChild extends MyFather {
	public MyChild() {// 상속관계에서 생성자는 상위 클래스를 호출한다. 호출문이 없으면 super가 자동적으로 호출한다.(2)
		super();//(3)
		System.out.println("MyChild");//(12)
	}//(13)
}//end class
