package java0430_inheritance.part10;

public class Bird extends Pet {
	String type; // 새 종류
	boolean flightYN; // 날수 있는지 여부
	
//	Bird(String type, boolean flightYN){// 새로 만든거
//		
//		this.type =type;
//		this.flightYN=flightYN;
//	}

	boolean getFlight() {
		return flightYN;
	}// end getFlight()
}// end Bird
