package java0430_inheritance.part10;

public class Dog extends Pet {
	String name; // 강아지 이름
	int weight; // 강아지 무게
	
//	Dog(String name, int weight){ // 새로 만든것
//		this.name = name;
//		this.weight=weight;
//		move();
//	}

	void sum(int a, int b) {

	}

	void sum(int a, int c, int d) {

	}

	int getWeight() {
		return weight;
	}// end gettWeight( )
}// end Dog