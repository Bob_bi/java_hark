package java0430_inheritance.part08;

public class Car {
	private int speed; // private가 붙으면 상속이 안됨
	String color;
	
	public void setSpeed(int speed) {
		this.speed=speed;
	}

}//end class
