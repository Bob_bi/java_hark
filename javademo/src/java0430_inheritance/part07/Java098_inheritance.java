package java0430_inheritance.part07;

/*
 * 다음과 같은 결과가 나오도록 구현하시오.
 * (단, main()안의 내용수정과 새로운 method추가하지 하시오)
 * 
 * ***x,y,z에 값 채우기***
 * 10을 넘겨받은 생성자
 * 10   20  을 넘겨받은 생성자
 * 10   20  30을 넘겨받은 생성자
 * 10   20  30
 * 합 : 60
 */


public class Java098_inheritance {

	public static void main(String[] args) {
		SubClass sc = new SubClass(10,20,30);
		

	}//end main()

}//end class
