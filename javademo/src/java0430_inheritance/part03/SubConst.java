package java0430_inheritance.part03;

public class SubConst extends SuperConst {
	// SuperConst클래스에 1개의 생성자가 정의되여 있으므로
	//JVM에서 기본생성자를 제공하지 않으므로 super()로 호출할 수 없다.
	SubConst(){//(3)
		//super();   /// 인자값이 없는 상위 생성자를 불러와야 하는데 상위 클래스에 인자값이 없는 생성자가 없기때문에 오류
		super(10,40);// 해결 방법은 상위 클래스에 인자값이 없는 생성자를 만들거나 하위클래스에서 super();를 이용하여 상위클래스에 있는 인자값을 넣어주면 된다.
		//(4)
	}//(9)
	

}
