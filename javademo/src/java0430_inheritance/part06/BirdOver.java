package java0430_inheritance.part06;

public class BirdOver extends PetOver {
	
	public void move() {
		System.out.println("서브클래스 move() : 새가 날아갑니다.");
	}

}//end BirdOver class
