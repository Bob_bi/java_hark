
/* Drop Tables */

DROP TABLE product CASCADE CONSTRAINTS;
DROP TABLE business CASCADE CONSTRAINTS;




/* Create Tables */

CREATE TABLE business
(
	bnum  NOT NULL,
	bname varchar2(50 char),
	baddress varchar2(100 char),
	PRIMARY KEY (bnum)
);


CREATE TABLE product
(
	pnum  NOT NULL,
	pname varchar2(50 char),
	bnum  NOT NULL,
	PRIMARY KEY (pnum)
);



/* Create Foreign Keys */

ALTER TABLE product
	ADD FOREIGN KEY (bnum)
	REFERENCES business (bnum)
;



