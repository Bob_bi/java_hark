package java0503_api;

/*
 * 난수를 이용해서 소수점 첫째자리까지 반올림해서 구하시오
 */
import static java.lang.Math.*;
public class Java_132Math {

	public static void main(String[] args) {
		double num= random()*100;
		System.out.println(num);
		long num1 = round(num); // 리턴값이 롱으로 들어온다.
		System.out.println(num1);
		double num3 = (double)num1/10;
		System.out.println(num3);
		System.out.println("=================");
		double res = Math.random();
		System.out.println((double)Math.round(res*100)/10);

	}//end main()

}//end class
