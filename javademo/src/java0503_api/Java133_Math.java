package java0503_api;

import java.util.Random;

public class Java133_Math {

	public static void main(String[] args) {
		Random ran = new Random();
		int[] num = new int[5]; //1부터 10까지 난수
		
		//난수발생
		for(int i=0; i<num.length; i++) {
			num[i] = ran.nextInt(10)+1;
			//중복체크
			for(int j=0; j<i; j++) {
				if(num[i]==num[j]) {
					--i;
					break;					
				}
			}
		}
		
		
		
		
//		for(int i=0; i<num.length; i++){ // 난수 값 발생후 0번지에 저장 이후 내부 for문을 돌면서 그전에 있는값과 비교하며 같으면 다시 외부포문으로 돌아가 똑같은 번지에 따시 난수를발생시켜 저장
//			int sum = ((int)(Math.random()*10))+1;
//			num[i]= sum;
//			for(int j=0; j<i; j++)
//				if(num[i]==num[j]) {
//					--i;
//					break;
//				}
//				
//		}
		
		
		for(int data :num)
			System.out.println(data);

	}//end main()

}//end class
