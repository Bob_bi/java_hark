package java0503_api;

/*
 * sn변수에서 저장된 문자열중에서 숫자갯수를 출력하는 프로그램을 구현하시오
 * [출력결과]
 * 숫자갯수 = 3
 * 
 */
public class Java142_warpper {

	public static void main(String[] args) {
		String sn = "korea12 parab3";
		int num=0;
		for(int i=0; i<sn.length(); i++) {
			if(Character.isDigit(sn.charAt(i)))
				num+=1;
		}
		System.out.println("숫자갯수 = "+num);

		
	}//end main()

}//end class
