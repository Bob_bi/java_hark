package java0503_api;
/*
 * 1 기본자료형을 클래스로 정의해놓은 것을 Wrapper라한다.
 *   char -> Character
 *   byte	-> Byte
 *   short	-> Short
 *   int	-> Integer
 *   long	-> Long
 *   float	-> Float
 *   double	-> Double
 *   boolean -> Boolean
 *       
 * 2 메모리에 저장된 값을 다른 자료형으로 변환해주는 메소드를 제공하기 위해서
 *   Wrapper클래스를 제공한다.
 * 
 * 3 auto boxing과 auto unboxing은 jdk5.0에서 추가된 기능이다.
 *   auto boxing => 스택 -> 힙영역에 복사
 *   auto unboxing => 힙 -> 스택영역에 복사
 */
public class Java139_Wrapper {

	public static void main(String[] args) {
		String data ="123";
		//String ->Integer
		Integer it = new Integer(data);
		//Integer -> int
		int num =it.intValue(); // it.bytevalue(), it.floatValue()
		System.out.println("num="+num);
		//Integer => double
		double pro =it.doubleValue();
		System.out.println("pro="+pro);
		
		//Integer => long
		long ro =it.longValue();
		System.out.println("ro="+ro);
		
		//Integer => Float
		float f =it.floatValue();
		System.out.println("f="+f);
		
		Float ft = new Float(data);
		//Float -> int
		int fx =ft.intValue();
		System.out.println("fx="+fx);
		
		//Float -> double
		double de = ft.doubleValue();
		System.out.println("de="+de);
		
		//String => int
		int num1=Integer.parseInt(data);
		System.out.println("num1 = "+num1);
		
		//String -> byte
		byte numByte =Byte.parseByte(data);
		System.out.println("numByte="+numByte);
		
		//String  -> short
		short numShort = Short.parseShort(data);
		System.out.println("numShort="+numShort);
		
		//String ->long
		long numLong = Long.parseLong(data);
		System.out.println("numLong="+numLong);
		
		//String ->float
		float numFloat = Float.parseFloat(data);
		System.out.println("numFloat=" +numFloat);
		
		//String -> double
		double numDouble = Double.parseDouble(data);
		System.out.println("numDoulble="+numDouble);
		
		//String -> boolean
		//파싱 불가 (true 혹은 false문자열만 파싱할 수 있음)
		boolean numBoolean = Boolean.parseBoolean(data);
		System.out.println("numBoolean="+numBoolean);//false
		
		String chk="true";
		numBoolean=Boolean.parseBoolean(chk);
		System.out.println("numBoolean="+numBoolean);
		
		char c = data.charAt(0); // 파싱하지 못하고 문자 1개만 분리해냄
		System.out.println("c="+c);
		
		int x=10;// 스택에 있는 인트형값을 힙에 저장하는 방법
		//Integer ig = new Integer(x);
		Integer ie =x; // auto boxing (stack -> heap)
		
		//int k =ie.intValue();
		int y=ie; //auto unboxing(heap ->stack)
		System.out.println("y="+y);
		
	}//end main()

}//end class
