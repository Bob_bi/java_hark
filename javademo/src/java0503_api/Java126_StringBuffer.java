package java0503_api;

public class Java126_StringBuffer {

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("jaba test");
		
		
		System.out.println(sb);//sb.tostring 과같다
		//4인덱스에 ",jsp"문자열을 삽입한다.
		sb.insert(4, ",jsp");
		System.out.println(sb);
		
		//4인덱스부터 8인덱스 미만 사이의 문자열을 삭제한다.
		sb.delete(4, 8);//java test
		System.out.println(sb);
		
		//0인덱스의 문자를 삭제한다.
		sb.deleteCharAt(0); // ava test
		System.out.println(sb);
		
		//문자열을 반대로 변경해서 리턴한다.
		sb.reverse();
		System.out.println(sb);
		
		String data = "mybatis orm";
		
		StringBuffer sf = new StringBuffer(data);
		sf.reverse();
		System.out.println(sf);
		data = sf.toString();
		System.out.println(data);
		
		char[] arr = new char[] {'k','o','r','e','a'}; // char배열을  스트링 버퍼로 받아서 리버스
		StringBuffer sbf =new StringBuffer();
		sbf.append(arr);
		System.out.println(sbf);
		sbf.reverse();
		System.out.println(sbf);
		
		String sng = new String(arr);// char배열을 스트링형태로 받은후 스트링버퍼 생성자로 스트링을 받아서 리버스
		StringBuffer sa = new StringBuffer(sng);
		sa.reverse();
		System.out.println(sa);
		
		

	}//end main()

}//end main
