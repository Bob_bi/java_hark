package java0503_api;

import java.util.Scanner;

public class Java137_Scanner {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//System.out.print("부서: ");
		System.out.println("부서 연봉 평가");
		String dept=sc.next(); //  넥스트와 인트 더블을 한줄로 표기할 경우 공백을 이용하여 나눠지게 된다. 중요한점 리턴값을 잘 봐야한다.
		 							//인트면 인트형 스트링형이면 같은 스트링형 반환타입을 잘보고 해야한다.
		//System.out.print("연봉: ");
		int salary=sc.nextInt();
		
		//System.out.print("평가 :");
		double avg = sc.nextDouble();
		
		System.out.printf("%s %d %.1f",
				dept,salary,avg);
		
		sc.close(); //연결종료

	}//end main()

}//end class
