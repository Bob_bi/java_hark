package java0503_api;

/*
 * java,lang.String : 불변
 * java,lang.StringBuffer : 가변, 동기화 (한쪽이 주고 다른쪽이 받고 나서야 한쪽이 다시 요청할수있는것)
 * java,lang.StringBuilder : 가변, 비동기화 (한쪽이 주고 다른쪽이 안받아도 한쪽이 계속 요청할수 있음)
 * 
 * StringBuffer 특징
 * 1. 문자열 값을 수정할 수 있는 muuable(가변)이다.
 * 2. 기본 16문자 크기로 지정된 버퍼를 이용하며, 크기를 증가시킬 수 있다.
 */


public class Java125_StringBuffer {
	public static void main(String[] args) {
		
		String sg =new String("java test");// 불변
		String ss= sg.replace("java", "jsp"); // String 형으로 리플레이스를 할경우 힙메모리에 스트링버퍼라는 메모리공간이 새로 생기면서 그공간에 변경된 문자열이 저장되며 원래의 스트링형인 문자열은 그대로 있다.
		System.out.println("ss:"+ss);
		System.out.println("sg:"+sg);
		
		System.out.println("=====================");
		StringBuffer sb =new StringBuffer("spring test"); //가변 
		sb.replace(0, 6, "framework");
		System.out.println("sb:"+sb);
	}//end main()

}//end class
