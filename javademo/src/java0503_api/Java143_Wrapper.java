package java0503_api;

import java.util.Scanner;
import java.util.StringTokenizer;
/*
 * 이름, 학점을 입력하는 프로그램을 구현하시오
 * 입력 : 홍길동, 80,93
 * 
 * [출력결과]
 * 이름: 홍길동
 * 국어 : 80
 * 영어 : 93
 * 평균 : 86.5(double로 계산)
 * 
 */
public class Java143_Wrapper {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("입력: ");
		String str = sc.nextLine();
		String[] st = str.split(",");
		double num=0;
		for(int i=1; i<st.length; i++) {
			num += Double.parseDouble(st[i]);
			
		}
		System.out.println("이름:"+st[0]);
		System.out.println("국어:"+st[1]);
		System.out.println("영어:"+st[2]);
		System.out.println("평균:"+num/2);
		
		StringTokenizer st1 = new StringTokenizer(str,",");
		while(st1.hasMoreElements())
			if(st1.nextToken().isEmpty())
			System.out.println("이름: "+st1.nextToken());
		

	}

}
