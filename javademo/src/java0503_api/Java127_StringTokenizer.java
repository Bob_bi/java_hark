package java0503_api;

import java.util.StringTokenizer;

public class Java127_StringTokenizer {

	public static void main(String[] args) {
		
		StringTokenizer st = new StringTokenizer("java,jsp",",");// 뒷부분 구분자를 선언안해줄 경우 공백이 구분자가 된다. 나눠지면 토큰이라 부른다.
		System.out.println(st.countTokens());
		
//		for(int i=0; i<st.countTokens(); i++)
//			System.out.println(st.nextToken());// 가져온다 꺼내온다. 메모리에 토큰 이 2개로 나눠져서 있는상태에서 nextToken하게되면 하나를 꺼내오고 꺼내온 메모리는 지워진다.
		
		
		while(st.hasMoreTokens()) { // 다음에 가져올 토큰이 있나 없나. 
			System.out.println(st.nextToken());//메소드가 hes나 is로 시작하면 리턴값이 논리값인 경우가 많다.
			
		}
		
	}//end main()

}//end class
