package java0503_api;

public class Java_StringBuffer {
	
	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("java test");
		//StringBuffer -> String
		String str = sb.toString();
		
		//String -> char[]
		display(str.toCharArray());
	}//end main()
	
	public static void display(char[] data) {
		for(char cn: data)
			System.out.println(cn);
	}

}//end class
