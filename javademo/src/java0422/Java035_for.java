package java0422;
/*
 * 3행 x 4열
 * 1   2   3   4
 * 5   6   7   8
 * 9   10  11  12
 */
public class Java035_for {

	public static void main(String[] args) {
//		for(int i=1; i<=12; i++) {
//			
//			if(i%4 == 0) {
//				System.out.printf("%4d\n", i);
//			}else  {
//				System.out.printf("%4d", i);
//				
//			}
//			
//		}
//		
		
//		for(int i=1; i<=12; i++) {
//			System.out.printf("%4d", i);
//			if(i%4==0) {
//				System.out.println();
//			}
//		}
		int data = 1;
		//행
		for(int row=1; row<=3; row++) { // 밖에 있는건 행 안에 있는건 열 
			//열
			for(int col=1; col<=4; col++) {
				System.out.printf("%4d", data);
				if(data%4==0) {
					System.out.println();
				}
				data++;
			}
		}
		
		
		

	}//end main()

}//end class
