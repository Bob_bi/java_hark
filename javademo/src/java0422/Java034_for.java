package java0422;

/*3의 배수만 제외하고 출력하기
 * 1
 * 2
 * 4
 * 5
 * 7
 * 8
 * 10
 *
 */




public class Java034_for {

	public static void main(String[] args) {
		
		for(int i=1; i<=10; i++) {
			
//			if(i%3 != 0) {
//				System.out.println("i=" + i);
//			}else {
			if(i%3==0) {
				continue;// 반복문에서 continue를 만나면 (for)증감식으로 바로 간다.  do_while, while문에서는 조건식으로 바로 간다.
			}
			System.out.println(i);
		}
		

	}//end main()

}//end class
