package java0422;

/*
 * i=1
 * i=2
 * i=3
 * i=4
 * i=5
 */

public class Java031_for {

	public static void main(String[] args) {
		int sum =0;
		
		for(int i=1; ; i++) {
			sum += i;
			System.out.println("i="+i);
			if(sum==15) {
				break;
			}
		}
		System.out.println("sum="+15);
		

	}//end main()

}//end class
