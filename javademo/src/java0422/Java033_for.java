package java0422;

/*
 * 1부터 10까지 짝수, 홀수 누적을 계산하는 프로그램 구현하세요.
 * [출력결과]
 * 짝수누적:30
 * 홀수누적:25
 */

public class Java033_for {

	public static void main(String[] args) {
	
		int odd =0;//홀수 누적
		int even =0;//짝수 누적
		
		for(int i=1; i<=10; i++) {
			
			if(i%2 == 0) { //if(i%2==1){ 홀수가 먼저일때
				even += i;
			}else {
				odd += i;
			}
		}
		
		System.out.println("짝수누적:" + even + "\n홀수누적:"+odd);
		

	}//end main()

}//end class
