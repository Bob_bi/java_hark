package java0422;

/*
 *  while(조건식){
 *    수행할 문장;
 *  }
 *  
 *  while문은 조건식을 생략할 수 없다.
 */
public class Java038_while {

	public static void main(String[] args) {
//		for(int i=1; i<=5; i++) {
//			System.out.println(i);
//		}
//		int i = 1;
//		while(i<=5) {
//			System.out.println(i);
//			i++;
//		}
		
//		int i=0;
//		while(i<5) {
//			i++;
//			System.out.println(i);
//		}
//		int i =0;
//		while(++i<=5) {  // 선증가 비교연산자보다 플러스 연산을 하고 난후에 비교연산을 한다.
//			System.out.println(i);
//		}
		
		int i =0;
		while(i++<5) { // 후증가 비교연산자 실행후 플러스 연산자가 실행
			System.out.println(i);
		}
	

	}//end main()

}//end class
