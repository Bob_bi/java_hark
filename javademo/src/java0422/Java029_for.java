package java0422;

public class Java029_for {

	public static void main(String[] args) {
		int sum = 0; //누적을 저장할 변수
	    int i;
	    
	    
	    for(i=1; ; i++) { //(i=1; true; i++)
	    	
	    	sum += i;
	    	if(sum >= 15) { // (sum == 15)
	    		break;// 가장 가까운 반복문을 빠져나온다.
	    	}
	    	
	    }
		System.out.printf("i=%d sum=%d\n", i, sum);
		
		
		

	}//end main()

}//end class
