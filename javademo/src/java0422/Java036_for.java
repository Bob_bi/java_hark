package java0422;

/*
 *   1 2 3 4 5
 *   1 2 3 4 5
 *   1 2 3 4 5
 *   1 2 3 4 5
 * 
 */

// 행 4 열 5
// 행 초기값 1 종료값 5 증가값 1
public class Java036_for {
	public static void main(String[] args) {
//		int cnt =0;
		
//		for(int row=1; row<=4; row++) {
//			for(int col=1; col<=5; col++) {
//				cnt++;
//				System.out.printf("%-3d",cnt);
//				if(cnt == 5) {
//					cnt = 0;
//					System.out.printf("\n");
//				}
//			}
//		}
		
		for(int row=1; row<=4; row++) {
			int cnt = 1;
			for(int col=1; col<=5; col++) {
				System.out.printf("%4d", cnt);
				if(col>=5) {
					System.out.printf("\n");
				}
				cnt++;
			}
		}
		
		
		
	}//end main()

}//end class
