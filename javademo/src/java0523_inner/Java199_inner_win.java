package java0523_inner;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class Win extends Frame{
	public Win() {
		this.setSize(500, 500); // 픽셀단위 사이즈  웹
		this.setVisible(true);
		addWindowListener(new WindowAdapter() {// 플레임창에 연결해주는 역확
			@Override
			public void windowClosing(WindowEvent e) { // 윈도우창에서 윈도우이벤트가 발생하게 되면 꺼져라
				System.exit(0); // 윈도우가 종료될때 닫혀라?
			}
		});
	}//end Win
}//end Win


public class Java199_inner_win {

	public static void main(String[] args) {
		new Win();
		
	

	}//end main()

}//end class
