package java0523_inner;

abstract class InnerAnonymous{
	abstract void prn();
}//end InnerAnonymous

class Test extends InnerAnonymous{

	@Override
	void prn() {
		System.out.println(10);
	}//end prn
}//end Test

//일회용 클래스 (익명클래스)

class OuterAnonymous{
	private int x;
	public void call() {
		
		new InnerAnonymous() { // new 부터 ;까지가 하나인 클래스 익명클래스
			@Override
			void prn() {
				System.out.println("prn");
			}
		}.prn();
		
//		InnerAnonymous tt = new InnerAnonymous() { // new 부터 ;까지가 하나인 클래스 익명클래스
//			@Override
//			void prn() {
//				System.out.println("prn");
//			}
//		};
//		tt.prn();
	}//end call()//////////////
}//end OuterAnonymous////////


public class Java198_inner {

	public static void main(String[] args) {
		OuterAnonymous ou =new OuterAnonymous();
		ou.call();
		

	}//end main()

}//end class
