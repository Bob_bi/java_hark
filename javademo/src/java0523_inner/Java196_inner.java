package java0523_inner;

class OuterStatic{
	private int x;
	static private int y;
	
	static void call() {
		//x=30; static이 안붙여있어서 안됨
		y=9;
		System.out.println(y);
	}
	
	static class InnerStatic{
		int z;
		void prn() {
			//비 static외부 자원을 static내부클래스에서
			//참조할수 없다.
			//x=50;
			y=20;
			z=40;
			System.out.printf("%d %d\n",y,z);
		}
	}
}//end OuterStatic
public class Java196_inner {

	public static void main(String[] args) {
		OuterStatic.call();
		
		OuterStatic.InnerStatic inner = new OuterStatic.InnerStatic();
		inner.prn();
		

	}//end main()

}//end class
