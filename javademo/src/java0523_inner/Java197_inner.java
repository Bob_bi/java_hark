package java0523_inner;

class OuterLocal{
	private int x;
	static int y;
	final private int Z=10;
	
	public void call( int a) { // 메소드의 매개변수를 지역클래스에서 참조할경우 final이 자동으로 붙기 때문에 값 변경이 안된다.
		final int b=20;
		int c=30;
		//a=300;
		System.out.println(a);
		//메소드에 안에 클래스를 지정
		class InnerLocal{// 지역클래스
			void prn() {
				x=5;
				y=150;
				//a=200; //지역클래스 내에서는 메소드의 지역변수의 값변경이 안된다.
				//Outer 클래스의 메소드에 정의된 매개변수,지역변수을
				//지역변수내부클래스에서 새로운 값을 할당할 수 없다.
				System.out.println("x="+x);
				System.out.println("y="+y);
				System.out.println("a="+a); // final이어야 사용할수 있다.
				System.out.println("b="+b);
				System.out.println("c="+c);
			}//end prn()
		}//end InnerLocal
		
		InnerLocal inner = new InnerLocal();
		inner.prn();
		
	}//end call()

}//end OuterLocal



public class Java197_inner {

	public static void main(String[] args) {
		OuterLocal outer = new OuterLocal();
		outer.call(100);
		
		

	}//end main()

}//end class
