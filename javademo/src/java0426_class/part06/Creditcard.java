package java0426_class.part06;

public class Creditcard {
	String cardName;
	String cardNum;
	int pay;
	
	public Creditcard() {
		
	}

	public Creditcard(String cardName, String cardNum, int pay) {
		super();
		this.cardName = cardName;
		this.cardNum = cardNum;
		this.pay = pay;
	}
	
	public void prn() {
		System.out.printf("%s %s %d\n",
				cardName, cardNum, pay);
	}
	

}//end class
