package java0426_class.part06;


/*
 * [출력 결과]
 * 기업은행 42523-52325 100000
 * 하나은행 52253-22623 153000
 * 신한은행 16239-95235 256000
 * 총납입액 : 509000 
 *    
 */
public class Java078_class {

	public static void main(String[] args) {
		//[출력결과]를 참조하여 구현하시오//////////////
		
		Creditcard[] c1 = new Creditcard[3];
		c1[0] = new Creditcard("기업은행", "42523-52325", 100000);
		c1[1] = new Creditcard("하나은행", "52253-22623", 153000);
		c1[2] = new Creditcard("신한은행", "16239-95235", 256000);
		int sum=0;
		for(int i=0; i<c1.length; i++) {
			sum+=c1[i].pay;
			c1[i].prn();
		}
		System.out.println("총납입액:"+sum+"원");
		int summi=0;
		cardsumpi(c1,summi);

	}//end main()
	public static void cardsumpi(Creditcard[] c1, int summi) {
		for(int i=0; i<c1.length; i++) {
			c1[i].prn();
			summi+=c1[i].pay;
			}				
		System.out.println("총납입액: "+summi+"원");
	}
	
	
	

}//end class
