package java0426_class.part09;

public class Java081_for {

	public static void main(String[] args) {
		int[] num = {10, 20, 30, 40, 50};
		int sum=0;
		for(int a : num) {
			sum+=a;
			System.out.println(a);
		}
		System.out.println("num의 계산값은:"+sum);
		/*
		 * 향샹된 루프=확정된 루프=개선된for문
		 * for(데이터타입 변수 : 배열 or 컬렉션){
		 *   수행할 문장;
		 * }
		 */

	}//end main

}//end class
