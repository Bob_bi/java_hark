package java0426_class.part02;
/* shift+art+r = 소스안에 있는  같은변수명 한번에 바꾸기
 * [실행 결과] 를 참조하여 main()메소드를 추가하는 로직을 구현하세요.
 * 
  [실행 결과] 
    상품                              가격       상품제고   팔린수량
  Nikon            400000    30    50
  Sony             450000    20    35
  FujiFilm         350000    10    25
 */
public class Java074_class {

	public static void main(String[] args) {
		/*Goods g1 = new Goods("Nikon",400000, 30, 50);
		Goods g2 = new Goods("Sony",450000, 20, 35);
		Goods g3 = new Goods("FujiFilm",350000, 10, 25);
		System.out.println("[실행 결과]");
		System.out.printf("%2s  %30s %10s   %s\n","상품","가격","상품제고","팔린수량");
		g1.prn();
		g2.prn();
		g3.prn();*/
		
		Goods[] goodArray = new Goods[3];
		goodArray[0] = new Goods("Nikon",400000, 30, 50);
		goodArray[1] = new Goods("Sony",450000, 20, 35);
		goodArray[2] = new Goods("FujiFilm",350000, 10, 25);
		/*
		System.out.printf("%2s  %30s %10s   %s\n","상품","가격","상품제고","팔린수량");
		for(int i=0; i<goodArray.length; i++) {
			goodArray[i].prn();
		}
		for(Goods a : goodArray) {
			a.prn();
		}*/
			display(goodArray);	

	}//end main()
	

	public static void display(Goods[] goodArray) {
		for(int i=0; i<goodArray.length; i++) {
			goodArray[i].prn();
		}
		
	}//end display()

}//end class
  