package java0426_class.part08;

/*
 * variable argument(가변매개변수)
 * 1 5.0이전 버번에서는 특정 메소드를 정의할때 인자의 타입과 수를 정해놓고
 *   호출할때 일치하지 않으면 오류가 발생했다.
 * 2. 이를 유연하게 처리할 수 있도록 variable argument 기능을 제공해주고 있다.
 * 3. 리턴타입 메소드명(데이터타입...매개변수){}
 * 매개변수 갯수는 상관없음...
 * 고정 변수와 같이 사용 하려면 고정변수를 먼저 쓴후 마지막에 가변병수를 사용한다.
 * 
 */


public class CountVarg {
	void addValue(int...arr) {
		int sum=0;
		for(int i=0; i<arr.length; i++) {
			sum+=arr[i];
		}
		System.out.println(sum);
	}//end addValue()
	//고정 매개변수와 가변매개변수가 같이 사용될때는
	// 언제나 고정 매개변수를 먼저 선언한다.
	void addValue(String name, int ...arr) {
		int sum=0;
		for(int i=0; i<arr.length; i++) {
			sum+=arr[i];
		}
		System.out.printf("name=%s sum=%d\n", name, sum);
	}

}//end class
