package java0426_class.part08;

public class Java080_varg {

	public static void main(String[] args) {
		
		CountVarg ab  = new CountVarg();
		
		ab.addValue(4,8);
		ab.addValue(2,3,9);
		ab.addValue(2,2,4,4,4,4,4);
		
		ab.addValue("홍길동", 70,80,65);
		ab.addValue("길동무", 90,70);
		

	}//end main

}//end class
