package java0426_class.part03;

public class Doctor {
	String name; //의사명
	String medical; // 진료 과목
	int patient; //대기환자수
	
	
	public Doctor() {
		
	}


	public Doctor(String name, String medical, int patient) {
		super();
		this.name = name;
		this.medical = medical;
		this.patient = patient;
	}
	
	public void prn() {
		System.out.printf("%s %s %d\n", name, medical, patient);
	}

}//end class
