package java0426_class.part04;

public class Calc {
	int firsr;
	int second;
	int ope;
	
	public Calc() {
		
	}

	public Calc(int firsr, int second, int ope) {
		super();
		this.firsr = firsr;
		this.second = second;
		this.ope = ope;//배열에 저장한 연산자는 +,-,*,/만 저장함
	}
	
	public int process() {
		int a=0;
		if(ope == '*') {
			a = firsr*second;
		}else if(ope == '+') {
			a = firsr+second;
		}else if(ope == '-') {
			 a = firsr-second;			
		}else {
			a = firsr/second;
		}
		
		return a;
	}//end process()
	
	public void prn() {
		System.out.printf("%2d %c %2d = %d\n",
				firsr, ope, second, process());
	}//end prn

}//end class
