package java0429_static_access.part07;

public class Java090_singleton {

	public static void main(String[] args) {
		MemberDAO dp =MemberDAO.getInstance();
		dp.display();
		
		
		MemberDAO ds = MemberDAO.getInstance();
		ds.display();
		
		System.out.println(dp==ds);
		
		StudentDAO sa = new StudentDAO();
		StudentDAO st = new StudentDAO();
		System.out.println(sa==st);
		
		
		
		//getInstance 접근 제한자 static이 붙으면 바로 메서드 영역에 저장이 됨.....
		//싱글톤 패턴 외부에서 무분별한 객체 생성을 막기 위하여 클래스 내에  생성자에 private을 걸어 놓고
		// 내부에 자기자신의 객체를 생성후 외부에서 객체를 사용하려하면. 객체생성한곳과 메서드에 static을 붙여 놓으면.
		// 외부에서 접근하려면 클래스 변수를 선정한후 변수에 클래스.겟인스턴스를 불러 오면  
		//main 메소드가 실행이 되면서 메소드 메모리 영역에 main메소드 영역과 static이 붙은 dao영역이 생긴다.
		//dao의 객채가 생성이 되면서 new가 붙으면서 인스턴스화가 진행 hip영역에 dao인스턴스 공간이 만들어지게 된다.
		//밑으로 static이 붙은 MemberDAO 겟인스턴스가 자동으로 메소드 영역에 생성 겟인스턴스의 리턴값인 dao의 주소 값이
		//메소드 영역에 있는 객체에 있는 dao에 주소값이 간다.

	}//end main()

}//end class
