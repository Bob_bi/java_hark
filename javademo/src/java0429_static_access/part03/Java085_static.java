package java0429_static_access.part03;

public class Java085_static {
	
	public static void display() {
		System.out.println("display");
	}
	
	public void process() {
		System.out.println("process");
	}
	
	public static void main(String[] args) {
		
		display();
		Java085_static js = new Java085_static();
		js.process();// 클래스내에 static이 안 붙어있는 메소드를 호출 하려면 객체 
		             //생성 인스턴스화를 진행시켜 준후 인스턴스를 이용하여 메소드를 호출해야 한다.
		
		
		
	}//end main()

	
}//end class
