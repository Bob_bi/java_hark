package java0523_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class JdbcTest{
	Connection conn;
	Statement stmt; // 쿼리문 작성할때 쓰는
	ResultSet rs;
	public JdbcTest() {
		
	}
	
	void process() {
		try {
			//1.드라이버 로딩
			Class.forName("oracle.jdbc.OracleDriver"); //드라이버 로딩
			
			//2.서버 연결 (오라클 서버연결)
			String url="jdbc:oracle:thin://@127.0.0.1:1521:xe"; // 교육용이기 때문에 xe임 엔터프라이즈면 urc임  포트번호 :1521 기본적인 포트번호
			String username="hr";
			String password="a1234";
			conn=DriverManager.getConnection(url,username,password);
			
			//3. 쿼리문을 싱행하기 위한 Statement타입의 객체를 리턴
			stmt =conn.createStatement();
			String sql="select * from departments order by department_id";
			
			//4.쿼리문 전달 하며 실행한다 결과값을 rs에 담는다.
			rs=stmt.executeQuery(sql);
			
			//5. 결과 값 화면에 띄우기
			while(rs.next()) {
				int department_id = rs.getInt("department_id");// 가독성을 위해 인덱스 번호가 아닌 컬럼명을 한다.
				String dName= rs.getString("department_name");// 가독성을 위해 인덱스 번호가 아닌 컬럼명을 한다.
				int manId = rs.getInt("manager_id");
				int locId = rs.getInt("location_id");
				System.out.printf("%d %s %d %d\n",
						department_id, dName,manId,locId);
			}
			//규칙  "jdbc:oracle:thin://@127.0.0.1:xe" 다른 컴퓨터에서 접근을 하려면 골뱅이뒤에 자기 아이피주소 써주면 됨
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}///////////////////////////
			if(stmt!=null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}//////////////////////////
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}//////////////////////////////
		}
	}
}//end jdbcTest




public class Java200_jdbc {
	
	// 자바에서 jdbc에 접근을 하여 

	public static void main(String[] args) {
		JdbcTest jt = new JdbcTest();
		jt.process();
		

	}//end main()

}//end class
