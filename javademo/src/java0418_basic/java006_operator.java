package java0418_basic;

public class java006_operator {

	public static void main(String[] args) {
		
		int a = 10;
		int b = 3;

		System.out.println(a/b); // int/int => 3 int형이기때문에 소수점 나머지 버려짐
		
		System.out.println((double)(a/b)); // (double)(int/int) => 3.0 더블의 형태로 결과값이 나옴
		
		System.out.println((double)a/b); // 3.333333 앞의 a부분을 더블의 형태로 변환해주고나서  int형 b랑 나눔
		
		

	}//end main()

}// end class
