package java0418_basic;

public class java012_operator {

	public static void main(String[] args) {
		int a = 3;
		int b = 5;
		int c = 7;
		
		//&&연산자의 좌변이 false이면 우변은 수행하지 않는다
		boolean res = (++a > b) && (++b < c);
		System.out.println("a=" + a); //a=4
		System.out.println("b=" + b); //b=5
		System.out.println("res=" + res);//res = false
	
		
		int x = 4;
		int y = 8;
		int z = 10;
//		res = (z > ++y) || (++x > y);
		
		/*
		 * x>y => false
		 * && => false
		 * ++x => 5
		 * x==y => false
		 * || => false
		 */
		//res = x>y && ++y<z || ++x ==y;
		//res = x>y || ++y<z && ++x ==y;
		
		// 앞에 ||연산일 경우 좌변의 경우 트루일경우 우변을 결과를 안한다.
		// &&와 || 연산자가 두개일 경우 ||를 기준으로 좌변 우변을 기준으로 계산한다.
		res = ++y < z || ++y==z && ++y == z;
		System.out.println("x=" + x); // x => 4
		System.out.println("y=" + y); // y => 9  10
		System.out.println("res=" + res); // res => true
		
		
		
		
		
	}//end main()

}//end class
