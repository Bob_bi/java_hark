package java0418_basic;

//주석 : 프로그램에 대한 부가적인 설명이다.
//한 라인 주석처리

/*
 * 여러 라인에 대한 주석처리
 */

/*
 * 키워드(keyword:예약어) : 언어에서 단어에 의미를 부여해서 정의해놓은 단어. 
 */

public class Java001_main {
	
	public static void main(String[] args) {
		System.out.println(5); // 정수 숫자
		System.out.println('문'); // 문자 (char)약어, 한글자만
		System.out.println("java"); //문자열 (String)
		System.out.println(5.0); // 실수 숫자  소수점이하를 나타낼때 사용
		System.out.println(true); // 논리값 
		
	}

}
