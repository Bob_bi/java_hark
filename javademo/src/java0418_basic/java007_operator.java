package java0418_basic;

public class java007_operator {

	public static void main(String[] args) {
		int a = 3;
		int b = 4;
		
		System.out.println(a+b); //산술연산자
		System.out.println(a + "는(은) 삼입니다.");// 한쪽이 문자열일 경우 산술연산자가 아닌 문자열 연결을 뜻합니다.
		System.out.println("a=" + a);
		
		
		System.out.println("결과:" + (a+b));//결과:7
	}//end main()

}//end class
