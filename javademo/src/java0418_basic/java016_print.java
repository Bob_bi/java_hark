package java0418_basic;

public class java016_print {

	public static void main(String[] args) {
		System.out.println("java"); // println 출력문은 바로 줄바꿈이 일어난다.
		System.out.println("jsp");
		System.out.print("spring\n"); // print 출력할시 줄바꿈이 일어나지 않는다.
		System.out.print("oracle\n");
		
		/*
		 * printf("출력형식", 값1, 값2.....)
		 * 
		 * %s : 문자열 출력
		 * %d : 정수 출력
		 * %f : 실수 출력
		 * %c : 문자
		 * %b : 논리
		 * %% : %
		 * \ : 특수문자를 처리하는 기호
		 */
		
		
		int sum = 12;
		System.out.printf("%.1f\n", 0.568f); // float, double, 소수점 맞춰서 할때는 반올림이 발생함.
		System.out.printf("%d\n", 50l); // byte, short, int, long
		System.out.printf("%s \n", "mybatis"); // 문자열 String
		System.out.printf("%c\n", '아'); // 문자 char
		System.out.printf("%c, %b, %d\n", 'a', true, 10);
		System.out.printf("kkkseee\n");
		
		
		System.out.printf("%5.1f\n", 0.568f); // %5.1 앞의 5는 출력되는 자릿수까지를 나타내는 것이고 
		                                      //그 뒤에 있는 1은 소수점 자리수를 나타낸다
		System.out.printf("%5.1f\n", 34.568f); 
		System.out.printf("%5.1f\n", 128.568f); 
		System.out.printf("%5.1f\n", 2815678.5f); 
		System.out.printf("%5.2f\n", 4.56); //  4.56
		System.out.printf("%-5.2f\n", 4.5); //  4.50
		System.out.printf("%5.2f\n", 24.567); //24.57
		System.out.printf("%5.2f\n", 124.5); //124.50
		
		System.out.printf("%-5.2f\n", 124.5); //124.50
		
		
		System.out.printf("%-10s\n", "korea");
		System.out.printf("%10s\n", "korea");
		
		
		//홍길동님의 평균은 95.0 이므로 A학점입니다.
		System.out.printf("%s님의 평균은%.1f이므로 %c학점입니다.\n", "홍길동" , 95.0, 'A');
		
		// 3 + 2 = 5
		System.out.printf("%d + %d = %d\n", 3 , 2, (3+2) );
		
		//3 % 2 = 1
		System.out.printf("%d %% %d = %d\n", 3, 2, 3%2 );
		
		//당신의 색깔은 "파랑"입니다.
		System.out.printf("당신의 색깔은 \"%s\"입니다\n", "파랑");
		
		
	}//end main()

}//end class
