package java0418_basic;

public class java009_operator {

	public static void main(String[] args) {
		
		int i = 5;
		int j = 0;
		int k = 0;
		
//		j = i++; //
		System.out.println(j);
		
		//전위형 : 값이 참조되기 전에 증가시킨다.
		j = ++i;
		System.out.println("i=" + i + ",j="+j);
		
		k = i++;//후위형 : 값이 참조된 후 에 증가시킨다.
		System.out.println("i=" + i + ",k=" + k);

	}// end main()

}//end class
