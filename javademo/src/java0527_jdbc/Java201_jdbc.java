package java0527_jdbc;

import java.util.List;

/*
 * MVC패턴 : 컴퓨터 소프트웨어 개발의 구조적 패턴 (패턴의종류 : MVC패턴, 싱글톤패턴)
 * Model : 데이터에 관련된 작업(DepartmentDTO, DepartmentDAO)
 * view : 클라이언트에 보여줄 화면(java201_Jdbc)
 * Controller : 요청,응답처리(java201_Jdbc)
 */
public class Java201_jdbc {

	public static void main(String[] args) {
		Department_DAO dao = Department_DAO.getInstence();
		List<DepartmentDTO> aList=dao.listMethod();
		for(int i=0; i<aList.size(); i++) {
			DepartmentDTO dto=aList.get(i);
			System.out.printf("%d %s %d %d\n",
					dto.getDepartment_id(),
					dto.getDepartment_naem(),
					dto.getManager_id(),
					dto.getLocation_id());
		}
		
		
	}//end main()

}//end class
