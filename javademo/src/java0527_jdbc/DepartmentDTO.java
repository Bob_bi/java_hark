package java0527_jdbc;
//Vo 같은거임
//DTO(Data Transfer Object ) : 데이터 관리 객체
public class DepartmentDTO { // DB에 있는 테이블에 있는 컬럼명과 똑같이 해야 한다.
	private int department_id;
	private String department_naem;
	private int manager_id;
	private int location_id;//캡슐화 과정으로 겟터 셋터를 많들어야함
	
	public DepartmentDTO() {
		
	}

	public int getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}

	public String getDepartment_naem() {
		return department_naem;
	}

	public void setDepartment_naem(String department_naem) {
		this.department_naem = department_naem;
	}

	public int getManager_id() {
		return manager_id;
	}

	public void setManager_id(int manager_id) {
		this.manager_id = manager_id;
	}

	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	
	

}//end class
