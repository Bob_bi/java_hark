package java0527_jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import templet.JdbcTemplate;

public class EmpDAO {

	private Connection conn;
	private Statement stmt;
	private PreparedStatement pstmt;
	private ResultSet rs;
	
	private static EmpDAO dao = new EmpDAO();
	
	private EmpDAO() {}
	
	public static EmpDAO getInstance() {
		return dao;
	}
	
	public List<EmpDTO> rangeMethod(HashMap<String, Integer> map){
		List<EmpDTO> aList = new ArrayList<EmpDTO>();
		try {
			conn = JdbcTemplate.getConnection();
			String sql = "select b.* ";
			sql += "from(select rownum as rm, a.* ";
			sql += "from(select * from employees ";
			sql += "order by employee_id)a)b ";
			sql += "where b.rm>=? and b.rm<=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, map.get("start"));
			pstmt.setInt(2, map.get("end"));
			rs = pstmt.executeQuery();
			while(rs.next()) {
				EmpDTO dto = new EmpDTO();
				dto.setEmployee_id(rs.getInt("employee_id"));
				dto.setFirst_name(rs.getString("first_name"));
				dto.setHire_date(rs.getDate("hire_date"));
				dto.setSalary(rs.getInt("salary"));
				aList.add(dto);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		return aList;
	}//end rangeMethod()
	
	
	public List<EmpDTO> searchMethod(String word){
		List<EmpDTO> aList = new ArrayList<EmpDTO>();
		//내꺼
//		try {
//			conn=JdbcTemplate.getConnection();
//			String str ="select employee_id, first_name, salary, hire_date\r\n" + 
//						"from employees\r\n" + 
//						"where upper(first_name) like upper('%"+word+"%')";
//			stmt=conn.createStatement();
//			rs=stmt.executeQuery(str);
//			while(rs.next()) {
//				EmpDTO dto = new EmpDTO();
//				dto.setEmployee_id(rs.getInt("employee_id"));
//				dto.setFirst_name(rs.getString("first_name"));
//				dto.setSalary(rs.getInt("salary"));
//				dto.setHire_date(rs.getDate("hire_date"));
//				aList.add(dto);
//				
//			}
//		} catch (ClassNotFoundException | SQLException e) {
//		
//			e.printStackTrace();
//		}finally {
//			JdbcTemplate.close(rs);
//			JdbcTemplate.close(pstmt);
//			JdbcTemplate.close(conn);
//		}
		//선생님꺼
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "select * from employees ";
					sql += "where upper(first_name) like ?";
					sql += " order by employee_id";
					pstmt=conn.prepareStatement(sql);
					pstmt.setString(1, "%"+word.toUpperCase()+"%");
					rs=pstmt.executeQuery();
					while(rs.next()) {
						EmpDTO dto = new  EmpDTO();
						dto.setEmployee_id(rs.getInt("employee_id"));
						dto.setFirst_name(rs.getString("first_name"));
						dto.setSalary(rs.getInt("salary"));
						dto.setHire_date(rs.getDate("hire_date"));
						aList.add(dto);					}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		
		return aList;
	}//end searchMethod()
	
	public List<EmpDTO> comboxSearchMethod(HashMap<String, Object> map){

		List<EmpDTO> aList = new ArrayList<EmpDTO>();

		try {

			conn = JdbcTemplate.getConnection();

			String sql = "select * from employees ";

			Set key = map.keySet();

			Iterator<String> ite = key.iterator();

			String stnkey = ite.next();

			if(stnkey.equals("전체")) {

				stmt = conn.prepareStatement(sql);

				rs = stmt.executeQuery(sql);

				while(rs.next()) {

					EmpDTO dto = new EmpDTO();

					dto.setEmployee_id(rs.getInt("employee_id"));

					dto.setFirst_name(rs.getString("first_name"));

					dto.setHire_date(rs.getDate("hire_date"));

					dto.setSalary(rs.getInt("salary"));

					aList.add(dto);

				}

			}else if(stnkey.equals("사원명")){

				String word = (String)map.get("사원명");

				System.out.println(word);

				sql += "where upper(first_name) like ? ";

				pstmt = conn.prepareStatement(sql);

				pstmt.setString(1, "%" + word.toUpperCase() + "%");

				rs = pstmt.executeQuery();

				while(rs.next()) {

					EmpDTO dto = new EmpDTO();

					dto.setEmployee_id(rs.getInt("employee_id"));

					dto.setFirst_name(rs.getString("first_name"));

					dto.setHire_date(rs.getDate("hire_date"));

					dto.setSalary(rs.getInt("salary"));

					aList.add(dto);

				}

			}else if(stnkey.equals("급여")) {

				int salary = Integer.valueOf((String)map.get("급여"));

				System.out.println(salary);

				 sql += "where salary >=" + salary;

				 stmt = conn.prepareStatement(sql);

				 rs = stmt.executeQuery(sql);

				 while(rs.next()) {

					EmpDTO dto = new EmpDTO();

					dto.setEmployee_id(rs.getInt("employee_id"));

					dto.setFirst_name(rs.getString("first_name"));

					dto.setHire_date(rs.getDate("hire_date"));

					dto.setSalary(rs.getInt("salary"));

					aList.add(dto);

				}

			}

			

		} catch (ClassNotFoundException | SQLException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} finally {

			JdbcTemplate.close(rs);

			JdbcTemplate.close(stmt);

			JdbcTemplate.close(pstmt);

			JdbcTemplate.close(conn);

		}
		return aList;
	}//comboxSearchMethod()
}//end class
