package java0527_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//DAO(Data Access Object) : 데이터 접근 객체 (DB에 접근 하는 클래스)
public class Department_DAO {
	private Connection conn=null;
	private Statement stmt = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	private static Department_DAO dao = new Department_DAO();
	
	private Department_DAO() {
	
	}
	public static Department_DAO getInstence () {
		return dao;}
	
	private Connection init() throws ClassNotFoundException, SQLException {// 커넥션을 메소드로 많들어 계속 연결해줄필요없이 메소드 호출로서 커넥션 객체값을 연결한다.
		Class.forName("oracle.jdbc.OracleDriver");
		String url = "jdbc:oracle:thin://@127.0.0.1:1521:xe";
		String username = "hr";
		String password = "a1234";
		return DriverManager.getConnection(url, username, password); // 커넥션으로 return
	}//end init()
	
	
	private void exit() {
		if(rs!=null)
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if(stmt!=null)
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if(conn!=null)
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}//exit()
	public List<DepartmentDTO> listMethod(){
		List<DepartmentDTO> aList = 
				new ArrayList<DepartmentDTO>();
		try {
			conn=init();
			stmt=conn.createStatement();// 쿼리문을 실행 하려면  커넥션에 있는 createStatment을 먼저 받아와서 stmt을 실행시켜야한다.
			String sql = "select * from departments or"
					+ "der by depa"
					+ "rtment_id";
			rs=stmt.executeQuery(sql);
			while(rs.next()) {
				DepartmentDTO dto = new DepartmentDTO();
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_naem(rs.getString("department_name"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
				aList.add(dto);
			}
		} catch (ClassNotFoundException | SQLException e) {// 두개의 익셉션을 같이 한다.
			e.printStackTrace();
		}finally {
			exit();
			
		}
		return aList;
	}//end listMethod()
	
	public List<DepartmentDTO> searchMethod(String data){
		List<DepartmentDTO> aList=
				new ArrayList<DepartmentDTO>();
		
		try {
			conn=init();
			stmt=conn.createStatement();
			String sql="select * from departments where lower(department_name) like lower('%"+data+"%')";
			rs=stmt.executeQuery(sql);
			while(rs.next()) {
				DepartmentDTO dto = new DepartmentDTO();
				dto.setDepartment_id(rs.getInt("department_id"));
				dto.setDepartment_naem(rs.getString("department_name"));
				dto.setManager_id(rs.getInt("manager_id"));
				dto.setLocation_id(rs.getInt("location_id"));
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return aList;
	}
}//end class
