package java0527_jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class MemDAO {
	private Connection conn=null;
	private Statement stmt=null;
	private PreparedStatement pstmt=null;
	private ResultSet rs=null;
	
	private MemDAO() {
		
	}
	private static MemDAO dao = new MemDAO();
	
	public static MemDAO getInstance() {
		return dao;
		
	}
	

}//end class
