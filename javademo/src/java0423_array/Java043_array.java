package java0423_array;

/*
 * num 배열의 저장된 요소의 합계를 구하는 프로그램을 구현하시오
 * 
 * [출력결과]
 * 합계 : 45
 */

public class Java043_array {

	public static void main(String[] args) {
		int[] num = new int[] {22, 3, 8, 12};
		int sum = 0;
		
		for(int index=0; index<num.length; index++) {
			sum += num[index];
		}
		
		System.out.println("num배열의 합은="+sum);

	}//end main()

}//end class
