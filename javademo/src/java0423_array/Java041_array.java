package java0423_array;

import java.lang.reflect.Array;

//변수 : 하나의 값을 저장하기 위한 메모리 공간
public class Java041_array {

	public static void main(String[] args) {
		int ko = 90;
		int en = 80;
		int jp = 40;
		
		int[] array; // int array[]
		//배열생성 = 메모리 생성(확보)
		//new 키워드를 이용해서 배열을 생성한다.
		// 배열의 크기를 5이다.
		//각 요소마다 인덱스가 0부터 생성된다.
		array = new int[5];
		System.out.printf("array[0]=%d\n", array[0]);
		System.out.printf("array[1]=%d\n", array[1]);
		System.out.printf("array[2]=%d\n", array[2]);
		System.out.println("===========================");
		array[0] = 90; //90을 array배열에 0 인덱스(index)에 저장
		array[1] = 80; //80을 array배열에 1 인덱스(index)에 저장
		array[2] = 40; //40을 array배열에 2 인덱스(index)에 저장
		/*
		 * 
		 * 
		 *  배열의 데이터 저장 == 배열을 선언하면 stack에 4바이트 짜리 메모리가 생긴다.
		 *  그 이후 배열의 크기를 정하면 힙에 각각 4바이트짜리 메모리 공간이 생긴다. 배열의 크기가 3이면 4바이트 짜리 3개가 생긴다.
		 *  점수를 정해주면 힙에 있는 메모리 주소 첫재짜리가 스택에 있는 공간으로 나온다 그러므로 힙에 있는 메모리가 스택에 메모리를 가르킨다.
		 */
		//length : 배열의 크기 => 배열명.length[키워드]
		for(int index=0; index<=array.length; index++) {
			System.out.println("array[" + index + "]=" + array[index]);
			System.out.printf("array[%d]=%d\n", index, array[index]);  
		}
		
		int[] jumsu = new int[5];
		
		

	}//end main()

}//end class
