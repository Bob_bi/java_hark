package java0423_array;

public class Java045_array {

	public static void main(String[] args) {
			int[] data1=new int[3]; // 0기본값
			long[] data2 = new long[3]; // 0L 기본값 <-(L)기호 아무것도 아닌 알려주는 기호
			float[] data3 = new float[3]; // 0.0F 기본갑
			double[] data4 = new double[3]; // 0.0 기본값
			char[] data5 = new char[3];// \u0000 (유니코드값 \u0000이 주어짐) 공백값이 아님 초기화된 값이다 null값 과 비슷한값
			boolean[] data6 = new boolean[3]; // false 기본값
			String[] data7 = new String[3]; // null 기본값
					
			for(int i=0; i<data7.length; i++) {
				System.out.printf("data[%d]=%s\n", i, data7[i]);
			}
			
			System.out.println("=================================");
			char a=' ';
			char b='\u0020';
			char c='\u0000';
			System.out.println(a==b);
			System.out.println(a==c);
			
			
		
	}//end main()

}//end class()
