package java0423_array;

public class Java047_array {

	public static void main(String[] args) {
		//3행 2열의 2차원 배열 생성
		int[][] sum = new int[3][2];
		
		System.out.printf("%4d", sum[0][0]);
		System.out.printf("%4d\n", sum[0][0]);
		System.out.printf("%4d", sum[1][0]);
		System.out.printf("%4d\n", sum[1][1]);
		System.out.printf("%4d", sum[2][0]);
		System.out.printf("%4d\n", sum[2][1]);
		
		sum[0][0] = 1;
		sum[0][1] = 2;
		sum[1][0] = 3;
		sum[1][1] = 4;
		sum[2][0] = 5;
		sum[2][1] = 6;
		
		System.out.printf("%4d", sum[0][0]);
		System.out.printf("%4d\n", sum[0][0]);
		System.out.printf("%4d", sum[1][0]);
		System.out.printf("%4d\n", sum[1][1]);
		System.out.printf("%4d", sum[2][0]);
		System.out.printf("%4d\n", sum[2][1]);
		System.out.println("=================================");
		
		// num.length : 2차원배열에서 행의 크기를 리턴
		//num[row].length : 2차원에서 열의 크기를 리턴
		for(int row=0; row<sum.length; row++) {// 행의 크기
			for(int col=0; col<sum[row].length; col++) {// 각 행마다 열의 크기를 나타낸다				
				System.out.printf("%4d", sum[row][col]);
			}
			System.out.println();
		}
		System.out.println("======================================");
		/*
		 * 1 3 5
		 * 2 4 6
		 */
		
		//num[0][0] num[1][0] num[2][0]
		//num[0][1] num[1][1] num[2][1]
		
		for(int col=0; col<2; col++) {
			for(int row=0; row<3; row++) {
				System.out.printf("%4d", sum[row][col]);
			}
			System.out.println();
		}
		
	   	
	}//end main()

}//end class
