package java0423_array.prob;

/*
 * num배열에서 최대값을 출력하는 프로그램을 구현하시오.
 * [출력결과]
 * 최대값:95
 */
// 변수 이름 수정할때 전체 바꾸기 컨트롤 + 1 리네임누르면 코드안에 있는 똑같은 변수이름 바꿀수있다.
public class Prob_01 {
	public static void main(String[] args) {

		int[] num = { 94, 85, 95, 88, 90 };
		int sum = 0;
		for (int i = 0; i < num.length; i++) {
			if (num[i] >= sum) {
				sum = num[i];
			}

		}
		System.out.println("최대값:" + sum);

		// 여기를 구현하세요.

	}// end main()

}// end class
