package java0423_array;

/*
 * [데이타]
 * 홍길동 90  85  40
 * 갑동이 100 35  75
 * 
 * [출력 결과]
 * 홍길동  90  85  40  215  71.7
 * 갑동이  100 35  75  210  70.0
 * 
 */


public class Java049_array {

	public static void main(String[] args) {
		
		String[] name = new String[] {"홍길동", "갑동이"};
		int[][] sum = new int[][] {{90,85,40},{100,35,75}};
		
//		for(int i=0; i<name.length; i++) {
//			System.out.printf("%3s",name[i]);
//			int total=0;
//			for(int row=0; row<sum[i].length; row++) {
//						System.out.printf("%4d", sum[i][row]);
//						total += sum[i][row];
//			}
//			System.out.printf("%6d, %.2f\t", total, (float)(total/3) );
//			System.out.println();
//		}
		
		int total;
		for(int i=0; i<name.length;i++) {
			total=0;
			System.out.printf("%8s", name[i]);
			for(int j=0; j<sum[i].length; j++) {
				System.out.printf("%4d",sum[i][j]);
				total += sum[i][j];
			}
			System.out.printf("%4d %.1f", total, (double)total/3);
			System.out.println();
		}
		
		
		
		
		

	}//end main()

}//end class
