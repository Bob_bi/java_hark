package java0522_collection;

import java.util.Comparator;

public class Persom implements Comparator<Persom> {
	private String name;
	private int age;
	
	
	public Persom() {
		
	}
	
	public Persom(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	//이름 오름차순, 나이 내림차순
	@Override
	public int compare(Persom o1, Persom o2) {
		//이름 오름차순 o1.name.compareTo.(o2.name)
		//이름 오름차순 o2.name.compareTo.(o1.name)
		//나이 오름차순 > 1, < -1 ==0
		//나이 내림차순 > -1, < 1 ==0
		if(o1.getName().equals(o2.getName())){
			return new Integer(o2.getAge()).compareTo(new Integer(o1.getAge()));
		}else {
			return o1.getName().compareTo(o2.getName());
		}
	}
//	@Override
//	public int compare(Persom o1, Persom o2) {
//		
//		return o2.getAge().compareTo(o1.getAge());
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	



}
