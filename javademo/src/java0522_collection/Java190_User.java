package java0522_collection;

import javax.naming.NameAlreadyBoundException;

class Average<T extends Number>{ // number 이거나 number을 상속받은 클래스어야 가능하다. T 제한이 있어야 하는경우에만 사용
	private T[] list;
	
	public Average() {
	}
	
	public Average(T[] list) {
		this.list = list;
	}
	
	public double findAverage() {
		double sum=0.0;
		for(T data : list) {
			sum += data.doubleValue();
		}
		return sum / list.length;
	}
}

public class Java190_User {

	public static void main(String[] args) {
		Integer it[] = new Integer[] {1,5,3,2};
		Average<Integer> av = new Average<Integer>(it);
		System.out.println(av.findAverage());
		
//		Average<String> ae = null;
		Number nb[] = new Number[] {2,5.3f,4.5};
		Average<Number> aa = new Average<Number>(nb);
		System.out.printf("%.1f\n",aa.findAverage());

	}//end main()

}//end class
