package java0522_collection;

import java.util.Iterator;
import java.util.TreeSet;

//TreeSet : 오름차순 정렬, 중복 안됨
public class Java184_TreeSet {

	public static void main(String[] args) {
		//tree가 들어가는말은 정렬을 말한다. set은 중복된값이 들어갈수 없다.
		TreeSet<Integer> tree =new TreeSet<Integer>(); // 자료를 정렬해줘야 할때만 사용해야 한다. 중복이 안된다.
		tree.add(new Integer(10));
		tree.add(new Integer(40));
		tree.add(new Integer(20));
		tree.add(new Integer(30));
		
		System.out.println("오름차순 정렬===========================");
//		for(Integer it : tree)// 자동정렬이 되어서 나온다. (오름차순 정렬)
//			System.out.println(it);
		Iterator<Integer> ita =tree.iterator();//오름차순정렬
		while(ita.hasNext())
			System.out.println(ita.next());
		
		System.out.println("내림차순 정렬=======================");
		Iterator<Integer> ite =tree.descendingIterator();//내림차순정렬
		while(ite.hasNext()) {
			System.out.println(ite.next());
		}
		
	

	}//end main()

}//end class
