package java0522_collection;

import java.util.LinkedList;
import java.util.ListIterator;

public class Java181_LinkedList {

	public static void main(String[] args) {
		LinkedList<String> aNode = new LinkedList<String>();
		//추가
		aNode.add(new String("java"));
		aNode.add(new String("jsp"));
		aNode.add(new String("spring"));
		
		ListIterator<String> ite = aNode.listIterator(); // 이전꺼를 가져오고 싶으면 사용
		System.out.println("앞->뒤");
		while(ite.hasNext())// 커서 포인트가 한칸씩 앞으로 간다. 그렇기때문에 객체를 가져올수 있다.

			System.out.println(ite.next());
		
		System.out.println("뒤->앞");
		while(ite.hasPrevious())
			System.out.println(ite.previous());// 프리비우스를 사용하고 싶으면 반드시 넥스트를 사용한 후에 사용해야 한다.
		
	}//end main()

}//end class
