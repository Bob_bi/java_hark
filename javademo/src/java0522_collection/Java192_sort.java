package java0522_collection;

import java.awt.color.ICC_ColorSpace;
import java.util.ArrayList;
import java.util.Arrays;

public class Java192_sort {

	public static void main(String[] args) {
		Integer[] arr = new Integer[] {1,3,5,2,4};
		ArrayList<Integer> aList=
				new ArrayList<Integer>(Arrays.asList(arr)); // 배열로 있는것을 리스트로 변환할때 사용
		
		//오름차순 정열
		aList.sort(new Ascending());
		System.out.println(aList);
		
		System.out.println("=========================================");
		//내림차순 정열
		aList.sort(new Descending());
		System.out.println(aList);

	}//end main()

}//end class
