package java0522_collection;

class Box<T>{
	private T data;
	public Box() {
	}
	public Box(T data) {
		this.data = data;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}//end class

public class Java189_User {

	public static void main(String[] args) {
		Box<Integer> box = new Box<Integer>(); // T에 오는 유형에 따라 T는 유형이 바뀐다.
		box.setData(new Integer(10));
		System.out.println(box.getData());
		
		Box<String> box2 = new Box<String>(); // T에 오는 유형에 따라 T는 유형이 바뀐다.
		box2.setData(new String("java"));
		System.out.println(box2.getData());
		

	}//end main()

}//end class
