package java0522_collection;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;

import java0501_abstract_interface.part05.Animal;

/*
 * Hashtable
 * 1 Map인터페이스 구현해 놓은 클래스
 * 2 Map인터페이스를 구현해놓은 클래스들은 key, value쌍으로 저장한다.
 * 3.value을 구현해주는 것은 key이므로 key은 중복을 허용하지 않는다.
 */
public class Java186_Hashtable {

	public static void main(String[] args) {
		Hashtable<Integer, String> table = new Hashtable<Integer, String>();
		//넣을때
		table.put(10, "java");
		table.put(20, "jsp");
		table.put(30, "spring");
		
		//꺼내올때
		System.out.println(table.get(20));
		
		System.out.println("=====Enumeration================");//열거해주기
		Enumeration<Integer> eu = table.keys();
		while(eu.hasMoreElements()) {//가지고올 요소가 있습니까?
			Integer key = eu.nextElement();
			System.out.printf("%d:%s\n",key,table.get(key)); //키만 열거가 된다.
		
		}
	}//end main()

}//end class
