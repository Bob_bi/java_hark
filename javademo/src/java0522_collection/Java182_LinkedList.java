package java0522_collection;

import java.util.LinkedList;

/*
 * stack(스텍)
 * 1 LIFO(Last In First Out) : 마지막에 저장된 요소를 먼저 꺼낸다.
 * 2 수식계산, 수식괄호검사, undo/redo, 뒤로/앞으로
 */
public class Java182_LinkedList {

	public static void main(String[] args) {
		LinkedList<String> nStack = new LinkedList<String>();
		//추가
		nStack.push(new String("java"));
		nStack.push(new String("jsp"));
		nStack.push(new String("spring"));
		
		System.out.println("size :"+nStack.size());
		
//		System.out.println(nStack.pop());// 스텍으로 집어넣었다가 꺼낼때는 리스트에  있는 자료를 꺼내온다. (add로 집어넣고 get으로 꺼내면 주소를 복사해온다.)
//		System.out.println(nStack.pop());
//		System.out.println(nStack.pop());
		
		while(!nStack.isEmpty())//메모리가 비어있니?
			System.out.println(nStack.pop());
		
		

	}//end main()

}//end class
