package java0522_collection;

public class Java193_Sort {

	public static void main(String[] args) {
		String a = "ABCqwder24";
		String b = "ABC";
		String d = "ABC";
		String e = "ab";
		
		
		System.out.println(b.compareTo(d)); // 0
		//65-97=-32
		System.out.println(d.compareTo(e));//-32
		//97-65=32
		System.out.println(e.compareTo(d));//32
		
		//앞에 문자가 같으면 뒤에 있는 문자갯수가 나온다.(길이)
		System.out.println(a.compareTo(b));// 7
		//-값이 나온다.
		System.out.println(b.compareTo(a));//-7

	}//end main()

}//end class
