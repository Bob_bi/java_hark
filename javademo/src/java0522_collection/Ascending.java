package java0522_collection;

import java.util.Comparator;

public class Ascending implements Comparator<Integer> {

	@Override
	public int compare(Integer o1, Integer o2) { // 내부에서는 o1이 두번째 값이고 o2가 첫번째값이다
		System.out.println((o1+"/"+o2+" ") // sort메소드를 호출을 하면서 내부에서는 compare라는 메소드가 실행한다.
				+o1.compareTo(o2));
		return o1.compareTo(o2);
	}//end compare()
	

}//end class
