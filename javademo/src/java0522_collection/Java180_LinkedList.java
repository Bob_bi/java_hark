package java0522_collection;

import java.util.ArrayList;
import java.util.LinkedList;

/*
 * 1 배열 구조로 데이터 관리 : Vector, ArrayList
 *               	ArratList-추가--(순차적으로 처리할때 사용)
 * 2 링크로 데이터 관리 : LinkedList 
 *              	사입, 삭제 -(비순차적으로 처리할때 사용)
 * 3 List을 구현해놓은 컬렉션
 *   Vector, ArrayList, LinkedList은 add()해준 순서대로
 *      데이터관리를 해준다.
 */

//List 순차적인 리스트를 활용할때 사용하는것

public class Java180_LinkedList {

	public static void main(String[] args) {
		ArrayList<String> aList = new ArrayList<String>();
		aList.add(new String("java"));
		aList.add(new String("jsp"));
		aList.add(new String("spring"));
		for(String a : aList)
			System.out.println(a);
		
		System.out.println("LinkedList=============================================");
		
		LinkedList<String> aNode = new LinkedList<String>();
		//추가
		aNode.add(new String("java"));
		aNode.add(new String("jsp"));
		aNode.add(new String("spring"));
		for(int i=0; i<aNode.size(); i++) {
			System.out.println(aNode.get(i));
		}
		
		//삽입
		aNode.add(1,new String("ajax"));
		System.out.println(aNode);
		
		//삭제
		aNode.remove(2);
		System.out.println(aNode);
		
		
	}//end main()
	
}//end class
