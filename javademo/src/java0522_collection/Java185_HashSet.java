package java0522_collection;

import java.util.HashSet;

//HashSet : 중복허용안되고, 순서유지 안된다.
public class Java185_HashSet {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<Integer>();
		set.add(new Integer(10));
		set.add(new Integer(20));
		set.add(new Integer(30));
		
		for(Integer a : set)
			System.out.println(a);

	}//end main()

}//end class
