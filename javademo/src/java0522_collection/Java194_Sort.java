package java0522_collection;

import java.util.ArrayList;

public class Java194_Sort {

	public static void main(String[] args) {
		ArrayList<Persom> ts = new ArrayList<Persom>();
		ts.add(new Persom("홍길동", 40));
		ts.add(new Persom("가비", 20));
		ts.add(new Persom("가비", 30));
		ts.add(new Persom("나비", 15));
		ts.sort(new Persom());
		for(Persom ps : ts)
			System.out.printf("%s %d\n",ps.getName(),ps.getAge());

	}//end main()

}//end class


/*
 * client(Front end)   Sever(Back end)
 *   HTML                  JAVA형자료ㅛ
 *   CSS                      JSP  sevlerte spring
 * 
 * 
 * 
*/
