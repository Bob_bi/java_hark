package practics_java06;

public class ProbDTO {
	
	private String name;
	private String name_id;
	private int number;
	
	public ProbDTO() {
		
	}
	
	public ProbDTO(String name, String name_id, int number) {
		this.name=name;
		this.name_id=name_id;
		this.number=number;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName_id() {
		return name_id;
	}

	public void setName_id(String name_id) {
		this.name_id = name_id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	
	
	

}
