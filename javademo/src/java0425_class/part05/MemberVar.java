package java0425_class.part05;

public class MemberVar {
	//멤버변수는 기본값을  제공한다.
	byte var_byte; //0 1바이트
	short var_short;// 0 2바이트
	int var_int; //0 4바이트
	long var_long;//0L 8바이트
	float var_float;//0.0F 4바이트
	double var_double;//0.0 8바이트
	boolean var_boolean;// false 1바이트
	char var_char;//\u0000 2바이트
	int[] num;// null // 4바이트
	String str; // null 
	

}
