package java0425_class.part05;

public class Java069_variable {

	public static void main(String[] args) {
		MemberVar mv=new MemberVar();
		System.out.printf("var_byte:%d\n", mv.var_byte);//0
		System.out.printf("var_shotr:%d\n", mv.var_short);//0
		System.out.printf("var_int:%d\n", mv.var_int);//0
		System.out.printf("var_long:%d\n", mv.var_long);//0
		System.out.printf("var_float:%f\n", mv.var_float);//0.0F
		System.out.printf("var_double:%f\n", mv.var_double);//0.0
		System.out.printf("var_boolean:%b\n", mv.var_boolean);//false
		System.out.printf("var_char:%c\n", mv.var_char);//\u0000
		System.out.printf("int[]:%s\n", mv.num);//null
		System.out.printf("Stirng:%d\n", mv.str);//null

	}//end main()

}//end class
