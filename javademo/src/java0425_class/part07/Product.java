package java0425_class.part07;

/*
 * this : 객체 자신을 의미한다.
 * this.멤버변수;
 * this.메소드();
 * this.(); 생성자
 */




public class Product {

	String code;
	String pname;
	int cnt;
	
	Product() {
	}
	
	Product(String code, String pname, int cnt){
		//멤버변수와 매개벼수 이름이 같을 때 멤버변수에 this키워드를 명시한다.
		this.code = code;
		this.pname = pname;
		this.cnt = cnt; // this 생성된 객체에서의 클래스 내에 멤버변수 나자신을 가리킴
	}//static에서는 this사용할수 없다.
	
	void prn() {
		System.out.printf("%s %s %d\n", code, pname, cnt);
	}
	
//스프링 프레임워크 에서는 기본생성자를 만들어야 잘돌아감
}//end class
