package java0425_class.part06;

public class HandPhone {
	//멤버변수
	String name;
	String number;
	
	//생성자
	HandPhone() {
		
	}
	
	HandPhone(String name, String number) {
		this.name=name;
		this.number=number;
	}
	
	
	
	//메소드
	void prn() {
		System.out.printf("%s %s\n", name, number);
	}

}//end class
