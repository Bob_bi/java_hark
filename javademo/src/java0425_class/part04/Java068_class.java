package java0425_class.part04;

/*
 * [출력 결과]
 * 메뉴명: 떡볶이
 * 가격: 2000
 * 갯수:  3
 * 메뉴금액:6000
 * 
 * 메뉴명: 김말이
 * 가격: 500
 * 갯수: 2
 * 메뉴금액:1000
 * 
 * 메뉴명: 오뎅
 * 가격: 300
 * 갯수: 4
 * 메뉴금액:1200
 * ==================
 * 총금액:8200
 */
public class Java068_class {

	public static void main(String[] args) {
		
		MenuShop a1= new MenuShop();
		a1.menu="떡볶이";
		a1.price = 2000;
		a1.cnt = 3;
		a1.prn();
		System.out.println("=================");
		MenuShop a2 = new MenuShop();
		a2.menu="김말이";
		a2.price = 500;
		a2.cnt = 2;
		a2.prn();
		System.out.println("=================");		
		MenuShop a3 = new MenuShop();
		a3.menu="오뎅";
		a3.price = 300;
		a3.cnt = 4;
		a3.prn();
		System.out.println("=================");
		
		int sum = a1.count()+a2.count()+a3.count();
		System.out.println("총금액:"+(a1.count()+a2.count()+a3.count())+"원");

	}//end main()

}//end class
