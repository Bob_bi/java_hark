package java0425_class.part01;

// 임포트 단축키 : 컨트롤 + 쉬프트 + O
public class Java065_class {

	
	public static void main(String[] args) {
		Person ps;//객체 선언-메모리 생성이 안됨
		ps = new Person();//객체생성-메모리 생성이 됨
		//객체참조변수.멤버변수
		ps.name="홍길동";
		ps.age = 30;
		ps.gen = 'M';
		System.out.printf("%s %d %c\n", ps.name, ps.age, ps.gen);
		//객체 참조 변수.메소드
		ps.eat();
		ps.run();
		
		//객체 선언 및 생성
		Person pn = new Person();
		pn.name = "이영희";
		pn.age = 25;
		pn.gen = 'F';
		System.out.printf("%s %d %c\n",pn.name, pn.age, pn.gen);
		pn.eat();
		pn.run();
		
		
		//System.out.println(ps.name); // 멤버변수는 기본값을 할당하지 않으면 기본값이 설정됨/ 모든 참조데이터 타입은 null
		//System.out.println(ps.age);

	}//end mian()

}//end class
