package java0529_gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import templet.JdbcTemplate;

public class JoinDAO {
	private Connection conn;
	private Statement stmt;
	private PreparedStatement pstmt;
	private ResultSet rs;
	
	private static JoinDAO dao = new JoinDAO();
	
	private JoinDAO() {
		
	}
	
	public static JoinDAO getInstance() {
		return dao;
	
	
	}
	public List<EmpDTO> listMethod(){
		List<EmpDTO> aList=new ArrayList<EmpDTO>();
		
		try {
			conn=JdbcTemplate.getConnection();
			stmt=conn.createStatement();
			String sql = "select e.employee_id,e.first_name,e.salary, " + 
					"d.department_id,d.department_name " + 
					"from employees e, departments d " + 
					"where e.department_id=d.department_id " + 
					"order by e.employee_id";
			rs=stmt.executeQuery(sql);
			while(rs.next()) {
				EmpDTO edto=new EmpDTO();
				edto.setEmplyee_id(rs.getInt("employee_id"));
				edto.setFirst_name(rs.getString("first_name"));
				edto.setSalary(rs.getInt("salary"));
				DeptDTO dept = new DeptDTO();
				dept.setDepartment_id(rs.getInt("department_id"));
				dept.setDepartment_name(rs.getString("department_name"));
				edto.setDept(dept); // 1:1로 되어있을때        일대 다로 되었을때는 list제네릭으로 해준다.
				
				aList.add(edto);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(stmt);
			JdbcTemplate.close(conn);
		}
		return aList;
	}

}//end class
