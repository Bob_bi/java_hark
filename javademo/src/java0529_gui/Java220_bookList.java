package java0529_gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java0521_collection.prob.BookDTO;
import templet.JdbcTemplate;

public class Java220_bookList {
		private static Connection conn;
		private static PreparedStatement pstmt;
		private static Statement stmt;
		private static ResultSet rs;
		
		private static Java220_bookList book = new Java220_bookList();
		
		public Java220_bookList() {
			
		}
		public static Java220_bookList getinstence() {
			return book;
		}
		
		public static void book() {
		ArrayList<BookDTO> bookList = new ArrayList<BookDTO>();
		bookList.add(new BookDTO("Java Programming", "컴퓨터", 1400));
		bookList.add(new BookDTO("SQL Programming", "컴퓨터", 1700));
		bookList.add(new BookDTO("Servlet Programming", "컴퓨터", 2300));
		bookList.add(new BookDTO("JDBC Programming", "컴퓨터", 700));
		bookList.add(new BookDTO("EJB Programming", "컴퓨터", 4200));
		bookList.add(new BookDTO("아버지", "소설", 1500));
		bookList.add(new BookDTO("고등어", "소설", 1700));
		bookList.add(new BookDTO("논리야 놀자", "소설", 1700));
		bookList.add(new BookDTO("개미", "소설", 1300));
		bookList.add(new BookDTO("찔레꽃", "소설", 1000));
		bookList.add(new BookDTO("톰 과 제리", "만화", 2000));
		
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "insert into bookList values(?,?,?)";
			pstmt=conn.prepareStatement(sql);
			for(BookDTO as : bookList) {
				pstmt.setString(1, as.getTitle());
				pstmt.setString(2, as.getKind());
				pstmt.setInt(3, as.getRentalPrice());
				pstmt.addBatch();// 메모리에  값들을 올린다.
			}
			int[] cnt =pstmt.executeBatch();// 그 올린 값들을 한번에 db에 저장한다.
			System.out.println("완료");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs); 
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		
		
		
		}//end bookList
		
		public static void booklist() {
			try {
				conn=JdbcTemplate.getConnection();
				String sql = "select * from booklist";
				stmt=conn.createStatement();
				rs=stmt.executeQuery(sql);
				while(rs.next()) {
					System.out.printf("%s, %s %d\n",
							rs.getString("title"),
							rs.getString("kind"),
							rs.getInt("rentalprice"));
				}
				System.out.println();
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
		
		public static int getRentalPrice(String kind) {
			int sum=0;
			try {
				conn=JdbcTemplate.getConnection();
				String sql = "select rentalprice from booklist where kind='"+kind+"'";
				stmt=conn.prepareStatement(sql);
				rs=stmt.executeQuery(sql);
				while(rs.next()) {
						sum+=rs.getInt("rentalprice");
					
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				JdbcTemplate.close(rs);
				JdbcTemplate.close(pstmt);
				JdbcTemplate.close(conn);
			}
			return sum;
		}
		
		public static void list() {
			int num =getRentalPrice("컴퓨터");
			System.out.println("컴퓨터책 대여수입 : "+ num);
			//소설책 대여수입 :
			int num1 =getRentalPrice("소설");
			System.out.println("소설책 대여수입 : "+ num1);		
			//만화책 대여수입 : 
			int num2 =getRentalPrice("만화");
			System.out.println("만화책 대여수입 : "+ num2);
		}
		
}//end class
