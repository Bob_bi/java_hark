package java0529_gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import templet.JdbcTemplate;

public class Info_DAO {
	private static Connection conn;
	private static Statement stmt;
	private static PreparedStatement pstmt;
	private static ResultSet rs;
	
	private static String name;
	private static int age;
	private static String phon;
	private static int num;
	
	private static Info_DAO info = new Info_DAO();
	
	public Info_DAO() {
	
	}
	
	public static Info_DAO getInstance() {
		return info;
	}
	
	public static List<Infro_DTO> Search() {
		Scanner sc = new Scanner(System.in);
		List<Infro_DTO> aList = new ArrayList<Infro_DTO>();
		System.out.print("이름을 입력하세요: ");
		String name =sc.nextLine();
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "select * from info ";
			sql += "where name like ?";
			
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, "%"+name+"%");
			rs=pstmt.executeQuery();
			while(rs.next()) {
				Infro_DTO dto= new Infro_DTO();
				dto.setName(rs.getNString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setPhon(rs.getNString("phone"));
				aList.add(dto);
				
			}	
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		return aList;
	}
	
	public static void input() {
		Infro_DTO dto = new Infro_DTO();
		Scanner sc = new Scanner(System.in);
		System.out.println();

		System.out.println("===========입력창입니다.===============");
		System.out.print("이름 : ");
		name=sc.next();
		System.out.print("나이 : ");
		age = sc.nextInt();
		System.out.print("연락처 : ");
		phon = sc.next();
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "insert into info(name,age,phone) values(?,?,?)";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setInt(2, age);
			pstmt.setString(3, phon);
			pstmt.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
	
	}//end input
	
	public static void update() {
		System.out.print("이름(기본에 있는 이름) :");
		Scanner sc = new Scanner(System.in);
		String name = sc.nextLine();
		System.out.print("수정하실  번호를 입력하세요 : ");
		String phon = sc.nextLine();
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "update info set Phone='"+phon+"' where name='"+name+"'";
			stmt =conn.prepareStatement(sql);
		rs=stmt.executeQuery(sql);
		System.out.println("수정완료");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		
	}//end update
	
	public static void delete() {
		System.out.println("삭제하실 이름을 적어주세요 : ");
		Scanner sc = new Scanner(System.in);
		String name =sc.nextLine();
		String sql="delete info where name='"+name+"'";
		try {
			conn=JdbcTemplate.getConnection();
			stmt=conn.prepareCall(sql);
			rs=stmt.executeQuery(sql);
			System.out.println("삭제완료");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		
	}//end delete()
	
	public static List<Infro_DTO> infor() {
		List<Infro_DTO> aList = new ArrayList<Infro_DTO>();
		try {
			conn=JdbcTemplate.getConnection();
			String sql = "select * from info ";
			stmt=conn.prepareStatement(sql);
			rs=stmt.executeQuery(sql);
			while(rs.next()) {
				Infro_DTO dto= new Infro_DTO();
				dto.setName(rs.getNString("name"));
				dto.setAge(rs.getInt("age"));
				dto.setPhon(rs.getNString("phone"));
				aList.add(dto);
				
			}	
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(pstmt);
			JdbcTemplate.close(conn);
		}
		return aList;
	}
	


}//end class
