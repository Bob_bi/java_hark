package java0529_gui;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/*
 * 작업을 선택 하세요(1:검색 2:입력, 3:수정, 4:삭제) : 2
 * 이름 : 홍길동
 * 나이 : 30
 * 연락처 : 010,2999,1535
 * 
 * 
 */
public class Java219_gui {

	public static void main(String[] args) {
		
		
		while(true) {
			System.out.print("작업을 선택 하세요(1:검색 2:입력, 3:수정, 4:삭제, 5:명단, 6:종료) : ");
			Scanner sc = new Scanner(System.in);
			int num = sc.nextInt();
			if(num==1) {
				List<Infro_DTO> dto = Info_DAO.getInstance().Search();
				for(Infro_DTO a : dto) {
					System.out.printf("%s, %d, %s\n",
							a.getName(),
							a.getAge(),
							a.getPhon());
				}
				System.out.println();
				continue;
			}else if(num==2) {
				Info_DAO.getInstance().input();
				System.out.println();
				continue;
			}else if(num==3) {
				Info_DAO.getInstance().update();
				System.out.println();
				continue;
			}else if(num==4) {
				Info_DAO.getInstance().delete();
				System.out.println();
				continue;
				
			}else if(num==5) {
				List<Infro_DTO> dto = Info_DAO.getInstance().infor();
				for(Infro_DTO a : dto) {
					System.out.printf("%s, %d, %s\n",
							a.getName(),
							a.getAge(),
							a.getPhon());
				}
				System.out.println();
				continue;
			}else if(num==6)
				System.out.println("종료");
			break;
		}
		

	}//end main()

}//end class
