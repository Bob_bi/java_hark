package java0529_gui;

public class EmpDTO {
	private int emplyee_id;
	private String first_name;
	private int salary;
	private DeptDTO dept;
	
	public EmpDTO() {
		
	}

	public int getEmplyee_id() {
		return emplyee_id;
	}

	public void setEmplyee_id(int emplyee_id) {
		this.emplyee_id = emplyee_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public DeptDTO getDept() {
		return dept;
	}

	public void setDept(DeptDTO dept) {
		this.dept = dept;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	

}
