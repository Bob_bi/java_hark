package part01;

import javax.tools.JavaFileObject.Kind;

public class Exculae {

	public static void main(String[] args) {
		
		Deck d = new Deck(); //카드  한 벌을 만든다.
		Card c = d.pick(); // 섞기 전에 제일 위의 카드를 뽑는다.
		System.out.println(c); 
		
		d.shuffle();
		c=d.pick(0);
		System.out.println(c);
		

	}//end main()

}//end class

class Deck{
	final int CARD_NUM = Card.Kind.values().length *
			Card.Number.values().length;//카드의 개수
	Card cardArr[] = new Card[CARD_NUM]; // Card객체 배열을 포함
	
	Deck() {
		
	}
	Card pick(int index) {//지정된 위치(index)에 있는 카드 하나를 꺼내서 반환
		return cardArr[index];
	}
	
	Card pick() {// Deck에서 카드 하나를 선택한다.
		int index = (int) (Math.random()*CARD_NUM);
		return null;
	}
	
	void shuffle() { //카드의 순서를 섞는다.
		for(int i=0; i<cardArr.length; i++) {
			int r =(int) (Math.random() * CARD_NUM);
		
			Card temp = cardArr[i];
			cardArr[i] = cardArr[r];
			cardArr[i] = temp;
		}	
	}
	
}//end class  

//card 클래스
class Card{
	enum Kind {
		CLOVER,HEART,DIAMOND,SPADE
		};
	enum Number{
		ACE,TWO,THREE,FOUR,FIVE,
		SIX,SEVEN,EIGHT,NINE,TEN,
		JACK,QUEEN,KING
	};
	
	static Kind Kind;
	Number num;
	
	Card(){
		this(Kind.SPADE , Number.ACE);
	}
	Card(Kind kind, Number num){
		this.Kind =kind;
		this.num = num;
	}
	public String toString() {
		return"["+Kind.name() + ","+num.name()+"]";
	}
}
