package java0419_statement;

/*
 * 
 *  1. data변수에 저장된 값이 영문자 대문자이면 "?는 대문자입니다."
 *     소문자이면 "?는 소문자입니다."
 *     그외는 "?는 기타입니다."로 출력하는 프로그램을 구현하시오
 *  2. [출력결과]
 *  data='D'   => D는 대문자입니다
 *  data='d'   => d는 대문자입니다
 *  data='1'   => 1는 대문자입니다
 */


public class java021_if {

	public static void main(String[] args) {
		char data = 'd';
		// 'A' => 유니코드 65      'Z'  => 유니코드 90
		// 'a' => 유니코드 97
		// '0' => 유니코드 48
		// 32 대문자와 소문자의 차이
		if (data>='A' && data<='Z') {
			System.out.printf("%c는  => \"%s\"입니다", data,"대문자");
		}else if(data >= 'a' && data <= 'z'){
			System.out.printf("%c는  => \"%s\"입니다.", data, "소문자");
		}else {
			System.out.printf("%c는 => \"%s\"입니다.", data, "기타");
		}
		
		
		

	}//end main()

}//end class
