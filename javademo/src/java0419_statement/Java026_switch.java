package java0419_statement;

public class Java026_switch {

	public static void main(String[] args) {
		
	int month = 11; //월
		
		//지역변수는 기본값을 제공하지 않는다.
		int lastDat = -1 ; //마지막 일
		
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 10:
		case 12:
			lastDat = 30; break;
		case 4:
		case 6:
		case 9:
		case 11:
			lastDat = 30; break;

		case 2:
			lastDat=28;break;
		}

		
	System.out.printf("%d월의 마지막일은 %d입니다.\n", month, lastDat);		

	}//end main()

}//end class
