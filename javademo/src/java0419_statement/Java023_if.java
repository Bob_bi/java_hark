package java0419_statement;

/*
 * 각 월의 마지막일
 * 1 3 5 7 8 10 12 = 31일
 * 4 6 9 11 => 30일
 * 2 => 28
 * 
 * [출력결과]
 * 4월의 마지막 일은 30일 입니다.
 */


public class Java023_if {

	public static void main(String[] args) {
		int month = 11; //월
		
		//지역변수는 기본값을 제공하지 않는다.
		int lastDat = -1 ; //마지막 일
		
		if(month <1 || month >12) {
			
			System.out.println("월은 1~12까지만 가능합니다.");
			
		}else if(month == 2) {
				System.out.printf("%d월의 마지막일은 %d일 입니다.", month, lastDat+29);
		}else if ( month == 4 || month == 6 || month ==9 || month == 11) {
				System.out.printf("%d월의 마지막일은 %d일 입니다.", month, lastDat+31);
		}else {
			System.out.printf("%d월의 마지막일은 %d일 입니다.", month, lastDat+32);
		}
		
	

		
		if(month == 2) {
			lastDat=28;
		}else if ( month == 4 || month == 6 || month ==9 || month == 11) {
			lastDat=30;
		}else {
			lastDat=31;
		}
	//	System.out.printf("%월의 마지막일은 %d입니다.\n", month, lastDat);
		

	}//end main()

}//end class
