package java0419_statement;

/*
 * 제어문( control statement) : 문장의 흐름을 제어해주느 기능이다.
 * 1. 종류
 *  조건문 : if~else,  switch~case
 *  반복문 : for, while, do~while
 *  기타 : break, continue, (label)기능이라함
 *  
 *  if(조건식){
 *  참일때 수행할 문장;
 *  }else{
 *  거짓일때 수행할 문장;
 *  }
 *  조건식 : 불리언 형태로 무조건 참 과 거짓으로 나와야함.
 */
public class java017_if {

	public static void main(String[] args) { // 1
		//int num =3;//2
		int num = 6;
		
		
		
		if(num % 2 == 0) {//3
			System.out.printf("%d는(은) %s입니다.\n", num, "짝수" ); // 3번의 조건이 만족하면 실행
		}else {//4 3번의 조건이 만족하지 못하면 밑으로
			System.out.printf("%d는(은) %s입니다.\n", num, "홀수");//5  4번이 실행되면 바로 실행
		}
		
		
		
		

	}//end main()

}//end class
