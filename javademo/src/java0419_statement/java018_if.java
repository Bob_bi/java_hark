package java0419_statement;

/*
 * 조건식을 만족할때만 수행할 문장이 있는 경우
 * if(조건식){
 *  수행할 문장;
 * }
 */



public class java018_if {

	public static void main(String[] args) {
		//int num=10;
		int num=-4;
		
		if(num>0) {
			System.out.printf("%d는(은) 자연수입니다.\n", num);
		}
		
		System.out.println("pogram end");
		
		
		

	}//end main()

}//end class
