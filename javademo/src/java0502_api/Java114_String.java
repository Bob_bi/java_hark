package java0502_api;

/*
 * [출력 결과]
 * ip :127.0.0.1
 * port:8080
 * 
 */

public class Java114_String {

	public static void main(String[] args) {
		String sn = "127.0.0.1:8080";
		
		int index = sn.indexOf(":"); // 9
		System.out.println("ip:"+sn.substring(0,index));
		System.out.println("port:"+sn.substring(index+1));
		
		System.out.println("ip "+sn.substring(0,9));
		System.out.println("port:"+sn.substring(10));
		
		String[] str = sn.split(":");
		for(int i=0; i<str.length; i++) {
			if(i==0) 
				System.out.println("ip :"+str[i]);
			else
				System.out.println("port:"+str[i]);
				
		}// (".")을 이용하여 스필릿을 할경우 .은 접근연산자이기 때문에 ("\\.") 이렇게 해야한다.

	}//end main()

}//ens class
