package java0502_api;

/*
 * JAVA API(Application Programming Interface)
 * Object
 * 1.클래스 중에서 최상의 클래스이다.
 * 2.object을 제외한 모든 클래스들은 object을 상속받고 있다.
 * 3.java.lang패키지에서 제공하는 클래스이다.
 */

/*컴파일시 자동으로 생성되는 코드
 * import java.lang.*;
 * default constructor
 * return;
 * .toString()
 * this.
 * super()
 * 자동 upcasting
 * 인터페이스에서의 메소드 public abstract 메소드
 * 인터체이스에서의 필드 public static final 필드(변수)
 */

public class Java110_Object {

	public static void main(String[] args) {
		Object obj = new Object();
		System.out.println(obj.toString());
		
		int x=5;
		int y=5;
		// ==값비교(기본데이터)
		System.out.printf("x=%d y=%d x==y:%b\n",x,y,x==y);
		
		Object ojt = new Object();
		//==주소비교(참조데이터)
		System.out.printf("obj==ojt:%b\n",obj==ojt);
		
		//참조데이터 타입만 비교 할수있다. equals(): 주소비교-객체비교만 가능하다.
		System.out.printf("obj.equals(ojt)=%b\n",obj.equals(ojt));
		
		//class java.lang.Object
		System.out.println(obj.getClass());// obj내에 있는 클래스의 주소
		
		//java.lang.Object
		System.out.println(obj.getClass().getName());
		
		System.out.println(obj);
		System.out.println(obj.toString());
	}//end main()

}//end class
