package java0502_api;
/*
 * 정규식(Regular Expression)
 * 1.정규식이란 텍스트 데이터 중에서 원하는 조건(패턴)과 일치하는 문자열을
 *    찾아내기 위해 사용하는 것으로 미리 정의된 기호와 문자를 이용해서
 *    작성한 문자열을 말한다.
 */


public class Java120_RegEx {

	public static void main(String[] args) {
		String sn = "java korea";
		String sg = "";
		String st = "java      korea";
		
		for(int i=0; i<sn.length(); i++) {
			if(sn.charAt(i)=='a' || sn.charAt(i)=='r') {
				sg+="_";
			}else {
				sg+=sn.charAt(i);
			}
		}
		System.out.println(sg);
		//replaceAll 원하는 문자를 다른 문자의 형태로 바꿀수있다.[] 대괄호를 하고 문자를 입력하면 ar a나 r을 바꿔준다.
		//replace 원하는 문자를 다른 문자의 형태로 바꿀수있다.
		System.out.println("regular expression을 이용한 문자변경");
		System.out.println(sn.replaceAll("[ar]", "_"));
		
		//sn변수에 저장된 문자열에서 a이거나 r이 포함되면 true 아니면 false을 리턴한다.
		//sn="aet";
		System.out.println(sn.matches(".*ar.*")); // [ar] 둘중에 하나만 있으면 true 두개다 있으면 false
		                                         //[ar]. 둘중의 문자로 시작하고 뒤의 임의문자 하나이면 트루
												//[ar].{2,3} 둘중의 문자로 시작하고 뒤의 임의문자 둘 이거나 셋이면 트루
												//[ar].* 둘중의 문자로 시작하고 뒤의 임의문자가 오면 트루 갯수 상관없음
												//[ar].+ 둘중의 문자로 시작하거나 뒤의 숫자가 하나 이상이면 트루
												//.*[ar].* 문자열 중간에 a이거나 r이면 true
		
		//{2,3} 2개부터 3개 까지의 공백을 "@"으로 변경한다.
		st = "java     korea";
		System.out.println(st.replaceAll("\\s{2,3}", "@")); // 공표 공백이 2개이거나 3개이면 @이 하나를 찍는다.
		                                                    // //s공백을 뜻한다.
	}//end main()

}//end class
