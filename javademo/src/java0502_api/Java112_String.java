package java0502_api;

public class Java112_String {
	
	public static void main(String[] args) {
		String sn="java"; // 자바메모리 힙에서 먼저 문자열 값이 있는지 확인해보고 없으면 만든다.
		String sg="java"; // 이미 만들어져있기때문에 같은곳을 가르킨다.
		String st= new String("java");//new를 이용하면 무조건 새로 매모리방이 생긴다.
		String ss= new String("java");
		
		//주소비교
		System.out.printf("sn==sg:%b\n",sn==sg); //true
		System.out.printf("st==ss:%b\n",st==ss); //false
		//equals() : 메모리에 저장된 문자열 비교
		System.out.printf("sn.equals(sg):%b\n",sn.equals(sg));
		System.out.printf("sn.equals(ss):%b\n",sn.equals(ss));
		//toString() 매모리에 저장된 문자열을 리턴       ( 오브젝트에 있는 toString메서드를 오버라이딩하여 변화)
		System.out.println(sn.toString());
		System.out.println(sn);
	}//end main()

}//end class
