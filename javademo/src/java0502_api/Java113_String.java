package java0502_api;

public class Java113_String {

	public static void main(String[] args) {
		String str = new String("KoREa,jsp,java");
		System.out.println("문자열길이:"+str.length());
		System.out.println("대문자:"+str.toUpperCase());// 문자열 전체를 대문자로 변환
		System.out.println("소문자:"+str.toLowerCase());// 문자열 전체를 소문자로 변환
		System.out.println("문자: "+str.charAt(1));// 문자열속 인덱스 번호에 해당하는 캐릭터형 출현
		System.out.println("인덱스 : "+str.indexOf("a"));// 차형으로 받은 매개값에 해당하는 문자열을 찾아 인덱스번호로 반환
		System.out.println("인덱스 : "+str.indexOf(97)); // 유니코드값으로 인식하여 97에 대한 유니코드 값을 찾아 인덱스 번호로 반환
		System.out.println("범위 :"+str.substring(5));//특정범위에 있는 문자열을 출력 (5인덱스부터 마지막까지)
		System.out.println("범위 :"+str.substring(2,5));//특정범위에 있는 문자열을 출력 (2인덱스부터 5인덱스미만까지(4까지로 알면됨))
		System.out.println("범위 :"+str.substring(2,8));//특정범위에 있는 문자열을 출력 (2인덱스부터 8인덱스미만까지(7까지로 알면됨))
		//특정구분자를 이용하여 문자를 나눠준다.
		String [] arr = str.split(",");
		for(int i=0; i<arr.length; i++) {
			System.out.printf("arr[%d]=%s\n",i,arr[i]);
		}
		
		int x=10;
		int y=20;
		System.out.printf
		("%d+%d=%d\n",x,y,x+y); 
		
		//int ->String
		String s1= String.valueOf(x);// 특정 값을  문자열로 변환 가능
		String s2= String.valueOf(y);
		System.out.printf("%s+%s=%s\n",s1,s2,s1+s2);
		//char[] -> String
		char[] data = {'1','2','3'};
		String s3 = String.valueOf(data);
		System.out.println(s3);

	}//end main()

}//end class
