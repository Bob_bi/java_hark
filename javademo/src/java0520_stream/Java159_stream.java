package java0520_stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Java159_stream {

	public static void main(String[] args) {
		//바이트 스트림
		InputStream is = System.in;
		
		//바이트 스트림과 문자스트림을 연결해주는 (클래스 인풋스트림리더)
		InputStreamReader ir = new InputStreamReader(is);
		
		//문자스트림
		BufferedReader br =new BufferedReader(ir);
		// 위에 3개를 합친것이 스캐너 클래스이다. 클래스의 생성자값에 파일이나 시스탬을 적어줘야 한다.
		
		int x, y;
		
		try {
			System.out.print("X:");
			x=Integer.parseInt(br.readLine());
			System.out.print("Y:");
			y=Integer.parseInt(br.readLine());
			System.out.printf("%d + %d = %d\n",x,y,x+y);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				br.close();
				ir.close();
				is.close();// 원래는 따로따로 클로즈를 해줘야 한다.
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}


	}//end main()

}//end calss
