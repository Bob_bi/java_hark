package java0520_stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Java158_stream {

	public static void main(String[] args) {
		System.out.println("데이터 입력:");
		//바이트 스트림
		InputStream is = System.in;
		
		//바이트 스트림과 문자스트림을 연결해주는 (클래스 인풋스트림리더)
		InputStreamReader ir = new InputStreamReader(is);
		
		//문자스트림
		BufferedReader br =new BufferedReader(ir);

		//한 라인을 읽어 오는 메소드
		try {
			// 입력데이타를 한라인씩 읽어와서 리턴
			String data =br.readLine(); // readline의 리턴타입이 String 이기때문에 스트링 변수에 저장
			System.out.println(data);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			// 클래스를 설정한 반대로 닫아줘야 한다.
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				ir.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}//end main()

}//end calss
