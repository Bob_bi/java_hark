package java0520_stream;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Java162_stream {

	public static void main(String[] args) {
		
		File file = new File("samle.txt"); // 안에 있는 파일 내용이 아니라 기본적인 정보를 가져온다.
		System.out.println(file.exists()); // 파일있냐? 유무 확인
		System.out.println(file.isFile());// 
		System.out.println(file.length());
		
		FileWriter fw=null;
		
		try {
			fw=new FileWriter(file,false);// file Writer일 경우 매개값의 파일이 없으면 파일이 생성된다. ( true를 주면 qppend기능으로 변경된다 기본은 업데이트 이다.
			// mode의 값이 true이면 append이고
			// mode의 값이 false이면 update이고
			// mode의 값이 생략하면 기본값이 false이고
			fw.write("java\n");//write로 쓰는순간  buffer에 저정됨 (flush 버퍼의 있는 내용을 다른대상에 넣어주고 버퍼에 있는 내용르 지워지는것)
			fw.flush();
			fw.write("JSP\n");
			fw.flush();
			fw.write(97);
			System.out.println();
			fw.close();
//			fw.write("spring");// 클로즈 된상태에서 다시 연결해줄려면 다시 객체 생성을 해줘야 한다.
//			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}// end main()

}// end class
