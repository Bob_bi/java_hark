package java0520_stream;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class Java164_stream {

	public static void main(String[] args) {
		FileReader fr = null;
		//라인번호를 출력할 수 있는 메소드를 제공하는 스트림이다.
		LineNumberReader nr = null;
		
		try {
			fr=new FileReader("src/java0520_stream/score.txt");
			nr=new LineNumberReader(fr); // 직접적으로 연결할수 있는게 아님
			String line ="";
			//1. nr.readLine()가 리천문자열을 line변수에 대입
			//2. line 변수에 문자열과 null을 비교
			while((line=nr.readLine()) != null){ // readLine은 파일의 끝일때 null을 리턴한다.
				System.out.printf("%d: %s\n", nr.getLineNumber(), line);
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				nr.close();
				fr.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}

	}

}
