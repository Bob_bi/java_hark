package java0520_stream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Java163_stream {

	public static void main(String[] args) {
		
		File file = new File("samle.txt");
		FileReader fr=null;
		int date;
		try {
			
			fr=new FileReader("samle.txt");//현재 경로에 있는 샘플쩜텍스트파일을 읽어온다.
			while((date = fr.read()) != -1){
					System.out.print( date);//read메소드든 더이상 읽어올 내용이 없으면 -1을 내보낸다.
				}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
			
	}//end main()

}//end class
