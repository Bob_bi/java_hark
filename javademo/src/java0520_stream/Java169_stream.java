package java0520_stream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.imageio.stream.FileImageInputStream;

/*
 * 직렬화(serializable)
 * 1. 객체를 연속적인 데이터로 변환하는 것이다.
 *     반대로는 역질렬화이다.
 * 2. 객체의 멤버변수들의 값을 일렬로 나열하는 것이다.
 * 3. 객체를 저장하기 위해서는 객체를 직렬화해야 한다.
 * 4. 객체를 저장한다는 것은 객체의 멤버변수의 값을 저장한다는 것이다.
 * 5. 객체를 직렬화하여 입출력할 수 있는 스트림
 *  ObjectInputStream, ObjectoutputStream
 */

// 메모리에 생성된 객체(내용)을 저장하기 위해서는 직렬화를 하여야 파일에 저장할수 있다.

class Phone implements Serializable{// 직렬화 하는것
	String name;
	int price;
	public Phone() {
	}
	public Phone(String name, int price) {
		super();
		this.name = name;
		this.price = price;
	}
	
	@Override
	public String toString() {
		return name + "\t"+price;
	}
	
	
}//end Phone class


public class Java169_stream {

	public static void main(String[] args) {
			File file = new File("src/java0520_stream/Phone.dat");
			FileOutputStream fs = null;
			ObjectOutputStream os=null;
			FileInputStream fi=null;
			ObjectInputStream oi = null;
			
			try {
				fs =new FileOutputStream(file);
				os=new ObjectOutputStream(fs);// 객체 생성
				Phone p =new Phone("android",5000);
				os.writeObject(p); // 생성한 객체에 클래스내에 있는 값을  저장해주는것 파일에 (자동으로 업캐스팅이 된다. 부모 타입으로)
				os.writeObject(new String("java"));
				System.out.println("객체저장");
				os.flush();
			} catch (FileNotFoundException e) {// 체크드 입섹션 예외처리 해야함
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		System.out.println("===============================");
		try {
			fi = new FileInputStream(file);
			oi=new ObjectInputStream(fi);
			Phone p =(Phone)oi.readObject();
			System.out.println(p.toString());
			String sn=(String)oi.readObject();
			System.out.println(sn.toString());
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		

	}//end main()

}//end class
