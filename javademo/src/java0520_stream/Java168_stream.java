package java0520_stream;

import java.io.File;
import java.io.IOException;

public class Java168_stream {

	public static void main(String[] args) {
		File file = new File("src/java0520_stream/test.txt");
		
			//test.txt파일이 존재하지 않으면
		if(!file.exists()) {
			try {
				file.createNewFile(); // 파일이 존재하지 않으면 파일 생성하는 메소드
				System.out.println("파일생성");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			file.delete();//파일삭제
			System.out.println("파일삭제");
		}


	}//end main()

}//end class
