package java0520_stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Java156_stream {

	public static void main(String[] args) {
		
		System.out.println("데이터 입력:");
		
		
		// 콘솔창 목적지에 InputStream으로 연결
		InputStream is = System.in;
		// 1바이트 읽어옴
		
			
			try {
				//한바이트를 읽어와 유니코드로 리턴한다.
				int line = is.read();// 예외처리.인셉션  체크드 립셉션은  받는쪽에서 (컴파일단계에서 ) 예외 처리를 꼭해야 한다.
				// 한글은 잘못읽어온다.
				System.out.println(line);
				System.out.println((char)line);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		

	}//end main()

}//end class
