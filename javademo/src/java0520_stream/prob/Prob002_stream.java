package java0520_stream.prob;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

/*[문제]
 * data.txt 파일에는 PRODUCT 테이블의 컬럼 이름들이 저장되어있다. 
 * 이 컬럼 이름들을 Java 의 변수명으로 변환하여 콘솔창에 출력하는
 *  makeVariable() 메소드를 구현하시오. 
 * Java 의 변수명은 camel case 가 적용된 형태로 변환해야 한다
 * 
 * [실행결과]
 * prodNo
 * prodName
 * price
 * amount
 * maker
 * regDate
 */

public class Prob002_stream {
	public static void main(String[] args) {
		String fileName = ".\\src\\java0520_stream\\prob\\data.txt";
		makeVariable(fileName);
	}// end main()

	private static void makeVariable(String fileName) {
		RandomAccessFile ref = null;
		String  str=null;
		char ch;
		try {
			ref=new RandomAccessFile(fileName, "r");
			while((str=ref.readLine()) != null){
				String str2=str.toLowerCase();
				char[] aa =str2.toCharArray();
				for(int i=0; i<aa.length; i++) {
					if(aa[i]=='_') {
						aa[i+1]-=32;
					}
				}
				for(int i=0; i<aa.length; i++) {
					if(aa[i] != '_') {
						System.out.printf("%c",aa[i]);
					}
					}
				System.out.println();
				}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	

	}// end makeVariable()
}// end class
