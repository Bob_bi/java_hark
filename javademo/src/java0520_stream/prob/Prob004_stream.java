package java0520_stream.prob;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
 * [문제]
 *  jumsu.txt 파일로부터 학생의 점수를 읽어들여 총점과 평균을 jumsu.txt 파일 끝에 덧붙여 출력하는 프로그램을 작성하시오.
    단, 평균점수는 소수점 첫 번째 자리까지만 남기고 나머지는 잘라낸다.(truncate) 
    
    [프로그램 실행결과]
    태연:65
	수영:97
	제시카:100
	티파니:86
	써니:88
	총점:436
	평균:87.2
 */
public class Prob004_stream {
	public static void main(String[] args) {
		// 프로그램을 구현하시오.
		File fr=new File(".\\src\\java0520_stream\\prob\\jumsu.txt");
		RandomAccessFile ref =null;
		String str =null;
		int avg=0;
		int sum =0;
		int num=0;
		double dou = 0.0;
		try {
			ref=new RandomAccessFile(fr, "r");
			
			while((str=ref.readLine()) != null) {
				String[] str2 = str.split(":");
					sum += Integer.parseInt(str2[1]);
					System.out.println(str2[0]+":"+str2[1]);
					num++;
					
					
			}
			dou = sum/num;
			System.out.println("총점 :"+ sum);
			System.out.printf("평균 :%.2f",dou);
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}//end main()
}//end class








