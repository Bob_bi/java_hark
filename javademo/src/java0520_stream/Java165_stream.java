package java0520_stream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Java165_stream {

	public static void main(String[] args) {
		File file = new File("src/java0520_stream/score.txt");
		RandomAccessFile raf = null;
		
		try {
			// "r" : 읽기(read)만 가능하다.
			// "rㅈ" : 읽기(read) 쓰기(write)가 가능하다.
			raf = new RandomAccessFile(file, "r");
			System.out.println(raf.getFilePointer()); // 현재 작업 위치를 나타내주는것을 pointer  db에서 커서라 한다. (0)
			System.out.println((char) raf.read()); // k
			System.out.println(raf.getFilePointer()); //(1)
			System.out.println(raf.readLine()); // 마지막거까지 읽어 온다. im:56/78/12;
			System.out.println(raf.getFilePointer()); //14  텍스트 값에\r\n(줄바꿈)값이 들어가 있다.
			String line = raf.readLine();
			System.out.printf("%s %d\n",line,line.length());
			System.out.println(raf.getFilePointer()); // 현재 커서 포인트 30
			raf.seek(4);//( 무조건 처음으로 간다.) 매개변수 값 만큼 이동한다. 커서포인트 위치변경 -값은 할수없다.
 			System.out.println(raf.readLine()); // 56/78/12
 			
 			raf.skipBytes(2);// 지정된 수 만큼 byte을 건너뛴다.(현재 포인터가 있는 위치를 기준으로 건너뛴다)
 			System.out.println(raf.readLine());//ng:45/100/97
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				raf.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}

	}//end main()

}//end class
