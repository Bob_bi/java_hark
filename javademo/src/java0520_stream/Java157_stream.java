package java0520_stream;

import java.io.IOException;
import java.io.InputStream;

public class Java157_stream {

	public static void main(String[] args) {
		System.out.print("데이터 입력: ");
		InputStream is = System.in;
		int data;
		
		/*
		 * carriage return : 줄의 처음으로 이동(13)
		 * line feed : 다음줄로 이동(10)
		 */
		try {
			while((data=is.read())!=13) {
				System.out.println((char)data);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				//stream 연결 종료
				is.close();//자원 반납하는용도
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

	}//end main()

}// enda class
