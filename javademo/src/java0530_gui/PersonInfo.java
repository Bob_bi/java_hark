﻿package java0530_gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class PersonInfo extends JFrame implements MouseListener, ActionListener {
	PersonMain main;
	PersonMenu menu;
	PersonToolbar tool;
	//현재상태 파악 => crow >-1 : 수정상태, corw<0 : 수정상태 아님
	int crow=-1;

	public PersonInfo() {
		main = new PersonMain();
		menu = new PersonMenu();
		tool = new PersonToolbar();

		setJMenuBar(menu);//Jmenu 서브메뉴를 가지고 있는것   종단메뉴 Jmanueitem
		add(BorderLayout.NORTH, tool);
		add(BorderLayout.CENTER, main);

		// 이벤트 리스너 연결
		main.registerB.addMouseListener(this); // 마우스 리스너  커서가 버튼에 있을때 발새하는 이벤트 현상 글자가 바뀐다.
		main.modifyB.addMouseListener(this);
		main.deleteB.addMouseListener(this);
		main.clearB.addMouseListener(this);
		main.table.addMouseListener(this);

		main.registerB.addActionListener(this);
		main.modifyB.addActionListener(this);
		main.deleteB.addActionListener(this);
		main.clearB.addActionListener(this);
		main.commit.addActionListener(this);
		main.open.addActionListener(this);

		this.setSize(520, 550);
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}// end PersonInfo()//////////////////////////

	public static void main(String[] args) {
		new PersonInfo();
	}// end main()////////////////////////////////

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == main.registerB) {
			setRegister(); // 등록
		}else if(obj==main.modifyB) {
			setModify(); //수정
		}else if(obj==main.deleteB) {
			delete(); //삭제
		}else if(obj==main.clearB) {
			allClear(); //모든 컴포넌트 초기화
		}else if(obj==main.commit) {
			inppp();//DB에 저장
		}else if(obj==main.open) {
			open();//DB에 있는거 불러오기
		}

	}// end actionPerformed///////////////////////
	
	public void open() {//db불러오기
		allClear();
		 List<commitDTO> co =commitDAO.getInstance().open();
		 
		
		 
		for(int i=0; i<co.size(); i++) {
				main.table.setValueAt(i+1, i, 0);
				main.table.setValueAt(co.get(i).getName(), i, 1);
				main.table.setValueAt(co.get(i).getGender(), i, 2);
				main.table.setValueAt(co.get(i).getNumbe(), i, 3);
				main.table.setValueAt(co.get(i).getE_mail(), i, 4);					
		}
		
	}
	
	
	private void inppp() {//DB저장
			List<commitDTO> aList = new ArrayList<commitDTO>();
			for(int i=0; i< main.table.getRowCount(); i++) {
				if(main.table.getValueAt(i, 0)!=null) {
					commitDTO aa = new commitDTO();
					aa.setName((String)main.table.getValueAt(i, 1));
					aa.setGender(((Character)(main.table.getValueAt(i, 2))).charValue());// 시험에 나옴 캐릭터를 다루는거 나옴
					aa.setNumbe((String)main.table.getValueAt(i, 3));
					aa.setE_mail((String)main.table.getValueAt(i, 4));
					System.out.println(aa.getName());
					aList.add(aa);
				}
			}
		commitDAO.getInstance().input(aList);
		
	}
	
	private void allClear() {
		 setClear();// 모든 레코드 초기화 시키기
		 main.tableModel.setRowCount(0); //모든 레코드를 삭제
		 main.tableModel.setRowCount(50);//50개의 레코드를 생성
	}//end allClear()//////////////////////////////
	
	private void delete() {
		//삭제할 행 선택
		int row=main.table.getSelectedRow();
		//System.out.println(row);
		if(row<0) {
			showMessage("삭제할 레코드를 선택하세요");
			return;
		}
		
		int chk=JOptionPane.showConfirmDialog(this, "정말로 삭제하시겠습니까?","삭제",JOptionPane.YES_NO_OPTION);
		if(chk==JOptionPane.CANCEL_OPTION) {
			return;
		}
		//선택한 행 삭제		
		main.tableModel.removeRow(row);
		//행의 갯수 50개 생성
		if(main.table.getRowCount()<50)
		    main.tableModel.setRowCount(50);
		
		createNumber();
	}//end delete()/////////////////////////////////
	
	
	private void createNumber() {
		for(int i=0; i<main.table.getRowCount();i++) {// 행의 갯수 가지고옴 50개
			if(main.table.getValueAt(i, 0)==null) {
				return;
			}
			main.table.setValueAt(i+1, i, 0);// 다시처음부터 행의 값을 다시 매기는것! 행 자체가 없어지기때문에 다시 위로 올릴필요없이 행의 번호만 바꾸면 된다.
		}
	}//end createNumber///////////////////////////////
	
	//입력된 데이터 초기화
	private void setClear() {
		main.nameF.setText("");
		main.tel1F.setText("");
		main.tel2F.setText("");
		main.emailF.setText("");
		main.telC.setSelectedIndex(0);
		main.manR.setSelected(true);
		// 커서위치
		main.nameF.requestFocus();
		
	}//end setClear()///////////////////////////////s
	
	private void setModify() {
		if(crow<0) {
			showMessage("수정할 레코드를 먼저 선택해 주세요");
			return;
		}
		
		String name = main.nameF.getText().trim();
		char sex = main.manR.isSelected() ? '남' : '여';
		String tel1 = (String) main.telC.getSelectedItem(); // .toString();
		String tel2 = main.tel1F.getText().trim();
		String tel3 = main.tel2F.getText().trim();
		String email = main.emailF.getText().trim();
		
		main.table.setValueAt(name, crow, 1);
		main.table.setValueAt(sex, crow, 2);
		main.table.setValueAt(tel1 + "-" + tel2 + "-" + tel3, crow, 3);
		main.table.setValueAt(email,crow, 4);
		
		crow=-1;// 다시 초기화 하는것  등록을 다시 하기 위해서 초기화시키는것.
		setClear();
	}//end setModify()///////////////////////////////////

	private void setRegister() {
		if(crow>-1) {
			showMessage("이미 등록된 레코드입니다.");
			return;
		}
		
		String name = main.nameF.getText().trim();
		char sex = main.manR.isSelected() ? '남' : '여';
		String tel1 = (String) main.telC.getSelectedItem(); // .toString();
		String tel2 = main.tel1F.getText().trim();
		String tel3 = main.tel2F.getText().trim();
		String email = main.emailF.getText().trim();

		for (int i = 0; i < main.table.getRowCount(); i++) {//getrowcount 행의 갯수 나타내는것
			if (main.table.getValueAt(i, 0) == null) {
				main.table.setValueAt(Integer.toString(i + 1), i, 0);
				main.table.setValueAt(name, i, 1);
				main.table.setValueAt(sex, i, 2);
				main.table.setValueAt(tel1 + "-" + tel2 + "-" + tel3, i, 3);
				main.table.setValueAt(email, i, 4);
				break;
			}

		}
		
		setClear();
	}// end setRegister()//////////////////////////
	
	private void setInputRecord() {
		//더블클릭한 레코드의 행값을 얻는다.
		int row=main.table.getSelectedRow();// 현재 선택된 행번호를 가져온다.
		if(row<0 || main.table.getValueAt(row, 0)==null) {
			showMessage("등록되지 않은 레코드입니다.");//진행을 안하는것
			return;
		}
		
		crow=row;
		main.nameF.setText(main.table.getValueAt(row, 1).toString());
		String sex=main.table.getValueAt(row, 2).toString();
		if(sex.equals("남"))
			main.manR.setSelected(true);
		else
			main.womanR.setSelected(true);
		
		String[] tel=main.table.getValueAt(row, 3).toString().split("-");
		main.telC.setSelectedItem(tel[0]);//010앞번호가 들어감 번호 툴박스에 넣어준것.
		main.tel1F.setText(tel[1]);
		main.tel2F.setText(tel[2]);
		
		main.emailF.setText(main.table.getValueAt(row, 4).toString());
		
		
	}//end setInputRecord()
	
	
	private void showMessage(String message) {
		JOptionPane.showMessageDialog(this, message);
	}//end showMessage()

	@Override
	public void mouseClicked(MouseEvent e) {
		Object obj=e.getSource();//여러객체에 리스너를 받았기때문에 구별을 해줘야 한다.
		if (obj == main.table && e.getClickCount() == 2) {
			setInputRecord();// 테이블 객체에 더블클릭이 발생했으면 메소드를 실행한다. 수정명령문/
		}			
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		//마우스커서가 엑션판넬안에 들어갔을때 발생하는 퍼포먼스
		Object obj = e.getSource();
		if (obj == main.registerB)
			main.registerB.setText("Register");
		else if (obj == main.modifyB)
			main.modifyB.setText("Modify");
		else if (obj == main.deleteB)
			main.deleteB.setText("Delete");
		else if (obj == main.clearB)
			main.clearB.setText("Clear");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Object obj = e.getSource();
		if (obj == main.registerB)
			main.registerB.setText("등록");
		else if (obj == main.modifyB)
			main.modifyB.setText("수정");
		else if (obj == main.deleteB)
			main.deleteB.setText("삭제");
		else if (obj == main.clearB)
			main.clearB.setText("초기화");
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

}// end class
