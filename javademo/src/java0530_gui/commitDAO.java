package java0530_gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import templet.JdbcTemplate;

public class commitDAO {
		private static Connection conn;
		private static Statement stmt;
		private static PreparedStatement pstmt;
		private static ResultSet rs;
		
		private static commitDAO comit = new commitDAO();
		
		public commitDAO() {
		}
		
		public static commitDAO getInstance() {
			return comit;
		}
	
		
		
		
		public static void input(List<commitDTO> aList) {
			try {
				conn=JdbcTemplate.getConnection();
				String sql ="insert into inffo values(inffo_number.nextval, ?, ?, ?, ?)";
				pstmt=conn.prepareStatement(sql);
				for(commitDTO a : aList) {
				pstmt.setString(1, a.getName());
				pstmt.setString(2, String.valueOf(a.getGender()));
				pstmt.setString(3, a.getNumbe());
				pstmt.setString(4, a.getE_mail());
				pstmt.addBatch();
				}
				int[] num =pstmt.executeBatch();
				System.out.println("배치완료");
			} catch (ClassNotFoundException | SQLException e) {

				e.printStackTrace();
			}finally {
				JdbcTemplate.close(rs);
				JdbcTemplate.close(pstmt);
				JdbcTemplate.close(conn);
			}
		}//end input
		
		
		public static List<commitDTO> open(){
			List<commitDTO> aList =new ArrayList<commitDTO>();
			try {
				conn=JdbcTemplate.getConnection();
				String sql ="select * from inffo";
				stmt=conn.prepareCall(sql);
				rs=stmt.executeQuery(sql);
				
				while(rs.next()) {
					commitDTO dto = new commitDTO();
					dto.setName(rs.getString("name"));
					dto.setGender(rs.getString("gender").charAt(0));
					dto.setNumbe(rs.getString("numbe"));
					dto.setE_mail(rs.getString("e_mail"));
					aList.add(dto);
					System.out.printf("%s,%s,%s,%s\n",
							rs.getString("name"),
							rs.getString("gender").charAt(0),
							rs.getString("numbe"),
							rs.getString("e_mail"));
				}
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				JdbcTemplate.close(rs);
				JdbcTemplate.close(stmt);
				JdbcTemplate.close(conn);
			}
			
			
			return aList;
		}
		
		
		
		
		
		
		
		
}//end class
