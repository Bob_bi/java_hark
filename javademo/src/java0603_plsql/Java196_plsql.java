package java0603_plsql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import templet.JdbcTemplate;

/* 프로 시저 : pro06_othermod
 * sql>begin
 *     pro06_othermode('정소라','대전');
 */


public class Java196_plsql {
	private Connection conn;
	private CallableStatement cstmt;
	
	public Java196_plsql() {
	
	}
	public void process() {
		try {
			conn=JdbcTemplate.getConnection();
			String sql="{call pro06_othermode(?,?)}";
			cstmt=conn.prepareCall(sql);
			cstmt.setString(1, "정소라");
			cstmt.setString(2, "대전");
			cstmt.executeUpdate();
			System.out.println("insert end");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(cstmt);
		}
	}//end process///////////////////////

	public static void main(String[] args) {
		new Java196_plsql().process();

	}//end main()

}//end class
