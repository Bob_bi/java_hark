package java0603_plsql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import templet.JdbcTemplate;

/*
 *
테이블생성
create table pltest(
  num number,
  message varchar2(50)
);

프로시저 생성
create or replace procedure projdbc01
(msg in varchar2)
is
begin
 for i in 1..10 loop
  insert into pltest values(i,msg);
 end loop;
 commit;
end;
/

execute projdbc01('korea')

select * from pltest;

 */
public class Java195_plsql {
	private Connection conn;
	private CallableStatement cstmt;
	
	public Java195_plsql() {
	}
	public void process() {
		try {
			conn=JdbcTemplate.getConnection();
			String sql ="{call projdbc01(?)}"; //프로시저를 사용하기 위해선 콜오브 스테이먼트 사용 call 
			//String sql = "{begin projdbc01(?)}";
			cstmt=conn.prepareCall(sql);
			cstmt.setString(1, "message");
			cstmt.executeUpdate();
			System.out.println("insert end");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(cstmt);
			JdbcTemplate.close(conn);
		}
		
	}//end process

	public static void main(String[] args) {
		new Java195_plsql().process();

	}//end main()

}//end class
