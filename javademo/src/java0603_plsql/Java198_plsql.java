package java0603_plsql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import templet.JdbcTemplate;
/*
create or replace procedure my_select
(v_empid in number,
v_name out varchar2 ,
v_salary out number)
is
begin 
select first_name,salary
into v_name,v_salary
from employees
where employee_id=v_empid;
end;
/
*/

public class Java198_plsql {
	private Connection conn;
	private CallableStatement cstmt;
	private ResultSet rs;

	public Java198_plsql() {

	}
	
	public void process() {
		try {
			conn=JdbcTemplate.getConnection();
			String sql="{call my_select(?,?,?)}";
			cstmt=conn.prepareCall(sql);
			//입력값
			cstmt.setInt(1, 100);
			
			//반환값
			cstmt.registerOutParameter(2, Types.VARCHAR); // 프로시저를 통하여 값을 자져올때는 registerQout을 사용하여야 한
			cstmt.registerOutParameter(3, Types.INTEGER);
			cstmt.execute();
			System.out.printf("%s %d\n",
					cstmt.getString(2),
					cstmt.getInt(3));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcTemplate.close(rs);
			JdbcTemplate.close(cstmt);
			JdbcTemplate.close(conn);
		}
		
	}//end process()/////////////////////////
	
	public static void main(String[] args) {
		new Java198_plsql().process();

	}//end main()

}//end class
