package java0501_binding.part03;

public class Java111_binding {

	public static  void display(double x, double y) {
		System.out.println(x+y);
	}//end display()
	
	
	public static void processLg(LgTv lg) {
		lg.turnOn();
	}//end processLg()
	
	public static void processSamsung(SamsungTv ss) {
		ss.turnOn();
	}
	
	public static void main(String[] args) {
		display(4.5, 6.3);
		display(10, 20);
		
		
		LgTv lg =new LgTv("LG");
		processLg(lg);
		
		SamsungTv ss =new SamsungTv("SAMSUNG");
		processSamsung(ss); // 결합도가 높다 (의존성이 높다) 특정한 객체만을 받아 쓸수만 있어서 최악이라 한다. 유지보수가 높다 확장성이 낮다.
		//개발자의 목표는 객체들간의  결합도를 낮추는 일이다.
	
	}//end main()

}//end class
