package java0501_binding.part04;

public class Java112_binding {
	public static void process(HomeTv tv) {
		tv.turnOn();
		if(tv instanceof LgTv) { //객체에서 만 사용 형태가 같은지 확인  비교연산자 같은 타입이면 true 아니면 false
			LgTv lg =(LgTv)tv;//다운 캐스팅 객체
			lg.call();
		}else if(tv instanceof SamsungTv) {
			SamsungTv ss = (SamsungTv) tv;
			ss.loc();
		}
		//tv.loc();
	}
	
	public static void main(String[] args) {				
			LgTv lg=new LgTv("LG");
			process(lg);		    
		    SamsungTv ss=new SamsungTv("SAMSUNG");
		    process(ss);
	}

}
