package java0501_binding.prob.part03;

public class Busur {
	private String name;
	private String bu;
	private String gg;
	
	
	public Busur() {
		
	}
	
	public Busur(String name, String bu, String gg) {
		this.name=name;
		this.bu = bu;
		this.gg =gg;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBu() {
		return bu;
	}
	public void setBu(String bu) {
		this.bu = bu;
	}
	public String getGg() {
		return gg;
	}
	public void setGg(String gg) {
		this.gg = gg;
	}
	
	
	

}
