package java0501_binding.prob.part02;

public class Developer extends Employee{
	public Developer() {
		super();
	}

	public Developer(String name, int salary) {
		super(name, salary);
	}
	
	public int ttBonus() {
		int a =0;
		a = (int)(salary*1.5);
		return a;
	}

	@Override
	public int getBonus() { //정규보너스
		return 100000;
	}

	@Override
	public String toString() {
		return super.toString()+"\t"+getBonus();
	}
}