package java0501_binding.prob.part02;

public class Engineer extends Employee{
	public Engineer() {
		super();
	}

	public Engineer(String name, int salary) {
		super(name, salary);
	}
	
	public int ttBonus() { //특별보너스
		int a = 0;
				a = (int)(salary*1.3);
		
		return a;
	}

	@Override
	public int getBonus() { // 정규보너스
		return 150000;
	}

	@Override
	public String toString() {
		return super.toString()+"\t"+getBonus();
	}
}