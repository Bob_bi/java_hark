package java0501_binding.part01;

public class Java109_casting {

	public static void main(String[] args) {
		Parent p = new Parent();
		Outer t =new Outer();
		
		//p=t;
		//t==p;  서로 독립된 참조데이터 타입(객체) 끼리는 형변환이 안된다.
		// 업캐스팅
		Child c= new Child();
		p =c;
		p.process();
		//p.call();
		
		//객체생성 및 업캐스팅
		Parent pt = new Child();
		pt.process();
		//pt.call();
		
		//다운캐스팅
		Child cn=(Child)pt;
		cn.process();
		cn.call();
		
	}//end main()

}//end class
