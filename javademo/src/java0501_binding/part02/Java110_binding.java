package java0501_binding.part02;

public class Java110_binding {

	public static void main(String[] args) {
		BindTest bt =new BindTest();
		// 정적 바인딩
		
		System.out.println(bt.toString());
		
		Object obj =bt;//업캐스팅
		System.out.println(obj.toString());//동적바인딩
		
		process(new String("java")); // 동적바인딩
		process(new Object());// 정적바인딩
		//Object -> Number -> Integer
		process(new Integer(10));
	}//end main()
	
	//is a(상속관계) up-casting(업캐스팅) -> 동적바인딩(dynamic binding)
	public static void process(Object obj) {
		System.out.println(obj.toString());
	}

}//end class
