package java0501_binding.part02;
//import java.lang.Object;   이 패키지에 있는 핵심인 클래스 인터페이스들으 핵심만 모아놓은 패키지 이기때문에 임포트를 안해도 상관없다.
                             // 자바 가상머신에 이미 탑재가  대어 있어 별도의 임포트 없이 사용 가능하다.

public class BindTest extends Object {// 클래스 주에 제일 큰 클래스 Object
	String stn;
	
	public BindTest() {
		
	}
	
	public BindTest(String stn) {
		this.stn=stn;
	}
	
	public String toString() {
		
		return stn;
	}
	
}//end class
