package java0424_method;

/*
 * [출력결과]
 * 문자열의 길이:4
 * 2인덱스의 요소값 가져 오기:v
 */

public class Java060_method {
	
	public static void main(String[] args) {
		char[] data = new char[] {'j','A','v','a'};
		int index =2;
		
		System.out.println("문자열의 길이:"+ length(data)+"\n"+index+"인덱스의 요소값 가져오기: "+charAt(data, index));
		
		
	}//end main()
	
	public static int length(char[] data) {
		//data 배열의 크기를 리턴하는 프로그램을 구현하세요.
		int sum = data.length;
		return sum;
	}//end lenght
	public static char charAt(char[] data, int index) {
		//data 배열에서 index에 해당하는 문자를 리턴하는 프로그램을 구현하세요.
		for(int i=0; i<length(data); i++) {
			if(data[index]==data[i]) {
				return data[index];
			
			}
		}
		return data[index];
	}//end charAt
	
}//end class
