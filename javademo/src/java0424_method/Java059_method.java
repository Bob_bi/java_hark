package java0424_method;
/*
 * [출력결과]
 * 10는(은) 짝수입니다.
 * 
 */
public class Java059_method {

	public static void main(String[] args) {
		int data =15;
		//process()호출하면서 data변수의 값을 넘겨주고
		//[출력결과]을 참조하여 구현하시오.
		
		//System.out.println(process(data));
//		if(process(data)== true) {
//			System.out.printf("%d는(은) 짝수입니다.\n",data);
//			
//		}else {
//			System.out.printf("%d는(은) 홀수입니다.\n",data);
//			
//		}
		
		//조건연산자(삼항연산자)
		String a = process(data) ? "짝수입니다." : "홀수입니다.";
		System.out.printf("%d는(은)%s\n",data,a);
		System.out.println(data+"는(은)"+a);
		System.out.println(data+"는(은)"+(process(data) ? "짝수입니다." : "홀수입니다."));
		
		
		

	}//end main()
	
	public static boolean process(int data) {
		//data매개변수의 값이 짝수이면 true
		//홀수이면 false를 리턴하는 프로그램을 구현하세요.
		if(data%2==0) {
			//System.out.printf("%d는(은) 짝수입니다.",data);
			return true;
		}else {
			//System.out.printf("%d는(은) 홀수입니다.",data);
			return false;
		}
	}


}//end class
