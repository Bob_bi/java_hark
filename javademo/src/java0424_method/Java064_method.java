package java0424_method;

public class Java064_method {
	
	public static void main(String[] args) {
		char[] data = new char[] {'a','b','c'};
		System.out.println(data); // char형의 데이터 배열은 println에서만 배열안에 값이 그대로 나타남 예) {'a','e','v'} => aev
		System.out.printf("%s\n", data); // 주소값 나옴
		System.out.println("data="+data);
		
		int[] num = new int[] {1,2,3};
		System.out.println(num);//주소값만 나옴
		
	}//end main()
	
}//end class
