package java0424_method;

/*
 * [출력결과]
 * 20는 양수입니다.
 * 0은 0입니다.
 * -20은 음수입니다.
 */
public class Java061_method {

	public static void main(String[] args) {
	System.out.println("    [출력결과]    ");	
	process(20);
	process(0);
	process(-20);

	}//end main()
	public static void process(int num) {
		//num변수의 값이 0보다 크면 "양수", 0이면 "0"
		//0보다 작으면 "음수"로
		// 출력하는 프로그램을 구현하세요.
		String nama = num > 0 ? "양수" : num < 0 ? "음수" : " 0";
		System.out.println("  "+num + "은 " + nama + " 입니다.");
		if(num>0) {
			System.out.println(num+"은(는) 음수입니다");
		}else if(num<0){
			System.out.println(num+"은(는) 음수입니다");
		}else {
			System.out.println(num+"은(는) 0 입니다");
		}
	}
	

}//end class
