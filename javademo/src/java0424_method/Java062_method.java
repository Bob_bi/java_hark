package java0424_method;

/*
 * [출력 결과]
 * 입사 총점:815점
 * 입사결과 : 합격입니다.
 */
public class Java062_method {

	public static void main(String[] args) {
		// 두 매개변수의 합계를 계산하여 리턴한다.
		int toeic = 750, it = 65;
		int sum = total(toeic, it);
		System.out.println("  [출력결과]  ");
		System.out.println("입사총점:" + total(toeic, it) + "점");
		System.out.println("입사결과:" + rs(sum) + "입니다.");

	}//end main()
	
	public static int total(int toeic, int it) {
		// 두 매개변수의 합계를 계산하여 리턴합니다.
		
		int sum = toeic + it;
		return sum;
	}//end total
	
	public static String rs(int tot) {
		String as = tot >=800 ? "합격" : "불합격";
		
		//총점이 800이상이면 합격 미만이면 불합격
		
		return as;
	}//end rs
	
	
	

}//end class
