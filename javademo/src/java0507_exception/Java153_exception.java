package java0507_exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.FileAlreadyExistsException;

public class Java153_exception {

	public static void main(String[] args) {
		
		File file = new File("sample.txt");
		
		//FileNotFoundException은 checked exception이다.
		//반드시 try~catch~finally(예외처리)를 한다.
//		try{
//		FileReader fr = new FileReader(file);
//		}catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		
		try {
			FileReader fe = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}//end main()

}//end class
