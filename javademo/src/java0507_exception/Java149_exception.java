package java0507_exception;

public class Java149_exception {

	public static void main(String[] args) {
		int[] num = new int[3];
		
		try {
		num[1]= 10;
		System.out.println(num[1]);
		return;
		
		}catch(ArrayIndexOutOfBoundsException ex) { // 예외가 발생하면 나오는곳
			System.out.println(ex.getMessage());
			System.out.println("오류가 발생했습니다.");
		}finally {// 무조건 실행하는 구역
			System.out.println("program end");
		}


	}//end main()

}//end class
