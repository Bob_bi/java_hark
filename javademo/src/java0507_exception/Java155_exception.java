package java0507_exception;

class UserException extends Exception{
	public UserException(String message) {
		super(message);
	}
}//end class

//throw : 강제적으로 예외를 발생시킬 때 사용한다.
public class Java155_exception {

	public static void main(String[] args) {
		int data=9;
		try {
		if(data<10) {
			throw new UserException("10이상만 입력하세요.");
		}
		System.out.println(data);
		}catch (UserException ex) {
			//System.out.println(ex.toString());
			ex.printStackTrace();
		}
	}//end main()

}//end class
