package java0507_exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Java152_excaption {

	public static void main(String[] args) {
		int[] data = new int[2];
		for(int i=0; i<data.length; i++) {
			data[i] = inputData(i);
		}
		System.out.printf("두 수의 합은 : %d\n", data[0]+data[1]);
		
	}//end main()
	
	//sc.nextInt(); 메소드느 정수를 입력하면 개행문자 앞까지만 읽어오지만
	//에러가 발생하여 개행문자가 버퍼에 남아있어
	//다시 정수입력할때 개행문자가 자동으로 입력되기 때문에 무한반복이된다.
	//\r carriage return 개행문자 (13)유니코드값 커서가 다시 앞으로 가는것.
	//\n new line 줄바꿈 (10)유니코드 값
	
	public static int inputData(int index) {
		Scanner sc = new Scanner(System.in);
		int num;
		
		while(true) {
			try {
				System.out.printf("%d번째 정수를 입력하세요:",index+1);
				num=sc.nextInt();
				break;
			}catch (InputMismatchException ex) {
				System.out.println("정수만 입력해주세요");
//				sc.nextLine(); // 개행문자 다음줄 을 읽어와서  버려준다.
				sc.nextLine();
			}
		}
		return num;	
	}//end inputData()
	

}//end class
