package java0507_exception;

public class Java150_exception {

	public static void main(String[] args) {//1
		StringBuffer sb =null;//2
		
		try {//3
		sb.reverse();//4
		}catch (NullPointerException ex) {//5 예외 처리 발생시 catch가 발동함
			sb=new StringBuffer("Java");//6
			System.out.println(sb);//7
		}finally {//8
			System.out.println("program end");//9
		}//10

	}//end main()//11

}//end class
