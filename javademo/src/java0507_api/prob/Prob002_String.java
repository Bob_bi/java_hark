package java0507_api.prob;

import java.util.Arrays;

/*
 * javac Prob001_String.java
 * java Prob001_String JAva Test
 */
/*1 프로그램 실행할때  "JAva Test" 문자열을 args배열에서 값을 받아 출력결과에 같이
 *  나오도록 프로그램을 구현하시오.
 *  java Prob001_String JAva Test
 *   * 
 * 2 출력결과
 *   source : JAva Test 
 *   convert: jaVA tEST
 *   length: 9
 *   reverse : tseT avAJ
 */

public class Prob002_String {

	public static void main(String[] args) {
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println("source :"+args[0]+" "+args[1]);
		
		String avg = args[0]+" "+args[1];
		System.out.println(avg);
		char[] num = avg.toCharArray(); 
//		for(int i=0; i<num.length; i++) {
//			if(num[i]>='a' && num[i]<='z') {
//				num[i]=(char)(num[i]-32);
//			}else if(num[i]>='A' && num[i]<='Z') {
//				num[i]=(char)(num[i]+32);
//			}
//		}
		for(int i=0; i<num.length; i++) {
			if(Character.isUpperCase(num[i])) {
				num[i]+=32;
			}else {
				num[i]-=32;
			}
		}
		String st =String.valueOf(num);
		System.out.println("convert : "+st);
		System.out.println("length:"+ avg.length());
		StringBuffer sb = new StringBuffer(args[0]);
		StringBuffer sb2 = new StringBuffer(args[1]);
		System.out.println("revers : "+sb2.reverse()+" "+sb.reverse());
			
	

	}	
}// end class

