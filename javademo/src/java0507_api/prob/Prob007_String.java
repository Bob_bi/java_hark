package java0507_api.prob;

/*
 * [출력결과]
 *  영어 소문자 갯수:3
 *  영어 대문자 갯수:3
 *  숫자 갯수:2
 */

public class Prob007_String {

	public static void main(String[] args) {
		String data = "ke4RdTA5";

		display(process(data));
	}// end main()

	public static char[] process(String data) {
		char[] nu = data.toCharArray();

		return nu;
	}// end process()

	public static void display(char[] arr) {
		int numUpper=0;
		int numLower=0;
		int num=0;
		for(char a : arr) {
			if(Character.isUpperCase(a)) {
				numUpper++;
			}else if(Character.isLowerCase(a)) {
				numLower++;
			}else if(Character.isDigit(a)) {
				num++;
			}
		}
		System.out.println("영어 대문자 갯수:"+numUpper);
		System.out.println("영어 소문자 갯수:"+numLower);
		System.out.println("숫자 갯수:"+num);
		
		
		
	}// end display()

}// end class
