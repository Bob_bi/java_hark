package java0507_api.prob;

import java.util.Calendar;

/*
 * [출력결과]
 * <4시부터 12시 미만일때>
 * 지금은 5시 35분입니다.
 * Good Morning
 * 
 * <12시부터 18시 미만일때>
 * 지금은 16시 49분입니다.
 * Good Afternoon
 * 
 * <18시부터 22시 미만일때>
 * 지금은 20시 30분입니다.
 * Good Evening
 * 
 *  <그외 일때>
 *  Good Night
 */


public class Prob003_Calendar {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		
		System.out.println("지금은 "+cal.get(Calendar.HOUR_OF_DAY)+"시 "+cal.get(Calendar.MINUTE)+"분 입니다");
		if (4<=cal.get(Calendar.HOUR_OF_DAY) && cal.get(Calendar.HOUR_OF_DAY)<12){
			System.out.println("지금은 "+cal.get(Calendar.HOUR_OF_DAY)+"시 "+cal.get(Calendar.MINUTE)+"분 입니다");
				System.out.println("Good Morning");
			}else if(12<=cal.get(Calendar.HOUR_OF_DAY) && cal.get(Calendar.HOUR_OF_DAY)<18) {
				System.out.println("지금은 "+cal.get(Calendar.HOUR_OF_DAY)+"시 "+cal.get(Calendar.MINUTE)+"분 입니다");
				System.out.println("Good Afternoon");
				
			}else if(18<=cal.get(Calendar.HOUR_OF_DAY) && cal.get(Calendar.HOUR_OF_DAY)<22) {
				System.out.println("지금은 "+cal.get(Calendar.HOUR_OF_DAY)+"시 "+cal.get(Calendar.MINUTE)+"분 입니다");
				System.out.println("Good Evening");
				
			}else {
				System.out.println("good Night");
			}
				
				
	
	
			
	}//end main()
}//end class
