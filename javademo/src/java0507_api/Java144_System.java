package java0507_api;

import java.text.SimpleDateFormat;

public class Java144_System {

	public static void main(String[] args) {
		
		//currentTimeMillis() : 1970.1.1투터 초단위로
		//누적한 값을 밀리센컨드로 리턴한다.
		//하루는 86400초이다. 1초는 1000밀리세컨드다.
		
		long curr = System.currentTimeMillis();
		System.out.println(curr);
		
		String pattern = "yyyy-MM-dd HH:mm:ss a EEEE";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String date = sdf.format(curr);
		// auto boxing   -> upcasting
		//long(stack) -> (Long(heap)) -> Object 
		System.out.println(date);
		
		
		

	}//end main()

}//end class
