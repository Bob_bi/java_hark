package java0507_api;

import java.util.Calendar;

abstract class Test{
	public static Test getInstance() {
		return new TestExam();
	}
	abstract public void prn();
}//end Test

class TestExam extends Test{
	public void prn() {
		System.out.println("prn");
	}


}//end TestExam

public class Java145_Calendar {

	public static void main(String[] args) {
		//Test tt = Test.getInstance();
		//System.out.println(tt);
		//tt.prn();
		
		//Calendar은 추상클래스이므로 객체생성을 할 수 가 없다.
		Calendar cal = Calendar.getInstance();
//		Calendar cal2 = Calendar.getInstance();
//		System.out.println(cal==cal2);
//		cal.clear();
		System.out.println(cal.toString());
		int year =cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1; // 캘린더 먼스 데이는 출력 나올때 는 0부터 시작하기때문에 출력문으로 보여줄때에는 +1을 해줘야 한다,
		int date = cal.get(Calendar.DATE);
		//int hour = cal.get(Calendar.HOUR); // 12시간 표기
		int hour = cal.get(Calendar.HOUR_OF_DAY); // 24시간 표기
		int minut =cal.get(Calendar.MINUTE);
		int second =cal.get(Calendar.SECOND);
		System.out.printf("%d-%d-%d %d:%d:%d\n",year,month,date,hour,minut,second);
		
		//각 달에 마지막일을 리턴해주는 메소드
		System.out.println(cal.getActualMaximum(Calendar.DATE));
		//오늘의 요일 리턴(일요일 ->1)
		System.out.println(cal.get(Calendar.DAY_OF_WEEK));
		
		// 오늘을 기준으로 5일전의 날짜로 설정
		cal.add(Calendar.DATE, -5);
		System.out.println(cal.toString());
		
		cal.set(2017, 2, 1); // 2017 - 3 - 1
		System.out.println(cal.toString());
		
		
		
	}//end main()

}//end class
