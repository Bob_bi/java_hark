package java0507_api;

import java.util.GregorianCalendar;

public class Java147_GregorianCalendar {

	public static void main(String[] args) {
		// 윤년인지 아닌지 확인하는것은 그레고리안겔린더에 있다.
		int year = 2016;
		GregorianCalendar gre = new GregorianCalendar();
		if(gre.isLeapYear(year)) {
			System.out.println(year+"년도는 윤년입니다.");
		}else {
			System.out.println(year+"년도는 평년입니다.");
		}

	}//end main()

}//end class
