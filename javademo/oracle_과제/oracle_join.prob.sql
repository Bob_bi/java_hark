----------------------------------------------
 문제
 ----------------------------------------------
 
1)EMPLOYEES 테이블에서 입사한 달(hire_date) 별로 인원수를 조회하시오 . 
  <출력: 월        직원수   >
select to_char(hire_date, 'mm')as 월,count(*)as 인원수
from employees
group by to_char(hire_date, 'mm')
order by to_char(hire_date, 'mm');

  
2)각 부서에서 근무하는 직원수를 조회하는 SQL 명령어를 작성하시오. 
단, 직원수가 5명 이하인 부서 정보만 출력되어야 하며 부서정보가 없는 직원이 있다면 부서명에 “<미배치인원>” 이라는 문자가 출력되도록 하시오. 
그리고 출력결과는 직원수가 많은 부서먼저 출력되어야 합니다.
select nvl(to_char(department_id),'미배치인원'),count(*)
from employees
group by department_id
having count(*)<5
order by count(*);


3)각 부서 이름 별로 2005년 이전에 입사한 직원들의 인원수를 조회하시오.
 <출력 :    부서명		입사년도	인원수  >
select d.department_name as 부서명, to_char(hire_date, 'yyyy')as 입사년도,count(*)as 인원수
from departments d ,employees s
where d.department_id=s.department_id
and to_char(hire_date, 'yyyy')<2005
group by to_char(hire_date, 'yyyy'),department_name
order by department_name;


4)직책(job_title)에서 'Manager'가 포함이된 사원의 이름(first_name), 직책(job_title), 부서명(department_name)을 조회하시오.
select first_name, job_title, department_name
from employees e, jobs j, departments d
where d.department_id = e.department_id
and e.job_id=j.job_id
and j.job_title like ('%Manager%');


5)'Executive' 부서에 속에 있는 직원들의 관리자 이름을 조회하시오. 
단, 관리자가 없는 직원이 있다면 그 직원 정보도 출력결과에 포함시켜야 합니다.
 <출력 : 부서번호 직원명  관리자명  >

select d.department_id as, e.first_name as "직원명", e2.first_name as "관리자명"
from departments d, employees e , employees e2
where d.department_id = e.department_id
and e.manager_id = e2.employee_id(+)
and d.department_name = 'Executive';



