package ncs.test8;

public class UserTes {

	public static void main(String[] args) {
		User[] users = new User[3];
		//user 객체를 3개 생성하여 배열에 넣는다.
		
		users[0] = new User("user01","pass01","김철수",32,'M',"010-1234-5678");
		users[1] = new User("user02","pass02","이영희",25,'F',"010-1234-5678");
		users[2] = new User("user03","pass03","황진이",20,'F',"010-1234-5678");
		for(int i=0; i<users.length; i++) {
			System.out.println(users[i].toString());
		}
		User[] copyUsers =new User[users.length];
		//users 가 참조하는 객체들의 복사본을 만든다. for문을 이용할것
		for(int i=0; i<copyUsers.length; i++) {
			copyUsers[i]= (User)users[i].clone(); 
		}
		//copyUsers의 객체 정보를 모두 출력한다. - for each문을 이용할것
		System.out.println("==========copyUsers================");
		for(User u : copyUsers) {
			System.out.println(u.toString());
		}
		
		//배열에 있는 객체 정보를 모두 toString() 으로 출력한다. -for문을 이용
		System.out.println("============비교결과=================");
		for(int i=0; i<users.length; i++) {
			if(users[i] == copyUsers[i]) {
				System.out.println(true);
			}else {
				System.out.println(false);
			}
		}
		
		
		//users copyUsers 의 각 index별 객체의 값들이 일치하는지 확인 출려한다.

	}//end main()

}//end class
