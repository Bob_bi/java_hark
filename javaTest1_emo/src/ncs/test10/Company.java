package ncs.test10;

public class Company {

	public static void main(String[] args) {
		Employee[] employees = new Employee[2];
		
		// 1번의 사용 데이터를 기반으로 객체를 생성하여 배열에 넣는다.
		employees[0] = new Secretary("Hilery", 1, "secretary", 800);
		employees[1] = new Sales("Cilnten", 2, "sales", 1200);
		// 모든 객체의 기본 정보를 출력한다 (for 문을 이용하여 출력한다)
		System.out.println("name \t department \t salary");
		System.out.println("=====================================");
		for(int i=0; i<employees.length;i++) {
			System.out.printf("%s    %s     %d\n", employees[i].getName(),employees[i].getDepartment(),employees[i].getSalary());
		}
		// 모든 객체에 인센티브 100씩 지급한 급여를 계산하고 다시 객체의  salary 에 넣는다
		System.out.println("인센티브 100지급");			
			Secretary se =(Secretary)employees[0];
			Sales so =(Sales)employees[1];
			se.incentive(100);
			so.incentive(100);
			employees[0] = se;
			employees[1] = so;
		// 모든 객체의 정보와 세금을 출력한다.(for문을 이용하여 출력한다.)
			System.out.println("name \t department \t salary \t tax");
			System.out.println("=====================================");
			for(int i=0; i<employees.length;i++) {
				System.out.printf("%s    %s     %d    %.1f\n", employees[i].getName(),employees[i].getDepartment(),employees[i].getSalary(),employees[i].tax());
			}

	}//end main()

}//end class
