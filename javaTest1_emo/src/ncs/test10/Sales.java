package ncs.test10;

public class Sales extends Employee implements Bonus {

	
	 public Sales() {
		
	}
	 public Sales(String name, int number, String department, int salary) {
		 super(name,number,department,salary);
	 }
	
	
	public double tax() { // 세금
		
		return getSalary()*0.13;
	}
	
	@Override
	public void incentive(int pay) { // 인센티브 지급
		super.setSalary((int)(super.getSalary()+(pay*1.2)));
		
	}

}
