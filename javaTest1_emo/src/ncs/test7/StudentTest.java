package ncs.test7;

public class StudentTest {

	public static void main(String[] args) {
		Student[] student = new Student[3];
		//student 객체를 3개 생성하여 배열에 넣는다.
		student[0] = new Student("홍길동",25,171,81,"201401","영어영문학");
		student[1] = new Student("한사랑",23,183,72,"201402","건축학");
		student[2] = new Student("임꺽정",26,175,65,"201403","체육학");
	// 배열에 있는 객체 정보를 모두 toString 으로 출력한다. - for문을 이용하할것!
		
		for(Student s : student) {
			System.out.println(s.toString());
		}

	}//end main()

}//end class
