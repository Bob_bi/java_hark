package ncs.test12;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javafx.scene.web.PromptData;

public class PropTest {
	

	public static void main(String[] args) {
		Properties p =new Properties();
		fileSave(p);
		fileOpen(p);

	}//end main()
	
	public static void fileOpen(Properties p) {
		Fruit[] fu = new Fruit[p.size()];
		String[] str = new String[p.size()];
		System.out.println(p.size());
		try {
			InputStream input= new FileInputStream("data.xml");
			p.load(input);
			input.close();
			for(int i=1; i<=p.size(); i++) {// 파일을 읽어 오고  property을 이용하여 한문장씩 출력
				str=p.getProperty(i+"").split(",");
				fu[i-1]=new Fruit(str[0],Integer.parseInt(str[1]),Integer.parseInt(str[2]));
			}
			// toString 형에서 문자열 리턴
			for(int i=0; i<fu.length; i++) {
				System.out.println((i+1)+fu[i].toString());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}//end fileOpen
	public static void fileSave(Properties p) {
		Fruit f = new Fruit();
		p.setProperty("1", "apple,1200,3");
		p.setProperty("2", "banana,2500,2");
		p.setProperty("3", "grape,4500,5");
		p.setProperty("4", "orange,800,10");
		p.setProperty("5", "melon,5000,2");
		try {
			OutputStream steam = new FileOutputStream("data.xml");
			p.store(steam, "data");
			steam.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}//end class
