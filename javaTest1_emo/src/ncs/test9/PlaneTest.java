package ncs.test9;

public class PlaneTest {

	public static void main(String[] args) {
		//Airplane 과 Cargoplane 객체 생성
		Airplane ai = new Airplane("L747",1000);
		Cargoplane ai2 = new Cargoplane("C40",1000);
		
		// 생성된 객체의 정보 출력
		System.out.println("Plane  fuelSize");
		System.out.println("===========================");
		System.out.println(ai.planeName +"      " +ai.fuelSize );
		System.out.println(ai2.planeName +"       " +ai2.fuelSize );
		//Airplane 과 Carhoplane 객체에 100씩 운항
		System.out.println("100 운항");
		ai.flight(100);
		ai2.flight(100);
		
		//100 운항 후 객체 정보 출력
		System.out.println("Plane  fuelSize");
		System.out.println("===========================");
		System.out.println(ai.planeName +"      " +ai.fuelSize );
		System.out.println(ai2.planeName +"       " +ai2.fuelSize );
		//Airplane 과 Carhoplane 객체에 200씩 주유
		System.out.println("200 주유");
		ai.refuel(200);
		ai2.refuel(200);
		//200 주유 후 객체 정보 출력
		System.out.println("Plane  fuelSize");
		System.out.println("===========================");
		System.out.println(ai.planeName +"      " +ai.fuelSize );
		System.out.println(ai2.planeName +"       " +ai2.fuelSize );

	}//end main()

}//end class
