package ncs.test9;

public abstract class Plane {
	String planeName;
	int fuelSize;
	
	public Plane() {
		
	}
	
	public Plane(String planeName, int fuelSize) {
		this.planeName=planeName;
		this.fuelSize=fuelSize;
	}
	public void refuel(int fule) { // 주유
		this.fuelSize +=fule;
	
	}
	
	public abstract void flight(int distance); // 운항
	

}//end class
