package ncs.test1;

public class ArrayTest {

	public static void main(String[] args) {
		int[][] array = {{12,41,36,56,21},{82,10,12,61,45},{14,16,18,78,65},{45,26,72,23,34}};
		
		double num=0;
		double avg=0;
		int av =0;
		
		for(int i=0; i<array.length; i++) {
			for(int j=0; j<array[i].length; j++) {
				num +=array[i][j];
				av++;
			}
		}
		
		System.out.printf("합계 : %.2f\n ",num);
		System.out.printf("평균 : %.2f \n", num/av);
		
		
	}//end main()

}//end class
