package ncs.test13;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MapTest {
	
	public static void main(String[] args) {
		// Generics 적용된 맴 객체를 선언 할당한다.
		Map<String, Inventory> map = new HashMap<String, Inventory>();
		SimpleDateFormat simdate = new SimpleDateFormat("yyyy년  MM월 dd일");
		String da ="2016-03-15";
		Date date;
		try {
			//상품명을 키로 사용하여 저장 처리한다.
			date = simdate.parse(da);
			map.put("삼성 갤럭시s7",new Inventory("삼성 갤럭시s7",date,30));
			da ="2016-02-25";
			date = simdate.parse(da);
			map.put("LG G5",new Inventory("삼성 갤럭시s7",date,20));
			da ="2016-01-23";
			date = simdate.parse(da);
			map.put("애플 아이패드 Pro", new Inventory("삼성 갤럭시s7",date,15));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 맴에 기록된 정보를 연속 출력한다. EntrySet() 사용한다.
		
		//맵에 기록된 정보를 Inventory() 로 변환한 다음
		// 출고 날짜를  오늘 날짜로, 출고 수량은 모두 10개로 지정한다. 예외처리함
		
		//변경된 Inventory()의 정보를 모두 출력한다.
		
	}//end main()

}//end class
