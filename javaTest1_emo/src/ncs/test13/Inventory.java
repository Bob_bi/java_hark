package ncs.test13;



import java.util.Date;

import javafx.scene.chart.PieChart.Data;

public class Inventory {
	private String productName;
	private Date putDate;
	private Date getDate;
	private int putAmount;
	private int getAmount;
	private int inventoryAmount;
	
	public Inventory() {
		
	}
	
	public Inventory(String productName, Date putDate, int putAmount) {
		this.productName=productName;
				this.putDate=putDate;
				this.putAmount=putAmount;
				this.inventoryAmount=putAmount;
	}
	
	public void setGetDate(Date setDate) {
		
	}
	
	@Override
	public String toString() {
	
		return  productName+putDate+getDate+putAmount+getAmount+inventoryAmount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getPutDate() {
		return putDate;
	}

	public void setPutDate(Date putDate) {
		this.putDate = putDate;
	}

	public int getPutAmount() {
		return putAmount;
	}

	public void setPutAmount(int putAmount) {
		this.putAmount = putAmount;
	}

	public int getGetAmount() {
		return getAmount;
	}

	public void setGetAmount(int getAmount) {
		this.getAmount = getAmount;
	}

	public int getInventoryAmount() {
		return inventoryAmount;
	}

	public void setInventoryAmount(int inventoryAmount) {
		this.inventoryAmount = inventoryAmount;
	}

	public Date getGetDate() {
		return getDate;
	}
	
	

}//end class

