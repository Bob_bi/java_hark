package ncs.test11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class ListTest {
	
	public static void display(List<Integer> list) {
		for(Integer a : list) {
			System.out.printf("%4d",a);
		
		}
	}

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		int num=0;
		// 명시한 사용 데이터를 list에 기록한다.
		for(int i=1; i<=10; i++) {
			int ro =  ((int)(Math.random()*100))+1;
			list.add(ro);
		}
		
		//list의 데이터를 내림차순 정렬한다.
		//정렬 전 호출
		
		System.out.print("정렬전 : ");
		for(Integer a : list) {
			System.out.printf("%4d",a);
		}
		//내림차순 정렬
		System.out.println();
		System.out.print("내림차순 정렬후 : ");
		list.sort(new Decending());
	
		//display 메소드를 호출한다.
		display(list);
	}//end main()
	

}//end class
