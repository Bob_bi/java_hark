///////////////////////////////////////////////////////////////////////////
//                    	데이터베이스 연결하는 클래스                                                   //
//////////////////////////////////////////////////////////////////////////
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

       //DB연결에 필요한 객체
	   public static Connection dbConn;
	   
	   //DB에 연결하는 함수 getConnection()
	   //sqldeveloper에 만들어준 계정으로 접속
       public static Connection getConnection()
       {
    	   //연결에 필요한 객체 conn
           Connection conn = null;
           try {
        	   //sqldeveloper에서 접속할때 입력하는 부분을 String객체로 만들어서 저장
               String user = "mini"; //sqldeveloper에서 접속하는 아이디
               String pw = "tiger";	////sqldeveloper에서 mini라는 아이디의 비밀번호
               String url = "jdbc:oracle:thin:@localhost:1521:xe"; ////sqldeveloper접속 주소
               
               //java와 db연결해주는 jdbc 드라이버 로딩(import)
               Class.forName("oracle.jdbc.driver.OracleDriver");
               //아까 생성한 conn객체에 DriverManager클래스의 getConnection함수를 통해 DB연결
               conn = DriverManager.getConnection(url, user, pw);
               //DB연결 완료성공하면 성공했다는 메세지 출력
               System.out.println("Database에 연결되었습니다.\n");
           
           //밑에 부분들은 접속할때 발생하는 예외처리
           } catch (ClassNotFoundException cnfe) {
               System.out.println("DB 드라이버 로딩 실패 :"+cnfe.toString());
           } catch (SQLException sqle) {
               System.out.println("DB 접속실패 : "+sqle.toString());
           } catch (Exception e) {
               System.out.println("Unkonwn error");
               e.printStackTrace();
           }
           //DB와 연결된 상태인 객체 conn리턴
           return conn;     
       }


}
