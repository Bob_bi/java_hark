///////////////////////////////////////////////////////////////////////////
//                프로그램 시작하는 main()메소드가 있는  Main클래스                               //
//////////////////////////////////////////////////////////////////////////


//프로그램 실행할떄 main()함수가 먼저 실핼되므로 여기서 부터 시작
//main()함수부터 순차적으로 프로그램 흐름을 따라가시면 됨
package mini;



public class Main {

	public static void main(String[] args) {
			
			//전체로직 while(true) : 무한반복
			while(true) {
				//Log클래스의 init()함수의 리턴값을 result에 저장
				//result의 값을 가지고 로직을 진행
				//result==1 : 회원가입 로직
				//result==2 : 기존유저 로그인 로직
				//result==3 : 프로그램 종료
				//result==4 : 1,2,3이 아닌 잘못된 값 입력했을때 
				int result = Log.getLogInstance().init();
				System.out.println("mainresult:"+result); //result값 출력
				//result값에 따른 서로 다른 로직
				if(result==1) {
					//회원가입
					clearScreen();
					//회원가입하는 함수 Log.getLogInstance().newjoin() 호출
					//리턴 값이 true이면 "회원가입 성공" 메세지 출력하고 다시 처음 메뉴로
					if(Log.getLogInstance().newjoin()) {
						System.out.println("[회원가입 성공]");
						continue; //회원가입성공하면 다시 while시작부분으로
					}
					
				}else if(result==2){
					//로그인
					clearScreen();
					//로그인하는 함수 Log.getLogInstance().login() 호출
					//리턴값이 true이면 "로그인 성공" 메세지 출력하고 
					//음악재생 담당하는 Play클래스 객체 하나 생성하고
					//음악재생하는 playSound()함수호출
					if(Log.getLogInstance().login()) {
						System.out.println("[로그인 성공]");
						Play p = new Play();
						p.playSound();
					}
				}else if(result==3){
					System.out.println("[종료합니다]");
					break; //종료니 while문 빠져나오고 프로그램 종료
				}else if(result==4){//잘못된 값 입력해서 while처음으로 다시 돌아감
					System.out.println("[잘못입력햇습니다. 다시 실행하세요]");
					continue;
				}
				
			}//end while

	
	}//end main

	//그냥 콘송창 화면에 빈한줄 출력함수
	public static void clearScreen() {
		
		  for (int i = 0; i < 1; i++)
		      System.out.println();
	}
}//end class
