///////////////////////////////////////////////////////////////////////////
//                    	로그인,회원가입 처리 클래스      							//
//																		//										
//-로그인,새로운유저 회원가입 처리 담당하는 클래스									//
//-하나만 존재하면 되기 때문에 Singleton 패턴 사용									//
//																		//
//////////////////////////////////////////////////////////////////////////
package mini;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import db.DBConnection;


public class Log {
	//Log클래스의 객체 lg에 실제 인스턴스를 생성자로 만들어서 대입
	private static Log lg = new Log();
	//Log클래스 기본생성자	
	private Log() {
		System.out.println("[Log클래스 생성]");
	}//end Log()
	
	//처음 프로그램 실행해서 어떤작업을 할건지 입력받는 함수 init()
	//입력받은 값에 따라 return하고 종료
	public int init() {
		//Scanner로 사용자 키보드 입력값 받기
		Scanner scan = new Scanner(System.in);
		System.out.printf("[1.회원가입    2.로그인    3.종료]");
		int result = scan.nextInt(); //입력받은값 result에 저장
		System.out.println("init()result:"+result); //result 출력
		//입력받은 result값을 리턴
		if(result ==1) {
			//scan.close();
			return result;
		}else if(result ==2) {
			//scan.close();
			return result;
		}else if(result ==3){
			//scan.close();
			return 3;
		}else {
			return 4;
		}
	}//end init()
	
	//새로운 회원가입을 담당하는 함수 newjoin()
	public boolean newjoin() {
		//새로 가입
		//DB에 유저정보 저장
		Scanner scan = new Scanner(System.in);
		//무한루프
		while(true) {
			//아이디,패스워드,패스워드확인 3개 입력받아서 저장
			System.out.println("[아이디     패스워드     패스워드한번더입력]");
			String id = scan.next(); //아이디
			String pw = scan.next(); //패스워드
			String rpw = scan.next(); //패스워드 확인
			
			//패스워드 두번 입력했는데 두개가 틀리면? 다시처음으로
			//가입할때 패스워드 잘못칠수도있으니 확인하는거 두번입력받아서
			if(pw.equals(rpw)==false) {
				System.out.println("[비밀번호 입력잘못했습니다]");
				Main.clearScreen();
				continue;
			}
			
			Connection conn = null; // DB연결된 상태(세션)을 담은 객체
	        PreparedStatement pstm = null;  // SQL 문을 나타내는 객체
	        int result = 0;  // 쿼리문을 날린것에 대한 반환값
			
	        //String 객체 quary에 sqldeveloper에서 실행할 쿼리문을 작성
	        //원래는 sqldeveloper에서 쿼리문을 작성했는데 우리는 자바랑 연동해서
	        //할거니까 여기서 실행할 쿼리문을 작성해서 실행
	        String quary = "INSERT INTO USERINFO(id,pw) VALUES(?,?)";
	        //DB연결관리하는 DBConnection클래스의getConnection()함수호출하면
	        //DB에 연결된 객체를 리턴해준다 그걸 받아서 conn객체에 저장하면
	        //conn객체가 DB연결된 상태이므로 이걸 이용해서 작업 , 다시 DB연결작업을 여기서 할 필요X
            conn = DBConnection.getConnection();
            //이부분은 백프로 이해못하셔도 되고 어떤식으로 쓴다만 이해하시면 되요
            try {
            	//작성한 쿼리문을 pstm객체에 import
				pstm = conn.prepareStatement(quary);
				//위에 작성한 quary문에 ?부분에 대입입 ?부분이 두개
				//1번 ?에는 id 저장
				//2번 ?에는 pw 저장
				pstm.setString(1, id);
				pstm.setString(2, pw);
				//이제 완성된 쿼리문을 실행하는 함수
				//실행하고 그 결과값을 result에 저장(리턴값:반영된 레코드의 건수)
				result = pstm.executeUpdate();
				//위에 insert쿼리문 하면 DB에 내용이 저장되고 반영된 레코드건수가 1개니까 1 리턴
				//리턴값이 1이면 developer에서 쿼리문이 정상적으로 실행된거니까 break로 
				//while문 빠져나옴
				if(result ==1)
					break;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				//scan.close();
				//처리다되면 db연결이랑 쿼리연결 닫기
			    try{
	                if ( pstm != null ){pstm.close();}   
	                if ( conn != null ){conn.close(); }
	            }catch(Exception e){
	                throw new RuntimeException(e.getMessage());
	            }
			}
		}//end while()
		
		return true; //회원가입 성공했으니 true리턴
	}//end newjoin()
	
	//기존 유저 로그인담당 함수 login()
	public boolean login() {
		//DB연걸해서 로그인
		Scanner scan = new Scanner(System.in);
		//아이디,패스워드 2개 입력받아서 저장
		while(true) {
			System.out.println("[로그인 창입니다]");
			System.out.println("[아이디	패스워드]");
			String id = scan.next();
			String pw = scan.next();
			
			//System.out.println(id);
			//System.out.println(pw);
			
			Connection conn = null; // DB연결된 상태(세션)을 담은 객체
			PreparedStatement pstm = null;  // SQL 문을 나타내는 객체
			ResultSet rs = null;  // 쿼리문을 날린것에 대한 반환값을 담을 객체
			
			//실행할 쿼리문 - 입력받은 id의 pw를 DB에서 받아오는 쿼리문
			String quary = "SELECT pw FROM USERINFO WHERE id="+"'"+id+"'";
			//System.out.println(quary);
			//DB연결
			conn = DBConnection.getConnection();
			 try {
				 	//쿼리문 import
					pstm = conn.prepareStatement(quary);
					//pstm.setString(1, id);
					//쿼리문 실행해서 결과값 rs객체에 저장
					rs = pstm.executeQuery();
					/*if(!rs.next()) {
						System.out.println("해당하는 정보가 없습니다");
					}*/
					//쿼리문 실행 결과값이 있을떄 실행
					//결과값이 있다는 말은 입력받은 id값이 USERINFO라는 테이블에 정보가 있다는말
					//즉 회원가입 되있는 id이다
					while(rs.next()) {
						//System.out.println("id : " + id);
						//System.out.println("pw : " + rs.getString(1));
						//rs.getString(1)호출하면 결과값가져오는데 결과가 pw하나뿐
						//즉 사용자가 입력한 pw랑 DB에서 가져온 pw가 일치하면 아이디에 맞는
						//비밀번호를 입력한거기 떄문에 로그인 성공
						if(pw.equals(rs.getString(1))) {
							//System.out.println("로그인 성공");
							//로그인하고 User클래스에 id랑 pw저장하고 true 리턴 함수종료
							User.getInstance().setId(id);
							User.getInstance().setPw(pw);
							return true;
						}else {
							System.out.println("[비밀번호가 틀립니다]");
						}
					}
				//예외처리 및 쿼리import,db연결 닫기	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					//scan.close();
				    try{
		                if ( pstm != null ){pstm.close();}   
		                if ( conn != null ){conn.close(); }
		            }catch(Exception e){
		                throw new RuntimeException(e.getMessage());
		            }
				}
			
		}
		
	}//end login()
	
	public static Log getLogInstance() {
		return lg;
	}
}
