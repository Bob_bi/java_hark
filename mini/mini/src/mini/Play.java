///////////////////////////////////////////////////////////////////////////
//                    	음원파일 재생 관련 클래스        							//
//																		//
//////////////////////////////////////////////////////////////////////////
package mini;

import java.io.File;
import java.io.IOException;


import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

//선택된 파일을 로컬에서 찾아서 실행하기
public class Play {
	/*
	//선택된 번호
	private play_number;
	public void play_file(int number) {
		//선택된 mp3파일 로컬에서 불러와서 재생
	}
	*/
	
	public Play() {
	
	}
	//음악파일 재생하는 함수
	//일단은 이함수는 뭐하는거다만 알면 되요
	public void playSound() {
		
		
		File file = new File("C:\\sound\\test\\test.wav");
		AudioInputStream audioInputStream = null;
		SourceDataLine auline = null;
		
		try {
			audioInputStream = AudioSystem.getAudioInputStream(file);
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AudioFormat format = audioInputStream.getFormat();
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
		
		try {
			auline = (SourceDataLine)AudioSystem.getLine(info);
			auline.open(format);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		auline.start();
		
		int nBytesRead = 0;
		final int EXTERNAL_BUFFER_SIZE = 524288;
		byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];
		
		try {
			while(nBytesRead != -1) {
				nBytesRead = audioInputStream.read(abData,0,abData.length);
				if(nBytesRead >=0)
					auline.write(abData, 0, nBytesRead);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			auline.drain();
			auline.close();
		}
	}
	
}
