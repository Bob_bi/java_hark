///////////////////////////////////////////////////////////////////////////
//                    	로그인한 유저의 정보클래스        							//
//-Singleton 패턴															//
//////////////////////////////////////////////////////////////////////////
package mini;

//유저의 객체
public class User {
	
	//아이디,패스워드,플레이 리스트
	private String id;
	private String pw;
	private String[] playlist;
	
	private static User us = new User();
	
	private User() {
		System.out.println("[유저클래스 생성]");
	}
	
	public static User getInstance() {
		return us;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}
	
}
