package Db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Dbconnect { 
	public static Connection dbConn;
	public static Statement stmt;
	public static ResultSet rs;
	
	public static Connection process() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			
			String url = "jdbc:oracle:thin://@127.0.0.1:1521:xe";
			String username = "mine";
			String password = "a1234";
			dbConn=DriverManager.getConnection(url,username,password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dbConn;
		
	}

}
