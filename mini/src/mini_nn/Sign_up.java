package mini_nn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import Db.Dbconnect;

public class Sign_up {
	Connection conn;
	PreparedStatement stmt;
	ResultSet rs;
	private String id="";
	private String pwd="";
	private String pw2="";
	int number=0;
	Scanner sc =new Scanner(System.in);
	
	private static Sign_up su = new Sign_up();
	
	private Sign_up() {
		System.out.println("[Sign_up 클래스 생성]");
	}//end Sign_up
	
	public static Sign_up getInstance() {
		return su;
	}
	
	public boolean process() {

		while(true) {
			System.out.println("[가입 할  아이디 페스워드를 입력해주세요.]");
			id=sc.nextLine();
			System.out.println("비밀번호를 입력해 주세요.");
			pwd=sc.nextLine();
			System.out.println("비밀번호를 다시한번 입력해주세요.");
			pw2=sc.nextLine();

			if(!(pwd.equals(pw2))) {
				System.out.println("비밀번호를 다시입력해주세요.");
				continue;
			}

			String str = "insert into mini_member(id,pwd) values(?,?)";
			conn= Dbconnect.process();

			try {
				stmt = conn.prepareStatement(str);
				stmt.setString(1, id);
				stmt.setString(2, pwd);

				number = stmt.executeUpdate();
				if(number==1) {
					break;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}finally {
				if(stmt != null) {try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}}
				if(conn != null) {try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				if(rs != null) {try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}}
				}
			}
		}
		return true;
	}//end process



	

}//end Sign_up
