package mini_nn;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


import Db.Dbconnect;


public class Log_in {
	
	public static Connection conn;
	public static Statement ad;
	public static ResultSet srs;
	
	
	public static boolean book= true;
	private static Log_in lg = new Log_in();
	
	private Log_in() {
		System.out.println("[클래스생성]log");
	}//end Log_in()
	//
	public static Log_in getInstance() {
		return lg;
	}
	
public static boolean Log() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("=========로그인==========");
		
		while(true) {
			conn= Dbconnect.process();
			System.out.print("아이디 입력해주세요 :");
			String id = sc.nextLine();
			System.out.print("페스워드 입력해주세요 :");
			String pw = sc.nextLine();
		
		try {
			ad=conn.createStatement();
			String str = "select * from mini_member";
			
			srs = ad.executeQuery(str);
			
			while(srs.next()) {
				String idd= srs.getString("id");
				String pww=srs.getString("pwd");
				if(id.equals(idd) && pw.equals(pww)) {
					book=true;
					break;
				}else {
					book=false;
					continue;					
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn!=null) {try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}}
			if(ad!=null) {try {
				ad.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}}
			if(srs!=null) {try {
				srs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}}
		}
		break;
	}
	return book;
		
	}//end Log()
	
	
	

}//end Lod_in
