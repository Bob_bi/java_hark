package mini_nn;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import Db.Dbconnect;

public class PlayList  {
public static Connection co;
public static Statement st;
public static ResultSet rs;

public static PlayList pl = new PlayList();
private PlayList() {
	
}//end PlayList

public static PlayList getIns() {
	return pl;
}


							
public static void musicPlary(){
	HashMap<Integer, String> hm = new HashMap<Integer, String>();
	Scanner sc = new Scanner(System.in);
	int numb=0;
	File bgm;
	AudioInputStream stream;
	AudioFormat format;
	DataLine.Info info;
	int num=0;
	co=Dbconnect.process();
	System.out.println("플레이 리스트 입니다.");
	try {
		st=co.createStatement();
		String sql = "select * from music_playlist";
		rs= st.executeQuery(sql);
		while(rs.next()) {
			num = rs.getInt("num");
			System.out.printf("%d  :  %s\n",num,rs.getString("name"));
			hm.put(num,rs.getString("name"));
		}
		while(true) {
			System.out.print("듣고 싶은  문장의 번호를 입력해주세요(종료 하고 싶으면 0번을 누르세요) : ");
			numb=sc.nextInt();
			if(numb!=0) {
				String str = hm.get(numb);
				bgm=new File("D:\\JDB_num\\"+str+".wav");
			
				Clip clip;
				try {
					stream = AudioSystem.getAudioInputStream(bgm);
					format = stream.getFormat();
					info = new DataLine.Info(Clip.class, format);
					clip = (Clip)AudioSystem.getLine(info);
					clip.open(stream);
					clip.start();
				} catch (UnsupportedAudioFileException | IOException e) {
					e.printStackTrace();
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				}
				continue;
			}
			break;
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		if(co!=null) {try {
			co.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}if(st!=null) {try {
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}if(rs!=null) {try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}}}}
	}
}

}//end playList
